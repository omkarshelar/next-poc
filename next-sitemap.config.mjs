/** @type {import('next-sitemap').IConfig} */
const config = {
  siteUrl: process.env.SITE_URL || "http://localhost:3000",
  generateRobotsTxt: true, // (optional)
  sitemapSize: 10000,
  exclude: ["/server-sitemap-index.xml"], // <= exclude here
  robotsTxtOptions: {
    additionalSitemaps: [
      "http://localhost:3000/server-sitemap-index.xml", // <==== Add here
    ],
  },
  // ...other options
};

export default config;
