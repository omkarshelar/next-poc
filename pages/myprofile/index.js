import React, { Component } from "react";
import { connect } from "react-redux";
import "../../pageComponents/myprofile/Profile.css";
import avatarIcon from "../../src/Assets/avatar.svg";
import { ProfileData } from "../../pageComponents/myprofile/ProfileData";
import CustomerDetails from "../../pageComponents/myprofile/SubComponent/CustomerDetails";
import PatientDetails from "../../pageComponents/myprofile/SubComponent/PatientDetails/PatientDetails";
import AddressDetails from "../../pageComponents/myprofile/SubComponent/AddressDetails/AddressDetails";
import {
  fetchCustomerDetailsThunk,
  updateCustomerDetailsThunk,
} from "../../redux/Login/Action";
import Router, { withRouter } from "next/router";
import { message } from "antd";
import { setCurrentPage } from "../../redux/Pincode/Actions";
import { Helmet } from "react-helmet";
export class Profile extends Component {
  state = {
    profileType: "customer",
  };

  componentDidMount() {
    this.props.setCurrentPage({ currentPage: "profile" });
    this.getCustomerDetails();
  }

  getCustomerDetails = () => {
    this.props.fetchCustomerDetailsThunk({
      access_token: this.props.accessToken,
      customerId: this.props.customerId,
      history: Router,
    });
  };

  updateDetails = (data) => {
    this.props.updateCustomerDetailsThunk(data).then(() => {
      if (this.props.error) {
        message.error("Something went wrong. Try again later.");
      } else {
        message.success("Details updated successfully!");
        this.getCustomerDetails();
      }
    });
  };

  getCustomerName = (name) => {
    let string = name.split(" ");
    let firstName = string[0];
    let lastName = string[string.length - 1];
    let lastNameLetter = lastName.slice(0, 1);
    let fullName = firstName + " " + lastNameLetter + ".";
    return fullName;
  };

  onImageChange = (e) => {
    let file = e.target.files[0];
    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onloadend = () => {
      let data = {
        customerName: this.props.customerDetails.customerName
          ? this.props.customerDetails.customerName
          : "",
        mobileNo: "",
        ageGroupId: "",
        ageGroupName: "",
        languageId: "",
        languageName: "",
        emailAddress: this.props.customerDetails.emailAddress
          ? this.props.customerDetails.emailAddress
          : "",
        profileImage: reader.result ? reader.result : "",
        profileImageUrl: "",
        age: this.props.customerDetails.age
          ? Number(this.props.customerDetails.age)
          : "",
        gender: this.props.customerDetails.gender
          ? Number(this.props.customerDetails.gender)
          : "",
        genderName: "",
        addressId: "",
        history: Router,
        access_token: this.props.accessToken,
      };
      this.updateDetails(data);
    };
  };

  render() {
    return (
      <div>
        <Helmet
          htmlAttributes={{ lang: "en" }}
          meta={[
            {
              name: "robots",
              content: "noindex nofollow",
            },
          ]}
        />
        {this.props.customerDetails && (
          <div className="profileHeaderContainer">
            <span>
              Welcome Back{" "}
              {this.props.customerDetails?.customerName?.length > 20
                ? this.getCustomerName(this.props.customerDetails.customerName)
                : this.props.customerDetails.customerName}
            </span>
            <div className="profileHeaderInsideContainer">
              {this.props.customerDetails.profileImageUrl ||
              this.props.customerDetails.profileImage ? (
                <img
                  src={
                    this.props.customerDetails.profileImage
                      ? this.props.customerDetails.profileImage
                      : this.props.customerDetails.profileImageUrl
                  }
                  alt="avatar"
                />
              ) : (
                <img src={avatarIcon} alt="avatar" />
              )}
              <label htmlFor="upload-photo-image">
                <label className="profileEditText" htmlFor="upload-photo-image">
                  Edit Profile Photo
                </label>
                <input
                  type="file"
                  name="photo"
                  id="upload-photo-image"
                  onChange={this.onImageChange}
                  multiple
                  accept="image/*"
                  capture="environment"
                />
              </label>
            </div>
          </div>
        )}
        <div className="profileTypesContainer">
          {ProfileData.map((data) => (
            <div
              key={data.id}
              className={
                this.state.profileType == data.id
                  ? "profileSelectedType"
                  : "profileUnselectedType"
              }
              onClick={() => {
                this.setState({ profileType: data.id });
              }}
            >
              <span
                className={
                  this.state.profileType == data.id
                    ? "profileSelectedTypeText"
                    : "profileUnselectedTypeText"
                }
              >
                {data.type}
              </span>
            </div>
          ))}
        </div>
        <div className="profileTypeContainer">
          {this.props.customerDetails &&
            this.state.profileType === "customer" && (
              <CustomerDetails
                fullName={this.props.customerDetails?.customerName}
                age={this.props.customerDetails?.age}
                gender={this.props.customerDetails?.gender}
                email={this.props.customerDetails?.emailAddress}
                token={this.props.accessToken}
                history={Router}
                details={this.props.customerDetails}
                error={this.props.customerDetailsError}
                getCustomerDetails={this.getCustomerDetails}
                updateDetails={this.updateDetails}
              />
            )}
          {this.state.profileType === "patient" && (
            <PatientDetails profile={true} />
          )}
          {this.state.profileType === "address" && (
            <AddressDetails profile={true} />
          )}
        </div>
      </div>
    );
  }
}

let mapStateToProps = (state) => ({
  accessToken: state.loginReducer.verifyOtpSuccess?.Response?.access_token,
  isLoading: state.loader?.isLoading,
  customerDetailsError: state.loginReducer.customerDetailsError,
  customerDetails: state.loginReducer.customerDetails?.CustomerDetails,
  customerId: state.loginReducer.verifyOtpSuccess?.CustomerDto?.customerId,
  currentPage: state.pincodeData?.currentPage?.currentPage,
});

export default withRouter(
  connect(mapStateToProps, {
    fetchCustomerDetailsThunk,
    updateCustomerDetailsThunk,
    setCurrentPage,
  })(Profile)
);
