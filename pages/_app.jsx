import App from "next/app";
import React from "react";
import Router from "next/router";
import Head from "next/head";
import { wrapper } from "../redux/store";
import window from "global";
import dynamic from "next/dynamic";
//helper
import { parseCookies } from "../components/Helper/parseCookies";

//components
import Navigation from "../components/WebComponents/Navigation/Navigation";
// import SideBar from "../components/WebComponents/SideBar/SideBar";
// import BackDropSideBar from "../components/WebComponents/Navigation/BackDropSideBar/BackDropSideBar";

//actions
import { pincodeSectionAction } from "../redux/Pincode/Actions";
import { getMasterDetailsThunkNew } from "../redux/SliderService/Actions";

//css imports
import "../node_modules/antd/dist/antd.css";
import "bootstrap/dist/css/bootstrap.min.css";
import "../components/styles/style.css";
import "../components/styles/App.css";

//image
import faviconLogo from "../src/Assets/logo192.png";

//dynamic imports
const SideBar = dynamic(() =>
  import("../components/WebComponents/SideBar/SideBar")
);
const BackDropSideBar = dynamic(() =>
  import(
    "../components/WebComponents/Navigation/BackDropSideBar/BackDropSideBar"
  )
);
// const Navigation = dynamic(() =>
//   import("../components/WebComponents/Navigation/Navigation")
// );

class MyApp extends App {
  static getInitialProps = wrapper.getInitialAppProps(
    (store) =>
      async ({ Component, ctx }) => {
        //fetching cookies
        const cookies = parseCookies(ctx?.req);

        // fetching pincode from cookies
        let pincodeData = null;
        if (cookies?.pincodeData) {
          pincodeData = JSON.parse(cookies?.pincodeData);
          await store.dispatch(pincodeSectionAction(pincodeData.pincodeData));
        }

        //fetching token from cookies
        let token = null;
        if (cookies?.token) {
          token = cookies?.token;
        }

        let customerId = null;
        if (cookies?.customerId) {
          customerId = cookies?.customerId;
        }
        let masterDetails = null;
        masterDetails = store.getState().masterDetails;
        if (!masterDetails) {
          await store.dispatch(
            getMasterDetailsThunkNew({
              accessToken: token,
              history: Router,
              customerId: customerId,
            })
          );
        }

        let isMobile = false;
        let hostName = "";
        //Server Call
        if (ctx?.req) {
          let userAgent = ctx?.req?.headers["user-agent"];
          hostName = ctx?.req?.headers["host"];
          isMobile = userAgent
            ? Boolean(
                userAgent.match(
                  /Android|BlackBerry|iPhone|iPad|iPod|Opera Mini|IEMobile|WPDesktop/i
                )
              )
            : false;
        } else {
          //Client Call
          isMobile = window?.innerWidth <= 768 ? true : false;
          hostName = window.location.hostName;
        }
        return {
          pageProps: {
            // Call page-level getInitialProps
            // DON'T FORGET TO PROVIDE STORE TO PAGE
            ...(Component.getInitialProps
              ? await Component.getInitialProps({ ...ctx, store })
              : {}),
            // Some custom thing for all pages
            pathname: ctx.pathname,
            isMobile: isMobile,
            pincodeDetails: pincodeData?.pincodeData,
            hostName: hostName,
          },
        };
      }
  );
  render() {
    //Page props that were returned  from 'getInitialProps' are stored in the props i.e. pageprops
    // console.log("in _APP");
    const { Component, pageProps } = this.props;

    let showMobile = false;
    let showWeb = false;
    let newHeader = false;
    let showSearch = false;

    let webFooterList = [
      "",
      "empty-cart",
      "orderflow",
      "myorderdetails",
      "myorders",
    ];
    let mobFooterList = ["/"];
    if (webFooterList.includes(pageProps.pathname.split("/")[1])) {
      showWeb = true;
    }
    if (mobFooterList.includes(pageProps.pathname)) {
      showMobile = true;
    }

    let headerList = [
      "orderflow",
      "empty-cart",
      "orderstatus",
      "myorders",
      "myorderdetails",
    ];
    let showSearchList = ["/orderflow"];

    if (headerList.includes(pageProps.pathname.split("/")[1])) {
      newHeader = true;

      if (newHeader && showSearchList.includes(pageProps.pathname)) {
        showSearch = true;
      }
    }

    return (
      <>
        <Head>
          <meta charset="utf-8" />
          <link
            rel="alternate"
            hreflang="en-IN"
            href="https://www.truemeds.in/"
          />
          <title>
            Online Medicines Order: Buy Medicines Online with the best discount
            @ Truemeds
          </title>
          <meta
            name="keywords"
            content="Online Medicines order, buy medicines online, buying medicine online, order medicine online, medicines online, medicine home delivery, online medicine shopping, online delivery of medicine, online pharmacy, online buy medicine"
          />
          <meta
            name="description"
            content="Online Medicines Order: One-stop solutions for all your health care needs. Buy medicines online at a discounted price from truemeds. Get up to 72% off with free delivery."
          />
          <link rel="icon" href={faviconLogo} />
          <meta
            property="og:title"
            content="Online Medicines Order: Buy Medicines Online with the best discount @ Truemeds"
          />
          <meta
            property="og:description"
            content="Online Medicines Order: One-stop solutions for all your health care needs. Buy medicines online at a discounted price from truemeds. Get up to 72% off with free delivery."
          />
          <meta property="og:image" content="faviconLogo" />
          <meta property="og:url" content="https://truemeds.in" />
        </Head>
        <div className="App">
          <div className="contentWrap">
            {pageProps.pathname !== "/register" &&
              pageProps.pathname !== "/verify" &&
              pageProps.pathname !== "/info" &&
              pageProps.pathname !== "/payment" && (
                <Navigation
                  newHeader={newHeader}
                  showSearch={showSearch}
                  pincodeDetails={pageProps.pincodeData}
                  isMobile={pageProps.isMobile}
                />
              )}
            <SideBar isMobile={pageProps.isMobile} />
            <BackDropSideBar isMobile={pageProps.isMobile} />
            <Component {...pageProps} />
          </div>
        </div>
      </>
    );
  }
}
export default wrapper.withRedux(MyApp);

// //makeStore function that returns a new store for every request
// const makeStore = () => store;

// //withRedux wrapper that passes the store to the App Component
// export default withRedux(makeStore)(MyApp);
