///? Default IMPORTS
import React, { Component } from "react";
import { connect } from "react-redux";
import { wrapper } from "../../redux/store";
import Router, { withRouter } from "next/router";
import { parseCookies } from "../../components/Helper/parseCookies";

///? Service IMPORTS
import { setCurrentPage } from "../../redux/Pincode/Actions";
import { walletThunk } from "../../redux/TMWallet/action";
import { getFaqCategory, getFaq } from "../../redux/FAQCall/action";
import { referralStatusThunk } from "../../redux/ReferEarn/action";

///? Component & Styling IMPORTS
import { Container } from "@material-ui/core";
import { ReferEarnWrapper } from "../../pageComponents/refer/ReferEarn.styled";
import { FAQSidebar } from "../../components/WebComponents/FAQSidebar/FAQSidebar";
import { message, Tabs } from "antd";
import ReferNow from "../../pageComponents/refer/ReferNow/ReferNow";
import MyReferral from "../../pageComponents/refer/MyReferral/MyReferral";
import ReferEarnLoader from "../../pageComponents/refer/ReferEarnLoader/ReferEarnLoader";
import window from "global";

///? Constants
const { TabPane } = Tabs;

export class ReferEarn extends Component {
  state = {
    faqData: null,
    activeReferTab: null,
    loading: true,
    referralStatusData: null,
  };

  componentDidMount = () => {
    // window.scrollTo(0, 0);
    // if (process.browser) {
    //   const body = document.querySelector("#root");

    //   body.scrollIntoView(
    //     {
    //       behavior: "smooth",
    //     },
    //     500
    //   );
    // }

    if (this.props.wallet.tmCashError) {
      message.error("Something went wrong.");
    }

    if (this.props.referEarn.referralStatusError) {
      message.error(this.props.referEarn.referralStatusError);
    } else {
      this.setState({
        loading: false,
        referralStatusData: this.props.referEarn.referralStatusData,
      });
    }

    if (this.props.faqData?.errorMessage) {
      message.error("Something went wrong.");
    } else {
      this.setState({
        faqData: this.props.faqData.FAQData?.payload,
      });
    }

    this.setState({ activeReferTab: "1" });

    // let { accessToken, customerId } = this.props;
    // this.props
    //   .walletThunk({
    //     access_token: accessToken,
    //     customerId: customerId,
    //     history: this.props.history,
    //   })
    //   .then(() => {
    //     if (this.props.wallet.tmCashError) {
    //       message.error("Something went wrong.");
    //     } else {
    //       this.props
    //         .referralStatusThunk({
    //           history: this.props.history,
    //           customerId: customerId,
    //           access_token: accessToken,
    //         })
    //         .then(() => {
    //           if (this.props.referEarn.referralStatusError) {
    //             message.error(this.props.referEarn.referralStatusError);
    //           } else {
    //             this.setState({
    //               loading: false,
    //               referralStatusData: this.props.referEarn.referralStatusData,
    //             });
    //           }
    //         });
    //     }
    //   });

    // this.props
    //   .getFaqCategory({
    //     history: this.props.history,
    //   })
    //   .then(() => {
    //     if (this.props.faqData?.errorMessage) {
    //       message.error("Something went wrong.");
    //     } else {
    //       this.setState(
    //         {
    //           faqCategory: this.props.faqData.FAQCategory?.payload.filter(
    //             (item) => item.name == "Refer And Earn"
    //           ),
    //         },
    //         () => {
    //           this.props
    //             .getFaq({
    //               history: this.props.history,
    //               categoryId: this.state.faqCategory
    //                 .map((item) => item.id)
    //                 .toString(),
    //             })
    //             .then(() => {
    //               if (this.props.faqData?.errorMessage) {
    //                 message.error("Something went wrong.");
    //               } else {
    //                 this.setState({
    //                   faqData: this.props.faqData.FAQData?.payload,
    //                 });
    //               }
    //             });
    //         }
    //       );
    //     }
    //   });
  };

  changeActiveKey = (value) => {
    this.setState({ activeReferTab: value });
  };

  render() {
    return (
      <Container style={{ maxWidth: "1100px" }}>
        <ReferEarnWrapper>
          {!this.state.loading ? (
            <Tabs
              defaultActiveKey={"1"}
              activeKey={
                !this.state.activeReferTab ? "1" : this.state.activeReferTab
              }
              className="referEarnTabs"
              onChange={(key) => {
                // Router.push({
                //   state: { activeReferTab: key },
                // });
                this.setState({ activeReferTab: key });
              }}
            >
              <TabPane tab="Refer Now" key="1">
                <ReferNow
                  history={this.props.history}
                  info={this.props.wallet.tmCash}
                  referralStatusData={this.state.referralStatusData}
                  changeActiveKey={this.changeActiveKey}
                />
              </TabPane>
              <TabPane tab="My Referrals" key="2">
                <MyReferral
                  changeActiveKey={this.changeActiveKey}
                  history={this.props.history}
                  referralStatusData={this.state.referralStatusData}
                  info={this.props.wallet.tmCash}
                />
              </TabPane>
            </Tabs>
          ) : (
            <ReferEarnLoader />
          )}

          {window.innerWidth >= 768 ? (
            <FAQSidebar
              tmwalletCondition
              history={this.props.history}
              // faqData={null}
              faqData={this.props.faqData.FAQData.payload}
              loading={this.props.faqData.FAQData.isLoading}
              // loading={false}
            />
          ) : this.state.activeReferTab === "1" ? (
            <FAQSidebar
              tmwalletCondition
              history={this.props.history}
              // faqData={null}
              faqData={this.props.faqData.FAQData.payload}
              loading={this.props.faqData.FAQData.isLoading}
              // loading={false}
            />
          ) : (
            ""
          )}
        </ReferEarnWrapper>
      </Container>
    );
  }
}

//? Server side calls

ReferEarn.getInitialProps = wrapper.getInitialPageProps(
  (store) => async (props) => {
    const cookies = parseCookies(props.req);

    await store.dispatch(setCurrentPage({ currentPage: "refer" }));

    await store.dispatch(
      walletThunk({
        history: Router,
        access_token: cookies?.token,
        customerId: cookies.customerId,
      })
    );

    await store.dispatch(
      referralStatusThunk({
        history: Router,
        access_token: cookies?.token,
        customerId: cookies.customerId,
      })
    );

    await store.dispatch(
      getFaqCategory({
        history: Router,
      })
    );

    let value = store
      .getState()
      .faqData.FAQCategory?.payload.filter((i) => i.name == "Refer And Earn")
      .map((p) => p.id)
      .toString();

    value &&
      (await store.dispatch(
        getFaq({
          history: Router,
          categoryId: value,
        })
      ));

    let prop = {};
    prop.accessToken =
      store.getState().loginReducer.verifyOtpSuccess?.Response?.access_token;

    prop.customerId =
      store.getState().loginReducer?.verifyOtpSuccess?.CustomerId;
    prop.wallet = store.getState().wallet;
    prop.referEarn = store.getState().referEarn;

    prop.faqData = store.getState().faqData;

    return { ...prop };
  }
);

const mapStateToProps = (state) => ({
  // referEarn: state.referEarn,
  // customerId: state.loginReducer.verifyOtpSuccess?.CustomerId,
  // accessToken: state.loginReducer.verifyOtpSuccess?.Response?.access_token,
  // wallet: state.wallet,
  // faqData: state.faqData,
});

const mapDispatchToProps = {
  setCurrentPage,
  walletThunk,
  getFaqCategory,
  getFaq,
  referralStatusThunk,
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(ReferEarn)
);
