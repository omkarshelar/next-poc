import { Button, message, Modal } from "antd";
import { connect } from "react-redux";
import { walletHistoryThunk, walletThunk } from "../../redux/TMWallet/action";
import App from "next/app";
import {
  CarouselItem,
  CarouselStyle,
  Carouselwrapper,
  List,
  ReferModalWrapper,
  ReferralTextField,
  SectionTitle,
  WalletContainer,
  WalletWrapper,
} from "../../pageComponents/wallet/TMWallet.style";
import { wrapper } from "../../redux/store";
import Router, { withRouter } from "next/router";
import { setCurrentPage } from "../../redux/Pincode/Actions";
import { Container } from "@material-ui/core";
import { redeemCodeThunk } from "../../redux/ReferEarn/action";
import { Helmet } from "react-helmet";
import ReferBanner from "../../pageComponents/wallet/SubComponentNew/ReferBanner/ReferBanner";
import { TMWalletMain } from "../../pageComponents/wallet/SubComponentNew/TMWalletMain/TMWalletMain";
import { getFaqCategory, getFaq } from "../../redux/FAQCall/action";
import DividerImg from "../../pageComponents/wallet/SubComponentNew/Images/separator.svg";
import { FAQSidebar } from "../../components/WebComponents/FAQSidebar/FAQSidebar";
import { Component } from "react";
import { parseCookies } from "../../components/Helper/parseCookies";

export class TMWallet extends Component {
  state = {
    option: 1,
    transactionData: null,
    loading: true,
    faqCategory: null,
    faqData: null,
  };
  selectOption = (id) => {
    this.setState({ option: id });
  };
  componentDidMount() {
    if (this.props.wallet.tmCashError) {
      message.error("Something went wrong.");
    }
    if (this.props.TransactionError) {
      message.error("Something went wrong.");
    } else {
      this.filterTransactionHistory();
    }

    if (this.props.faqData?.errorMessage) {
      message.error("Something went wrong.");
    }
  }

  filterSpecificStatusTransaction = (array, status) => {
    let filterFunc = array.filter((data) => data.status === status);
    return filterFunc;
  };
  groupElementofSimilarOrderId = (objectArray, property) => {
    return objectArray.reduce((acc, obj) => {
      const key = obj[property];
      if (!acc[key]) {
        acc[key] = [];
      }
      acc[key].push(obj);
      return acc;
    }, {});
  };

  filterSubElementBasedOnDate = (result) => {
    let tempStorage = [];
    for (const iterator in result) {
      if (result[iterator].length > 1) {
        let a = result[iterator].reduce((a, b) => {
          return new Date(a.transactionDate) > new Date(b.transactionDate)
            ? a
            : b;
        });
        tempStorage.push(a);
      } else {
        tempStorage.push(result[iterator][0]);
      }
    }
    return tempStorage;
  };

  filterFinalArrayBasedOnDate = (array) => {
    let sortedArray = array.sort(
      (a, b) => new Date(b.transactionDate) - new Date(a.transactionDate)
    );
    return sortedArray;
  };

  filterTransactionHistory = () => {
    let { TransactionList } = this.props;

    if (TransactionList?.payload?.ledger) {
      let transationWithStatusOne = this.filterSpecificStatusTransaction(
        TransactionList.payload.ledger,
        1
      );
      let transationWithStatuszero = this.filterSpecificStatusTransaction(
        TransactionList.payload.ledger,
        0
      );
      let sameOrderIdGroups = this.groupElementofSimilarOrderId(
        transationWithStatuszero,
        "orderId"
      );

      let orderWithStatusZeroResult =
        this.filterSubElementBasedOnDate(sameOrderIdGroups);

      let combinedArray = [
        ...orderWithStatusZeroResult,
        ...transationWithStatusOne,
      ];

      let sortByDate = this.filterFinalArrayBasedOnDate(combinedArray);
      this.setState({ transactionData: sortByDate, loading: false });
    }
  };

  recordView = (value) => {
    if (value === "showRewardScreen") {
      this.setState({ showRewardScreen: true, showCreditScreen: false });
    } else if (value === "showCreditScreen") {
      this.setState({ showRewardScreen: false, showCreditScreen: true });
    }
  };

  backFunction = () => {
    this.setState({
      showCreditScreen: false,
      showRewardScreen: false,
    });
  };

  render() {
    let { option } = this.state;

    return (
      <Container>
        <Helmet
          htmlAttributes={{ lang: "en" }}
          meta={[
            {
              name: "robots",
              content: "noindex nofollow",
            },
          ]}
        />
        <WalletWrapper>
          <div className="leftSide">
            <TMWalletMain
              info={this.props.wallet.tmCash}
              TMBtn={(state) => this.recordView(state)}
              history={Router}
              transactionData={this.state.transactionData}
              loading={this.props.faqData.isLoading}
            />
            <ReferBanner
              info={this.props.wallet.tmCash}
              banners={this.props.TransactionList?.walletBanners}
              transactionData={this.state.transactionData}
              loading={this.props.wallet?.loading}
            />
          </div>

          <img src={DividerImg} alt="Divider" className="TMFAQSeparator" />

          <FAQSidebar
            tmwalletCondition
            history={Router}
            faqData={this.props.faqData.FAQData.payload}
            loading={this.props.faqData.FAQData.isLoading}
          />
        </WalletWrapper>
      </Container>
    );
  }
}

//? Server side calls

TMWallet.getInitialProps = wrapper.getInitialPageProps(
  (store) => async (props) => {
    const cookies = parseCookies(props.req);

    await store.dispatch(setCurrentPage({ currentPage: "tm-wallet" }));

    await store.dispatch(
      walletThunk({
        history: Router,
        access_token: cookies?.token,
        customerId: cookies.customerId,
      })
    );

    await store.dispatch(
      walletHistoryThunk({
        history: Router,
        access_token: cookies?.token,
        customerId: cookies.customerId,
      })
    );

    await store.dispatch(
      getFaqCategory({
        history: Router,
      })
    );

    let value = store
      .getState()
      .faqData.FAQCategory?.payload.filter((i) => i.name == "TM Wallet")
      .map((p) => p.id)
      .toString();

    value &&
      (await store.dispatch(
        getFaq({
          history: Router,
          categoryId: value,
        })
      ));

    let prop = {};
    prop.accessToken =
      store.getState().loginReducer.verifyOtpSuccess?.Response?.access_token;

    prop.TransactionList = store.getState().wallet?.walletHistory;
    prop.customerId =
      store.getState().loginReducer?.verifyOtpSuccess?.CustomerId;
    prop.TransactionError = store.getState().wallet?.walletHistoryError;
    prop.wallet = store.getState().wallet;
    prop.referEarn = store.getState().referEarn;
    prop.customerMobileNo =
      store.getState().loginReducer?.verifyOtpSuccess?.CustomerDto?.mobileNo;
    prop.faqData = store.getState().faqData;

    return { ...prop };
  }
);

const mapStateToProps = (state) => ({
  // TransactionList: state.wallet?.walletHistory,
  // customerId: state.loginReducer?.verifyOtpSuccess?.CustomerId,
  // accessToken: state.loginReducer?.verifyOtpSuccess?.Response?.access_token,
  // TransactionError: state.wallet?.walletHistoryError,
  // wallet: state.wallet,
  // referEarn: state.referEarn,
  // customerMobileNo: state.loginReducer?.verifyOtpSuccess?.CustomerDto?.mobileNo,
  // faqData: state.faqData,
});

export default withRouter(
  connect(mapStateToProps, {
    walletThunk,
    walletHistoryThunk,
    setCurrentPage,
    redeemCodeThunk,
    getFaqCategory,
    getFaq,
  })(TMWallet)
);
