import React from "react";
import { connect } from "react-redux";
import { withRouter } from "next/router";
import moment from "moment";
import {
  ArticleContent,
  FirstRow,
  ShareSocial,
  HeaderBlog,
  ArticleContentWrapper,
  ArticleImage,
  TagsWrapper,
} from "../../pageComponents/blog/SubComponent/ArticleDetails.style";
import {
  WhatsappShareButton,
  TwitterShareButton,
  FacebookShareButton,
} from "react-share";
import {
  ArticleDetailsShimmerImageSection,
  ArticleDetailsShimmerLine,
  ArticleDetailsShimmerLineSection,
} from "../../pageComponents/blog/SubComponent/ArticleDetailsShimmer";
import { ArticleWrapper } from "../../pageComponents/blog/HeadlessHealthBlogs.style";
import facebook from "../../src/Assets/facebook.svg";
import twitter from "../../src/Assets/twitter.svg";
import whatsapp from "../../src/Assets/whatsapp.svg";

import {
  fetchArticleDetails,
  fetchCategoryList,
} from "../../redux/HeadlessHealth/Action";
import { setCurrentPage } from "../../redux/Pincode/Actions";
import { Helmet } from "react-helmet";
import "../../pageComponents/blog/SubComponent/ArticleDetails.css";
import ArticleSideBarComponent from "../../pageComponents/blog/SubComponent/ArticleSideBarComponent";
import optimizeImage from "../../components/Helper/OptimizeImage";
import { wrapper } from "../../redux/store";
export class ArticleDetails extends React.Component {
  state = {};

  componentDidMount = () => {
    let html = document.getElementsByTagName("html");
    if (html?.length > 0) {
      html[0].style.scrollBehavior = "smooth";
    }
    this.props.setCurrentPage({ currentPage: "health-details" });
  };

  render() {
    let { article } = this.props;

    let title = article?.metaDetails?.og_title;
    let description = article?.metaDetails?.og_description;

    if (!title) {
      title = article?.name;
    }
    if (!description) {
      description = article?.name;
    }
    return (
      <>
        {article && (
          <Helmet
            htmlAttributes={{ lang: "en" }}
            title={`${title}`}
            meta={[
              {
                name: "description",
                content: `${description}`,
              },
            ]}
          >
            <script type="application/ld+json">
              {`
                "@context": "https://schema.org/",
                "@type": "Article",
                "name": "${title}",
                "description": "${description}",
                "author": "${article?.authorName}",
              `}
            </script>
          </Helmet>
        )}
        <HeaderBlog>
          <div className="health-header">
            {this.props.isLoading ? (
              <ArticleDetailsShimmerLine />
            ) : (
              article && (
                <>
                  <h1>{article.name}</h1>
                  <p className="article-brief">
                    By {article.authorName} |{" "}
                    {moment(article.createdOn).format("Do MMM YYYY")}
                  </p>
                  <p className="article-tags-header">
                    {article.category.map((e, i) => {
                      if (i + 1 == article.category.length) {
                        return (
                          <a target="_blank" href={`/blog/category/${e.slug}`}>
                            {e.name + ""}
                          </a>
                        );
                      } else {
                        return (
                          <a target="_blank" href={`/blog/category/${e.slug}`}>
                            {e.name + ", "}
                          </a>
                        );
                      }
                    })}
                  </p>
                </>
              )
            )}
          </div>
        </HeaderBlog>
        <ArticleWrapper>
          <ArticleContentWrapper>
            <FirstRow>
              {this.props.isMobile ? (
                <ArticleSideBarComponent initCategory={this.props.category} />
              ) : (
                ""
              )}
              <ShareSocial>
                <WhatsappShareButton
                  url={`https://www.truemeds.in/blog/${article?.url}`}
                  title={`\nRead more at Truemeds App:\n`}
                  style={{ margin: "5px 5px" }}
                >
                  <img src={whatsapp} alt="social"></img>
                </WhatsappShareButton>
                <FacebookShareButton
                  title={"Test"}
                  description={"Title Desc"}
                  url={`https://www.truemeds.in/blog/${article?.url}`}
                  style={{ margin: "5px 5px" }}
                >
                  <img
                    src={facebook}
                    alt="social"
                    style={{ width: "35px", height: "35px" }}
                  ></img>
                </FacebookShareButton>
                <TwitterShareButton
                  title={"Test"}
                  description={"Title Desc"}
                  url={`https://www.truemeds.in/blog/${article?.url}`}
                  style={{ margin: "5px 5px" }}
                >
                  <img
                    src={twitter}
                    alt="social"
                    style={{ width: "35px", height: "35px" }}
                  ></img>
                </TwitterShareButton>

                {/* <img src={shareoption} alt="social"></img> */}
              </ShareSocial>
              <div className="article-image-content-section">
                {this.props.isLoading ? (
                  <ArticleDetailsShimmerImageSection />
                ) : article ? (
                  article?.image ? (
                    <ArticleImage>
                      <img
                        src={optimizeImage(article?.image, "650")}
                        alt="Banner"
                        style={{
                          width: "100%",
                          objectFit: "cover",
                          objectPosition: "100% 50%",
                          height: "auto",
                        }}
                        onError={this.addDefaultSrc}
                      ></img>
                    </ArticleImage>
                  ) : (
                    ""
                  )
                ) : (
                  <ArticleDetailsShimmerImageSection />
                )}
                <ArticleContent>
                  {this.props.isLoading ? (
                    <ArticleDetailsShimmerLineSection />
                  ) : (
                    <section
                      className="htmlSection"
                      dangerouslySetInnerHTML={{ __html: article?.description }}
                    ></section>
                  )}
                  {article ? (
                    article?.tagsList.length > 0 ? (
                      <TagsWrapper>
                        <div>Tags:</div>
                        {article?.tagsList.length > 0
                          ? article.tagsList.map((e) => {
                              return (
                                <a href={`/blog/tag/${e.slug}`} target="_blank">
                                  <div>{"#" + e.name + " "}</div>
                                </a>
                              );
                            })
                          : ""}
                      </TagsWrapper>
                    ) : (
                      ""
                    )
                  ) : (
                    <ArticleDetailsShimmerLineSection />
                  )}

                  {/* {article?.url ? (
                    <a
                      href={article?.url}
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      Continue Reading
                    </a>
                  ) : null} */}
                </ArticleContent>
              </div>
            </FirstRow>
          </ArticleContentWrapper>
          {!this.props.isMobile ? (
            <ArticleSideBarComponent initCategory={this.props.category} />
          ) : (
            ""
          )}
        </ArticleWrapper>
      </>
    );
  }
}

ArticleDetails.getInitialProps = wrapper.getInitialPageProps(
  (store) => async (props) => {
    let slug = props.query.id;
    let params = {};
    await store.dispatch(fetchArticleDetails(slug, null));
    await store.dispatch(fetchCategoryList(params));
    let prop = {};
    prop.article = store.getState().articleHlDetails?.article;
    prop.articleCustomListError =
      store.getState().articleHlDetails?.articleCustomListError;
    prop.category = store.getState().articleHlDetails?.categoryList;
    prop.isLoading = false;
    return { ...prop };
  }
);

let mapStateToProps = (state) => ({
  accessToken: state.loginReducer.verifyOtpSuccess?.Response?.access_token,
  article: state.articleHlDetails?.article,
  category: state.articleHlDetails?.categoryList,
  articleCustomListError: state.articleHlDetails?.articleCustomListError,
});
export default withRouter(
  connect(mapStateToProps, { fetchArticleDetails, setCurrentPage })(
    ArticleDetails
  )
);
