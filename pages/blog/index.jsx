import React from "react";
import { connect } from "react-redux";
import Router, { withRouter } from "next/router";
import {
  HealthWrapper,
  Header,
  ArticleList,
  ArticleWrapper,
} from "../../pageComponents/blog/HeadlessHealthBlogs.style";
import { PaginationWrap } from "../../pageComponents/blog/Pagination.style";
import _ from "underscore";
import ArticleCard from "../../pageComponents/blog/SubComponent/ArticleCard";
import ArticleCardShimmer from "../../pageComponents/blog/SubComponent/ArticleCardShimmer";
import ArticleSideBarComponent from "../../pageComponents/blog/SubComponent/ArticleSideBarComponent";
import { message } from "antd";
import ReactPaginate from "react-paginate";
import {
  fetchArticleListDetails,
  fetchCategoryList,
} from "../../redux/HeadlessHealth/Action";
import { setCurrentPage } from "../../redux/Pincode/Actions";
import { wrapper } from "../../redux/store";
import window from "global";
import { eventHomepageReadArticles } from "../../src/Events/Events";

export class HeadlessHealthBlog extends React.Component {
  state = {
    option: 1,
    filterList: this.props.article ? this.props.article : [],
    activeSlug: "",
    totalPostCount: 0,
    isLoading: true,
  };
  fetchArticle = (props) => {
    this.props.fetchArticleListDetails(props).then(() => {
      if (this.props.articleListError) {
        if (
          this.props.articleListError &&
          this.props.articleListError.response &&
          this.props.articleListError.response.status === 504
        ) {
          message.error(
            "The page number requested is larger than the number of pages available."
          );
        } else {
          message.error("Error! Please try again in sometime");
        }
      } else {
        this.setState({
          filterList: this.props.article,
          isLoading: false,
        });
      }
    });
  };
  componentDidMount = () => {
    let html = document.getElementsByTagName("html");
    if (html?.length > 0) {
      html[0].style.scrollBehavior = "smooth";
    }
    /* if (!this.props.blogToken) {
        this.props.fetchBlogToken();
      } else {
        this.props.fetchArticleListDetails(this.props.blogToken);
      } */
    this.props.setCurrentPage({ currentPage: "health" });

    // this.fetchArticle({ accessToken: this.props.accessToken });
  };

  componentDidUpdate(prevProps, prevState) {}

  specificArticle = (data) => {
    Router.push(`/blog/${data.slug}`, undefined, { shallow: true });
    // Event_Truemeds
    eventHomepageReadArticles();
  };
  selectOption = (id) => {
    let cnt = 0;
    let result = this.props.article?.filter((data) => {
      if (data.categories.includes(id) && cnt <= 4) {
        cnt++;
        return true;
      } else {
        return false;
      }
    });
    this.setState({ option: id, filterList: result });
  };
  handlePageClick(e) {
    window.scrollTo(0, 0);
    this.fetchArticle({
      accessToken: this.props.accessToken,
      page: e.selected + 1,
    });
  }
  render() {
    let { article } = this.props;
    let { filterList, totalPostCount } = this.state;
    let catTemp = [];
    if (article?.length > 0) {
      article.map((e) => {
        catTemp.push({ id: e.categoryId, name: e.categoryName });
      });
      catTemp = _.uniq(catTemp, "id");
    }
    return (
      <>
        <HealthWrapper>
          <Header>
            <h5>Blog</h5>
          </Header>
          {/* <FliterList>
              {category?.map((data) => {
                if (data.parent == 0) {
                  return (
                    <HealthHeader
                      id="healthHeader"
                      key={data.id}
                      className={
                        option == data.id
                          ? "myOrderSelectedType"
                          : "myOrderUnselectedType"
                      }
                      onClick={() => {
                        this.selectOption(data.id);
                        this.setState({
                          activeSlug: data.slug,
                        });
                      }}
                    >
                      <span
                        className={
                          option == data.id
                            ? "myOrderSelectedTypeText"
                            : "myOrderUnselectedTypeText"
                        }
                      >
                        {data.name}
                      </span>
                    </HealthHeader>
                  );
                }
              })}
            </FliterList> */}
        </HealthWrapper>
        <ArticleWrapper>
          <ArticleList>
            {this.props.isMobile ? (
              <ArticleSideBarComponent
                getTotalCount={(e) => this.setState({ totalPostCount: e })}
                initCategory={this.props.category}
              />
            ) : (
              ""
            )}
            {filterList?.length > 0 ? (
              filterList.map((data, index) => {
                return (
                  <ArticleCard
                    data={data}
                    specificArticle={this.specificArticle}
                    lastIndex={filterList.length !== index + 1}
                  />
                );
              })
            ) : this.props.isLoading || this.state.isLoading ? (
              <ArticleCardShimmer />
            ) : (
              ""
            )}
            <PaginationWrap>
              <ReactPaginate
                breakLabel="..."
                nextLabel=">"
                onPageChange={(e) => {
                  this.handlePageClick(e);
                }}
                pageRangeDisplayed={3}
                pageCount={
                  this.props.totalPages
                    ? this.props.totalPages
                    : Math.ceil(totalPostCount / 10)
                }
                previousLabel="<"
                renderOnZeroPageCount={null}
                containerClassName={"containerWrap"}
                pageClassName="page"
                pageLinkClassName="pageLink"
                activeClassName="active"
                activeLinkClassName="activeLink"
                previousClassName="previous"
                nextClassName="next"
                previousLinkClassName="previousLink"
                nextLinkClassName="nextLinkClassName"
                disabledClassName="disabled"
                disabledLinkClassName="disabledLink"
              />
            </PaginationWrap>
          </ArticleList>
          {!this.props.isMobile ? (
            <ArticleSideBarComponent
              getTotalCount={(e) => this.setState({ totalPostCount: e })}
              initCategory={this.props.category}
            />
          ) : (
            ""
          )}
        </ArticleWrapper>
      </>
    );
  }
}

//server calls
HeadlessHealthBlog.getInitialProps = wrapper.getInitialPageProps(
  (store) => async (props, req) => {
    let params = {};
    await store.dispatch(fetchArticleListDetails(params));
    await store.dispatch(fetchCategoryList(params));
    let prop = {};
    prop.article = store.getState().articleHlDetails?.articleList;
    return { ...prop };
  }
);

let mapStateToProps = (state) => ({
  accessToken: state.loginReducer?.verifyOtpSuccess?.Response?.access_token,
  article: state.articleHlDetails?.articleList,
  totalPages: state.articleHlDetails?.totalPages,
  totalPosts: state.articleHlDetails?.totalPosts,
  articleListError: state.articleHlDetails?.articleListError,
  category: state.articleHlDetails?.categoryList,
  isLoading: state.articleHlDetails?.isLoading,
});

export default connect(mapStateToProps, {
  fetchArticleListDetails,
  setCurrentPage,
})(HeadlessHealthBlog);
