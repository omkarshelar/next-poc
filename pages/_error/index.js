import React, { Component } from "react";
import {
  NotFoundWrap,
  ImageWrap,
  TextWrap,
  Title,
  SubTitle,
  SearchMedBtn,
} from "../../pageComponents/_error/NotFound.style";
import notFound from "../../src/Assets/notFound.svg";
import window from "global";

export default class NotFound404 extends Component {
  state = {
    timer: 3,
  };

  // componentDidMount = () => {
  //   message.loading(`Redirecting to home page`, 0);

  //   setTimeout(() => {
  //     Router.push("/");
  //     message.destroy();
  //   }, 2000);
  // };

  render() {
    return (
      <>
        <NotFoundWrap>
          <ImageWrap>
            <img src={notFound} />
          </ImageWrap>
          <TextWrap>
            <Title>{"404"}</Title>
            <SubTitle>{"Sorry the requested page does not exist"}</SubTitle>
            <SearchMedBtn
              onClick={() => {
                window.location.href = window.location.origin + "/?search=true";
              }}
            >
              Buy Medicines
            </SearchMedBtn>
          </TextWrap>
        </NotFoundWrap>
      </>
    );
  }
}
