import React, { Component } from "react";
import { connect } from "react-redux";
import CustomButton from "../../../components/WebComponents/CustomButton/CustomButton";
import { OrderTicketStatusWrapper } from "../../../pageComponents/help/HelpSubComponent/OrderTicket/OrderTicketStatus/OrderTicketStatus.style";
import { wrapper } from "../../../redux/store";
import Router, { withRouter } from "next/router";
import { parseCookies } from "../../../components/Helper/parseCookies";
import { setCurrentPage } from "../../../redux/Pincode/Actions";
export class OrderTicketStatus extends Component {
  // componentDidMount() {
  //   this.props.setCurrentPage({ currentPage: "help" });
  // }
  render() {
    return (
      <OrderTicketStatusWrapper>
        <div>
          <p>How can we help you?</p>
          <div>
            {this.props.accessToken && this.props.isUserLogged && (
              <CustomButton
                style={{ width: "180px" }}
                delYes
                onClick={() => Router.push("/helpwithmyorder")}
              >
                Help With My Order
              </CustomButton>
            )}

            <CustomButton delNo onClick={() => Router.push("/help")}>
              More
            </CustomButton>
          </div>
        </div>
      </OrderTicketStatusWrapper>
    );
  }
}

//? Server side calls

OrderTicketStatus.getInitialProps = wrapper.getInitialPageProps(
  (store) => async (props) => {
    const cookies = parseCookies(props.req);

    await store.dispatch(setCurrentPage({ currentPage: "help" }));

    let prop = {};
    prop.accessToken =
      store.getState().loginReducer.verifyOtpSuccess?.Response?.access_token;
    prop.isUserLogged = store.getState().loginReducer?.isUserLogged;

    return { ...prop };
  }
);

let mapStateToProps = (state) => ({
  accessToken: state.loginReducer?.verifyOtpSuccess?.Response?.access_token,
  isUserLogged: state.loginReducer?.isUserLogged,
});

export default withRouter(
  connect(mapStateToProps, { setCurrentPage })(OrderTicketStatus)
);
