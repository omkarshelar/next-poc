import React, { useEffect } from "react";
import { connect } from "react-redux";
import Router, { withRouter } from "next/router";
import { helpDetailsThunk } from "../../../redux/Help/action";
import {
  TopicHeader,
  HelpTopicWrapper,
} from "../../../pageComponents/help/HelpTopic.Style";
import RightArrow from "../../../src/Assets/RightArrow.png";

const getId = (str) => {
  let id = str.split("--")[1];
  return Number(id);
};

function HelpTopic({ dispatch, ...props }) {
  let PageId = null;

  useEffect(() => {
    PageId = Router.query.HelpTopic;

    dispatch(helpDetailsThunk({ id: getId(PageId) }));
  }, [dispatch, PageId]);

  const convertTitle = (name, id) => {
    return (
      name
        .replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, "")
        .toLowerCase()
        .trimEnd()
        .split(" ")
        .join("-")
        .replace("--", "-") +
      "--" +
      id
    );
  };

  const goToSpecificIssue = (issue) => {
    Router.push({
      pathname: `/help/${Router.query.HelpTopic}/${convertTitle(
        issue.issues,
        issue.issuesId
      )}`,
      query: {
        answer: issue.answers,
        categoryId: issue.categoryId,
        issues: issue.issues,
        issuesId: issue.issuesId,
      },
    });
  };

  return (
    <>
      <TopicHeader> Select an Issue</TopicHeader>
      <HelpTopicWrapper>
        {props.helpCategoryDetails?.Category.map((data) => (
          <div key={data.issuesId} onClick={() => goToSpecificIssue(data)}>
            <span> {data.issues}</span>
            <div>
              <img src={RightArrow} alt="Go"></img>
            </div>
          </div>
        ))}
      </HelpTopicWrapper>
    </>
  );
}

let mapStateToProps = (state) => ({
  helpCategoryDetails: state.help.HelpDetailsData,
});

export default withRouter(connect(mapStateToProps)(HelpTopic));
