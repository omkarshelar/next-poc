import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import Router, { withRouter } from "next/router";
import { SpecificIssueContainer } from "../../../pageComponents/help/SpecificIssueCard.style";
import {
  EnquiryWrapperBottom,
  EnclosingDiv,
} from "../../../pageComponents/help/Help.style";
import { helpDetailsThunk } from "../../../redux/Help/action";

const getId = (str) => {
  let id = str && str.split("--")[1];
  return Number(id);
};

const getNewId = (str) => {
  let substr = str && str.substring(0, str.lastIndexOf("/"));
  let id = substr.split("--")[1];
  return Number(id);
};

const getDetailsById = (id, arr) => {
  let obj = {};
  for (let i in arr) {
    let details = arr[i];
    if (details.issuesId === id) {
      obj = details;
      break;
    }
  }
  return obj;
};

function SpecificIssueCard({ dispatch, location, match, ...props }) {
  const [helpDetails, setHelpDetails] = useState([]);
  const [pageId, setPageId] = useState(null);
  const [pageData, setPageData] = useState(null);

  useEffect(() => {
    setPageId(Router.query.SpecificIssueCard);
    setPageData(Router.query);
    if (pageData && !pageData?.issues) {
      dispatch(helpDetailsThunk({ id: getNewId(pageId) }));
    }
  }, [dispatch, pageId, pageData?.categoryId]);

  useEffect(() => {
    setPageId(Router.query.SpecificIssueCard);
    setPageData(Router.query);
    if (props.helpCategoryDetails?.Category?.length > 0) {
      setHelpDetails(
        getDetailsById(
          getId(pageData && pageData?.categoryId),
          props.helpCategoryDetails.Category
        )
      );
    }
  }, [props.helpCategoryDetails?.Category]);

  return (
    <EnclosingDiv>
      <SpecificIssueContainer>
        <span>
          {helpDetails?.issues
            ? helpDetails.issues
            : pageData && pageData?.issues}
        </span>
        <p>
          {helpDetails?.answers
            ? helpDetails.answers
            : pageData && pageData?.answer}
        </p>
      </SpecificIssueContainer>
      <EnquiryWrapperBottom>
        <span>For Enquiries Call</span>
        <div className="info-enquiry">
          <span>
            <a
              style={{
                color: "#179b62",
                textDecoration: "none",
                fontWeight: 600,
              }}
              href="tel:022-48977965"
            >
              022-48977965
            </a>
          </span>
          <span>(Between 9am-9pm)</span>
        </div>
      </EnquiryWrapperBottom>
    </EnclosingDiv>
  );
}

let mapStateToProps = (state) => ({
  helpCategoryDetails: state.help.HelpDetailsData,
});

export default withRouter(connect(mapStateToProps)(SpecificIssueCard));
