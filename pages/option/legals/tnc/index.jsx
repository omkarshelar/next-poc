import React, { useEffect } from "react";
import { connect } from "react-redux";
import Router, { withRouter } from "next/router";
import { legalActionThunk } from "../../../../redux/Legals/action";

function Tnc(props) {
  useEffect(() => {
    let isTmWallet = false;
    if (Router.query?.isTMWALLET) {
      isTmWallet = true;
    }
    props.legalActionThunk(Router, isTmWallet);
  }, []);

  return (
    <>
      <div
        style={{
          width: "95%",
          padding: "2rem 0",
          margin: "0 auto",
          whiteSpace: "pre-line",
        }}
      >
        {props.isHeader ? (
          <div>
            <h5
              style={{
                margin: "1rem 0",
              }}
            >
              Terms and Conditions
            </h5>
          </div>
        ) : (
          ""
        )}
        <div>
          <h5
            style={{
              margin: "1rem 0",
            }}
          >
            {props?.tnc?.header}
          </h5>
        </div>
        {/* <h5>(applicable from 09/09/2019)</h5>
      <p>
        BY CONTINUING TO ACCESS AND USE THIS WEBSITE/MOBILE APPLICATION YOU
        CONFIRM THAT YOU ACCEPT OUR TERMS AND CONDITIONS OF USE SET OUT BELOW.
        IF YOU DO NOT ACCEPT THE TERMS AND CONDITIONS, YOU MUST LEAVE THIS
        WEBSITE/MOBILE APPLICATION.
      </p>
      <h5>About us</h5>
      <p>
        Truemeds.in website and the mobile application is owned and provided by
        Intellihealth Solutions Private Limited (hereinafter “Truemeds”), a
        company incorporated under the Companies Act, 2013. If you need any
        information or have a complaint about this website/mobile application or
        any of our services, please contact us by writing to us at
        support@truemeds.in or calling on 022-48977965.{" "}
      </p>
      <h5>Professional Standards</h5>
      <p>
        Truemeds services are rendered either from its own licensed premises or
        from the licensed premises of its associates/network pharmacies which
        are undertaken under the strict supervision of the qualified and
        registered pharmacists. All pharmacists are bound by codes of
        professional ethics and conduct.
      </p>
      <h5>Privacy</h5>
      <p>
        Your privacy and that of any other person whose information you provide
        to us are important to us. Please see our privacy policy for details of
        what information we collect and how we will use and protect it.{" "}
      </p>
      <h5>Registration</h5>
      <p>
        By registering and using the services you agree that you are eighteen
        years or above and you are not debarred by any law to contract and you
        agree to have read and accepted the terms and conditions provided
        herein. Visitors to this website/mobile application are required to
        register in order to use its facilities/ services. We are not under any
        obligation to accept a request for registration and reserve the right to
        suspend or terminate access at any time if your continued use is
        believed to prejudice us or other users. By registering to use this
        website /mobile application you confirm that the information you provide
        during the registration process is accurate and complete. You agree to
        update your registration details promptly if there are any changes. All
        registration information you provide will be kept secure and processed
        in accordance with our privacy policy
      </p>
      <h5>Username and password</h5>
      <p>
        During the registration process, you may be allocated, or invited to
        choose, your own username and password which will be unique to you. You
        are responsible for keeping your username and password confidential and
        for ensuring that they are not used by any other person.
      </p>
      <h5>Offers, promotions, software, and downloads</h5>
      <p>
        Truemeds and other third parties with whom we have a business
        relationship may occasionally promote their goods or services on the
        website or mobile application or through other direct marketing
        initiatives or may make software and other materials available for you
        to purchase or download. Whilst we try to encourage third parties to
        offer good quality products, and services and materials at competitive
        prices we have no control over them or other third parties, we do not
        endorse the products or services they offer or give you any assurance
        that they will be suitable for your needs. It is your responsibility to
        satisfy yourself in this regard and we have no liability in connection
        with the same. All promotions are for a limited period and subject to
        special terms and conditions, which are in addition and not to the terms
        and conditions stated herein.
      </p>
      <h5>Ownership of materials and license terms</h5>
      <p>
        This website or mobile application, the materials and software on it, or
        provided to you through it are protected by copyright, trademark and
        other intellectual property rights and laws throughout the world and are
        owned by or are licensed to Truemeds and/or third parties. You are
        permitted to display the materials on this mobile application on a
        computer screen/mobile screen and, save for restricted access documents,
        to download and print a hard copy for your personal use or for obtaining
        products or services from us provided you do not alter or remove any of
        the content or any part of the website/mobile application without our
        express permission to do so and that you do not change or delete any
        copyright, trademark or other proprietary notices.
      </p>
      <h5>You agree not to</h5>
      <p>
        Copy, reproduce, store (in any medium or format), distribute, transmit,
        modify, create derivative works from all or any part of this
        website/mobile application or the materials or software on it, or
        provided to you through it without our prior written consent (which may
        be given or withheld in our absolute discretion) use this website/mobile
        application or any of the materials or software on it, or provided to
        you through it, for: any unlawful purpose or in contravention of
        applicable law commercial exploitation without our prior written consent
        any purpose or in any manner that may give a false or misleading
        impression of us, our staff or our services use, upload or transmit any
        material that is defamatory, offensive, obscene or otherwise unlawful,
        or which may cause offence or distress, or which may affect or infringe
        the rights of any other person any device, software, file or mechanism
        which may interfere with the proper operation of this website or our
        systems establish a link to this mobile application from any other
        website, intranet or extranet site without our prior written consent
        decompile, disassemble or reverse engineer (or attempt to do any of
        them) any of the software or other materials provided on or through this
        website/mobile application do anything that may interfere with or
        disrupt this website/mobile application or our service encourage or
        permit others to do any of the above In the event that you do not comply
        with the above restrictions, any person affected by your actions may
        bring a claim against you and/or Truemeds. We will pursue a claim
        against you for any losses and costs (including legal costs) we may
        suffer as a result of your actions.
      </p>
      <p>
        Suitability of materials We do not give any assurance that the materials
        provided or available to you on or through this website/mobile
        application are suitable for your requirements or that they will be
        secure, error or virus free and we will have no liability in respect of
        those materials.
      </p>
      <h5>Mobile application availability</h5>
      <p>
        This mobile application is provided free of charge and we make no
        guarantee that it will be uninterrupted or error-free. We reserve the
        right to modify, suspend or withdraw the whole or any part of the
        website/mobile application or any of its content at any time without
        notice and without incurring any liability.
      </p>
      <h5>Links from this website/mobile application</h5>
      <p>
        We may, from time to time, provide links from this mobile application to
        other websites that are owned and controlled by third parties. These
        links are provided only for your convenience and we have no control over
        and will have no liability in respect of those websites.
      </p>
      <h5>Surveys, Contests & Referrals</h5>
      <p>
        From time-to-time, our site requests information from users via surveys
        or contests. Participation in these surveys, contests or referrals
        programs is completely voluntary and the user therefore, you have a
        choice whether or not to disclose any information requested. Information
        requested may include contact information (such as name and delivery
        address), and demographic information (such as postcode, age level).
        Contact information will be used to notify the winners and award prizes.
        Survey information will be used for purposes of monitoring or improving
        the functionality of the site. The terms and conditions for each survey
        and contest may differ or otherwise amended and cancelled at the sole
        discretion of Truemeds.
      </p>
      <h5>Monitoring</h5>
      <p>
        We may monitor activity and content on this website/mobile application
        and may take any action we consider appropriate if we suspect you may be
        in breach of these Terms and Conditions including suspending, attaching
        conditions to or terminating your access and/or notifying the
        authorities or relevant regulators of your activities.
      </p>
      <h5>Security</h5>
      <p>
        We employ security technology as detailed in our privacy policy however,
        Internet transmissions are never completely private or secure and there
        is a risk, therefore, that any message or information you send to us
        from this website may be intercepted and potentially read by others. We
        will have no liability in respect of any transmissions you send to us
        and you do so entirely at your own risk.
      </p>
      <h5>Accuracy of Information</h5>
      <p>
        We take care to ensure that all information available on our
        website/mobile application about our business, services and any products
        mentioned is accurate. However, these are continually developing and,
        occasionally, the information may be out of date. Medical, commercial
        and legal practice change frequently and the content on this
        website/mobile application, in any newsletters and in other items
        offering guidance have been prepared for general interest only and are
        not a substitute for specific medical, legal or other professional
        advice and should not be read or used as such. For accurate up-to-date
        information, you should contact us and/or your doctor directly.
      </p>
      <h5>Disclaimer</h5>
      <p>
        TRUEMEDS DOES NOT WARRANT OR REPRESENT THAT THE MATERIAL ON THIS
        WEBSITE/MOBILE APPLICATION IS ACCURATE, COMPLETE OR CURRENT OR THAT THE
        WEBSITE WILL BE FREE OF DEFECTS OR VIRUSES. NOTHING CONTAINED IN THE
        PAGES OF THIS WEBSITE/MOBILE APPLICATION SHOULD BE CONSTRUED AS MEDICAL,
        COMMERCIAL, LEGAL OR OTHER PROFESSIONAL ADVICE. DETAILED PROFESSIONAL
        ADVICE SHOULD BE OBTAINED BEFORE TAKING OR REFRAINING FROM ANY ACTION
        BASED ON ANY OF THE INFORMATION OR MATERIAL CONTAINED IN THIS
        WEBSITE/MOBILE APPLICATION OR ANY COMMUNICATIONS PROVIDED TO YOU AS A
        RESULT OF YOUR REGISTRATION.
      </p>
      <h5>Liability</h5>
      <p>
        WE WILL NOT BE LIABLE FOR ANY LOSS OR DAMAGE (IN CONTRACT, NEGLIGENCE OR
        OTHERWISE) WHERE: THERE IS NO BREACH OF A LEGAL DUTY OF CARE OWED TO YOU
        BY US; THE LOSS OR DAMAGE IS NOT A REASONABLY FORESEEABLE RESULT OF ANY
        SUCH BREACH; OR ANY LOSS OR DAMAGE OR INCREASE IN LOSS OR DAMAGE RESULTS
        FROM A BREACH BY YOU OF THESE TERMS AND CONDITIONS.THE MAXIMUM LIABILITY
        OF TRUEMEDS SHALL BE LIMITED TO THE COST OF THE PRODUCTS PURCHASED OR
        SERVICES AVAILED BY THE CUSTOMER, TRUEMEDS OR IT’S DIRECTORS SHALL NOT
        BE LIABLE FOR ANY INDIRECT, CONSEQUENTIAL OR OTHER DAMAGES CAUSED TO THE
        USER Third-party rights Nothing in these Terms and Conditions is
        intended to nor shall it confer a benefit on any third party under the
        Contracts and a person who is not a party to these Terms and Conditions
        has no rights to enforce them.
      </p>
      <h5>Waiver</h5>
      <p>
        No delay or decision not to enforce rights under these terms and
        conditions will constitute a waiver of the right to do so and will not
        affect rights in relation to any subsequent breach.
      </p>
      <p>
        WE RESERVE THE RIGHT TO CHANGE THESE TERMS AND CONDITIONS AT ANY TIME.
        THE NEW VERSION WILL BE POSTED ON THIS WEBSITE/MOBILE APPLICATION AND
        WILL TAKE EFFECT IMMEDIATELY UPON POSTING IF YOU USE THE WEBSITE/MOBILE
        APPLICATION AFTER THE NEW TERMS AND CONDITIONS HAVE COME INTO EFFECT,
        YOU WILL BE INDICATING YOUR AGREEMENT TO BE BOUND BY THE NEW TERMS AND
        CONDITIONS
      </p>
      <h5>PRESCRIPTION ORDER</h5>
      <p>
        THE FOLLOWING TERMS APPLY TO YOUR USE OF THE TRUEMEDS PRESCRIPTION
        ORDERING SERVICE. THESE ARE ADDITIONAL TO AND SUPPLEMENT OUR GENERAL
        WEBSITE/MOBILE APPLICATION TERMS AND CONDITIONS WHICH ALSO APPLY. IF YOU
        HAVE ANY QUESTIONS CONCERNING THE SERVICE, PLEASE CONTACT US.
      </p>
      <h5>Prescriptions for persons other than yourself </h5>
      <p>
        If the prescription is of a person other than yourself, you will need to
        have the authority of the person whose prescription it is to have it
        dispensed by us. By placing an order to dispense another person’s
        prescription you confirm that you have their authority to do so.
      </p>
      <h5>Age Restrictions</h5>
      <p>
        We only accept Prescription Orders from customers who are 18 years old
        or over although they may order prescriptions for persons who are under
        18. By placing an order, you confirm that you are at least 18 and
        legally eligible to contract.
      </p>
      <h5>Residency </h5>
      <p>
        We accept Orders from and despatch orders to addresses in selected
        locations in India.
      </p>{" "}
      <p>
        Binding agreement No Order placed by you will be binding on us until we
        have confirmed the order to you by phone, email or in writing. We
        reserve the right to reject any order. All orders are delivered subject
        to receipt of valid prescription and availability.{" "}
      </p>
      <h5>Verification of prescription</h5>
      <p>
        When we receive an original prescription from you, our pharmacist will
        verify it against the information provided to us at the time the order
        was placed. In the event that the information does not match with your
        original order as per the prescription, we may try to contact you using
        the information in your registration. If we cannot contact you and we
        are unable to dispense the item(s) on your Prescription Order we reserve
        the right to return your prescription to you.
      </p>
      <h5>Non-Receipt of Prescription </h5>
      <p>
        If you place a Prescription Order and we do not receive the relevant
        original prescription, we will not have any liability to you and it will
        be your responsibility to contact us within 7 days regarding your
        prescription order.
      </p>
      <h5>Validation Of Prescription</h5>
      <p>
        Validation of prescription services through our partner Registered
        Medical Practitioner shall be made available to the User only upon
        request of the User and in the following situations: The existing
        prescription has expired, and the User is unable to get an appointment
        with his Registered Medical Practitioner; or The prescription is not
        legible, and the prescribing Registered Medical Practitioner is
        unavailable to validate the prescription; or The prescription is not as
        per the standards defined by the Medical Council of India, including,
        missing Doctor details, patient details etc; or If the prescription
        contains schedule X drugs / unusual quantities. The User understands and
        agrees that the validation of prescription services provided herein are
        only on request and are intended for general purposes only and should
        not be used in case of medical emergencies and serious illness issues
        where physical consultation is recommended and at no point these
        services are intended to replace the physical consultation. The User
        also agrees and understands that for providing the validation of
        prescription service Truemeds will have to share the prescription with
        its partner Registered Medical Practitioner and by requesting the
        services the User confirms its consent to share his/her prescription
        with Truemeds's partner Registered Medical Practitioner. Upon the
        request of the User, the prescription of the User shall be shared with
        Truemeds's partner Registered Medical Practitioner and post-consultation
        or review of the prescription, if the Registered Medical Practitioner
        deems fit, he/she may validate the prescription shared with him/her of
        the User. However, at all time the Registered Medical Practitioner has
        the discretion to refuse consultation or validation of the prescription
        if the Registered Medical Practitioner is of the opinion that a physical
        consultation is required before a prescription is validated. User agrees
        that the Registered Medical Practitioner will use the technology of
        Truemeds for rendering the above services and they share the information
        with Truemeds of its affiliated partners for rendering the services.
      </p>
      <h5>Disclaimer</h5>
      <p>
        The User understands and agrees Truemeds is providing a technology
        service as an intermediary and that all the services are provided to the
        User directly by the Registered Medical Practitioner with regard to any
        health issues, consultation, validation, unsatisfactory services, etc.
        availed by the User under the above-mentioned clause services are
        between the User and the Registered Medical Practitioner and that at no
        point in time Truemeds shall be held responsible for the same. Further,
        notwithstanding anything contained herein, Truemeds shall not be liable
        for: any medical negligence, prescription of any wrong medication or
        treatment on part of the Registered Medical Practitioner; any type of
        inconvenience suffered by the User due to a failure on the part of the
        Registered Medical Practitioner in providing the agreed services or to
        make himself/herself available at the appointed time, inappropriate
        treatment, or similar difficulties; any misconduct or inappropriate
        behaviour by the Registered Medical Practitioner; cancellation or
        rescheduling of booked appointment; any medical eventualities that might
        occur subsequent to using the services of the Registered Medical
        Practitioner.
      </p>
      <h5>Subscription</h5>
      <p>
        Under the subscription model, User can subscribe for his/her/its
        medicine requirement by choosing a subscription plan that best suit the
        User's needs and the User can customise the same according to the User's
        requirement. All orders under the subscription plan shall be
        automatically placed in accordance with the chosen subscription plan.
        All orders under the subscription model shall be subject to all the
        other applicable terms and conditions mentioned herein along with all
        statutory requirements for processing an order.{" "}
      </p>
      <h5>Clubbing of offers</h5>
      <p>
        Any ongoing offer or promotional scheme cannot be clubbed with the
        orders placed under the subscription plan. All orders under the
        subscription plan shall be processed based on the standard discount and
        promotional scheme available at the time of processing any order under
        the subscription plan. Truemeds shall have the sole discretion to decide
        with regard to the offers or scheme that shall be applicable to the
        orders placed under the subscription plans.
      </p>
      <h5>Discount on Selected Products</h5>
      <p>
        Discounts provided on the website/mobile application are on selected
        products. Discounts may be changed on a product to product basis at the
        sole discretion of Truemeds & Truemeds will not be held liable for any
        issues/ inconvenience caused to users, resulting from these changes.
      </p>
      <h5>Non-Availability/Suitability of Products</h5>
      <p>
        If any of the items on your prescription are not available or are not
        suitable for dispensing through this service, we will try to contact you
        using the information in your registration to inform you about the same.{" "}
      </p>
      <h5>Delivery of prescription drugs </h5>
      <p>
        Prescription items can only be dispensed once we have received your
        original paper prescription or online prescription from Truemeds
        registered doctor. All items are delivered to the address provided by
        you in your registration or to an alternative address if directed by
        you. You acknowledge and accept that items will need to be signed for on
        delivery and authorise any person at your chosen delivery address to
        sign for the items as your authorised representative. In the event that
        the packaging is opened after delivery, we will have no liability to you
        for lost or damaged items or for what third parties find out about you
        as a result.
      </p>
      <h5>No Warranties </h5>
      <p>
        Truemeds makes no warranty that: (i) the application will meet your
        requirements; (ii) the application will be available on an
        uninterrupted, timely, secure, or error-free basis; (iii) any results
        that may be obtained from the use of the application or any services
        offered through the Website will be accurate or reliable (iv) Truemeds
        disclaims all warranties as to the accuracy, completeness or adequacy of
        such information.
      </p>
      <p>
        Truemeds disclaims all liabilities arising from manufacturing defects
        and the claims if any with regard to a defect in manufacturing or
        otherwise shall be made directly on the manufacturer whose information
        is provided on the product. Truemeds may at its sole discretion assist
        User in providing any information that may be required with regard to
        the claim.
      </p>
      <h5>Order, Acceptance & Delivery</h5>
      <p>
        Placement of an order either directly (in case of a repeat order or
        continuous drug administration as prescribed) shall constitute an
        agreement with us. All orders are subject to acceptance and confirmation
        of delivery date and time. Mere receipt of the prescription or order
        either directly from you or from the registered medical practitioner
        does not amount to acceptance of your order nor guarantee that the drugs
        will be delivered as per your requirement within a specific time. All
        orders are subject to availability of stocks. Truemeds reserves the
        right to share your prescription with its associated pharmacies for
        fulfilling your prescription, who will dispense the drugs as per the
        prescription and issue a valid sales receipt. All orders made by the
        customer shall be subject to various laws, including the Drugs and
        Cosmetics Act (as may be amended from time to time). We do not accept
        orders for certain drugs and cosmetics unless the same is prescribed by
        the registered medical practitioner licensed by the Medical Council of
        India. We do not verify the authenticity of the orders placed by you,
        where the prescriptions are received directly from a registered medical
        practitioner licensed by the Medical Council of India or your family
        doctor, who has placed the order on your behalf after your
        authorization. All drugs and cosmetics will be delivered through a
        delivery agent without disclosing your personal information or
        prescriptions in compliance with the applicable laws either directly to
        the patient or its authorised person. On delivery, Truemeds shall be
        entitled to full payment for the same. You agree that the prices
        mentioned on our website or mobile app are approximate estimates of the
        actual/exact price of the drugs. The actual/exact price of the drug will
        depend upon its batch number and will be mentioned in the invoice
        dispatched to you along with your order. In case a pre-payment has been
        made by you basis the price estimate at the time of placing the order,
        should there be a shortfall upon generation of the invoice, you
        undertake to pay the shortfall through any of the payment modes made
        available to you by us. In case the payment made by you basis the price
        estimate at the time of placing the order is more than the price as per
        the generated invoice, the excess payment will be refunded back to you
        in your bank account, credit card, debit card or wallet which may have
        been used by you at the time of making the payment.
      </p>
      <h5>Notification of errors</h5>
      <p>
        You should check the items dispensed to you carefully promptly upon
        receipt. If you believe there may have been a dispensing error, you
        should contact us immediately and should not take or use any of the
        items.{" "}
      </p>
      <h5>Delivery of fulfilled prescriptions </h5>
      <p>
        Prescription medicines will require a doorstep signature. Prescription
        items can only be dispensed once we have received your original paper
        prescription or online prescription from Truemeds registered doctor. All
        items are delivered to the address provided by you in your registration
        or to an alternative address if directed by you. You acknowledge and
        accept that items will need to be signed for on delivery and authorize
        any person at your chosen delivery address to sign for the items as your
        authorized representative. In the event that the packaging is opened
        after delivery, we will have no liability to you for lost or damaged
        items or for what third parties find out about you as a result.{" "}
      </p>
      <h5>Delivery and Packaging Costs </h5>
      <p>
        All orders with an order value of INR 500 or above will be delivered
        free of cost and no delivery charges shall be applicable on such orders
        provided that the delivery is within Truemeds serviceable areas.{" "}
      </p>
      <p>
        All orders with an order value less than INR 500 will attract delivery
        charges of INR 49. Any delivery charges paid by the customer for any
        modified/cancelled/un-delivery order shall be eligible for a full refund
        of the delivery changes provided the customer has made advance payment
        for the said order. Delivery charges shall be refunded in full if any
        other that we delivered and becomes eligible for a refund, subject to
        terms and conditions. Any delivery charges paid by the customer shall
        not be eligible for a refund if the order is modified or replaced by the
        customer due to change in prescription or otherwise.{" "}
      </p>
      <h5>Cancellation, Return and Refund</h5>
      <p>
        We understand that there will be genuine reasons when you decide to
        cancel or return an order.
      </p>
      <p>
        To process a refund, please raise a ticket on the Android / iOS app/
        website for any issue within 15 days of delivery.{" "}
      </p>
      <p>
        No refunds will be processed beyond 15 days from the date of delivery.{" "}
      </p>
      <p>
        In case of missing, damaged or incorrect medicines, the refund will be
        processed back to your original payment source (amount will be credited
        to TM Credit in case of a cash on delivery payment) within 48hrs of
        raising an issue upon verifying the genuineness of the complaint. A
        return pickup will be done from our end for the incorrectly delivered
        medicines.{" "}
      </p>
      <p>
        In case a Truemeds recommended medicine option does not suit you, a
        refund can be initiated for the same. Refund will be processed back to
        your original payment source (amount will be credited to TM Credit in
        case of a cash payment) within 48hrs of raising an issue. A return
        pickup will be done from our end for the remaining medicines.{" "}
      </p>
      <p>
        In case an error has been made by the customer while ordering, a return
        request can be generated by raising a ticket on the Android / iOS app/
        website for any issue within 15 days of delivery. A return pickup will
        be done from our end for the medicines to be returned. On receipt,
        Returned medicines will be verified and a refund will be processed back
        to the customers TM Credit account only. Refund will be initiated within
        48hrs of Truemeds receiving the returned medicines.{" "}
      </p>
      <h5>Acceptance of the Goods</h5>
      <p>
        We request you to verify the complete details and the documents provided
        to you at delivery before acceptance of the Goods. Any complaints with
        regard to shortage of Goods, defects or otherwise will not be
        entertained by Truemeds once the same are accepted at the time of
        delivery.
      </p>
      <h5>WARNING</h5>
      <p>
        YOU MUST CHECK ALL ITEMS DISPENSED TO YOU AND SHOULD NOT TAKE ANY
        MEDICATION THAT APPEARS TO HAVE BEEN TAMPERED WITH OR WHICH MAY HAVE
        BEEN DISPENSED IN ERROR. FAILURE TO ABIDE BY THIS WARNING COULD
        SERIOUSLY DAMAGE YOUR HEALTH AND TRUEMEDS SHALL NOT BE HELD LIABLE.
      </p>
      <h5>Information Technology Act </h5>
      <p>
        Certain laws in India prohibit and impose restrictions on use of the
        Website and you are advised to make familiar with those laws and not to
        post any information or messages that are, or that may be construed as
        being malicious, defamatory, inappropriate, slanderous, pornographic or
        otherwise sexually-oriented or that makes attacks on or otherwise opines
        or comments on any individuals or groups of individuals, educational
        institutions or any other entities whatsoever (whether companies, firms,
        or any other institutions). You also agree not to post any information
        to which you do not have copyrights or other appropriate permissions to
        post in a public forum. Your failure to comply with these terms may
        result in the removal of your postings without prior notice to User. The
        IP address of all posts is recorded to aid in enforcing these
        conditions.{" "}
      </p>
      <h5>Other Laws </h5>
      <p>
        Certain laws require to maintain data with respect to the services,
        Goods and other personal information in a prescribed format and Truemeds
        will use all the information to the extent required in compliance with
        the applicable laws and as may be directed or amended from time to time.
      </p>
      <h5>Force Majeure </h5>
      <p>
        Truemeds shall not be liable and to the extent, that the performance or
        delay in performance of any of its obligations are prevented,
        restricted, delayed or interfered with due to circumstances beyond the
        reasonable control and without the fault or negligence of such Party,
        including but not limited to change in legislation, fire, flood,
        explosion, epidemic, accident, act of God, war, riot, strike, lockout,
        traffic or other concerted act of workmen and/or act of Government.
        Truemeds may at its sole discretion withdraw the services or Goods if a
        Force Majeure event occurs.
      </p>
      <h5>Governing Law and Jurisdiction </h5>
      <p>
        The Terms of Use are governed by and constructed in accordance with the
        laws of India, without reference to conflict of laws principles and you
        irrevocably and unconditionally submit to the exclusive jurisdiction of
        the courts located in Mumbai, India.
      </p>
      <h5>Acceptance of Terms and Conditions</h5>
      <p>
        By Clicking on the "I have read and accept the terms and conditions''
        box at the bottom of the Registration Form, you indicate your acceptance
        of the above Terms and Conditions and you agree to be bound by these
        terms and conditions set forth below including any additional guidelines
        and future modifications. If at any time you do not agree to these terms
        and conditions or you wish to terminate your registration as an
        Affiliate, you may not access or use the information and notify your
        intention to block your registration.
      </p>
      <h5>Communication with the user</h5>
      <p>
        Truemeds will be using 3rd party software to make calls, send SMS,
        emails & notifications to its users. By continuing your usage of the
        website and/or mobile app, the customer indicates their acceptance of
        the same.{" "}
      </p>
      <h5>Refer and Earn </h5>
      <p>
        The referred friend should be a new customer and not an existing or
        returning customer of Truemeds. The referred friend must place a valid
        order for the User to get a referral bonus in their Truemeds cash
        account. Referral reward shall be credited to the User's account after
        successful delivery of the referred friend's order. The referred
        friend's order should not be cancelled or returned. Notwithstanding
        anything contained herein, these terms are in addition to and not in
        derogation to any other terms as stipulated by Truemeds from time to
        time. Truemeds reserves the right in its sole discretion at any time,
        without any prior notice to suspend/cancel this program or amend these
        terms.
      </p>
      <p>
        We as a merchant shall be under no liability whatsoever in respect of
        any loss or damage arising directly or indirectly out of the decline of
        authorization for any Transaction, on Account of the Cardholder having
        exceeded the preset limit mutually agreed by us with our acquiring bank
        from time to time.
      </p>
      <h5>TrueSaving Days</h5>
      <p>
        The promotion period of TrueSavings day will last from 12th Feb 2021 to
        22nd Feb 2021 (Offer Period). Truemeds holds the right to change this
        offer period without prior intimation. Winners of the promotion will be
        selected on a random basis and the selection depends on chance.
        Customers can become eligible for the promotion by placing an order
        during the Offer Period (Eligible Order). Winner will be awarded the
        final payable amount of Eligible Order (TrueSavings Award) in the form
        of TM Credit in their TM wallet post-delivery of Eligible Order. This
        credit can be used for any future orders. In case the Eligible Order is
        cancelled pre-delivery or returned post-delivery, then the TrueSavings
        Award value will be reversed/ removed from the winners TM Wallet. This
        offer/promotion cannot be clubbed with any other offer/promotion
        applicable. Truemeds holds the right to deny promotion credit on grounds
        of malpractice or manipulation of promotion.
      </p> */}
        {props?.tnc?.description}
      </div>
    </>
  );
}
const mapStateToProps = (state) => ({
  tnc: state.legal.tncSuccess,
});

export default withRouter(connect(mapStateToProps, { legalActionThunk })(Tnc));
