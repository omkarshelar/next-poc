import styled from "styled-components";

export const CartDetailWrapper = styled.div`
  width: 100%;
  display: flex;
  flex-flow: column;
  justify-content: space-between;
  margin-top: 10px;
`;
export const MedFirstRow = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-bottom: 0.1rem;
  > div[class="quntity-select"] {
    display: flex;
    align-items: center;
    justify-content: flex-end;
    order: 1;

    .css-b62m3t-container {
      width: 80px !important;
    }

    .css-1s2u09g-control,
    .css-1pahdxg-control {
      border: 1.6px solid #0071bc !important;
      border-radius: 6px;
      height: 44px !important;
      @media screen and (max-width: 468px) {
        height: 30px !important;
      }
    }

    .css-1okebmr-indicatorSeparator {
      display: none !important;
    }

    .css-tj5bde-Svg,
    .css-qc6sy-singleValue {
      color: #0071bc !important;
      font-family: "Inter";
    }

    .css-1pahdxg-control:hover {
      border-color: #0071bc !important;
    }

    .css-2613qy-menu div {
      color: #0071bc !important;
    }

    .css-1n7v3ny-option {
      background-color: #f5f5f5;
      color: #0071bc !important;
      font-size: 14px;
      font-weight: 400;
      font-family: "Inter";
    }

    .css-tlfecz-indicatorContainer {
      color: #0071bc !important;
    }
    .css-9gakcf-option {
      background-color: #e6f7ff;
      color: #0071bc !important;
    }

    .css-yt9ioa-option {
      color: #0071bc !important;
      font-size: 14px;
      font-weight: 400;
      font-family: "Inter";
    }

    .css-14el2xx-placeholder {
      color: #0071bc !important;
      font-size: 14px;
      font-weight: 400;
      font-family: "Inter";
    }

    .ant-select {
      width: 80px !important;
    }

    .ant-select:not(.ant-select-customize-input) .ant-select-selector {
      border: 1px solid #999 !important;
    }

    .ant-select-focused:not(.ant-select-disabled).ant-select:not(.ant-select-customize-input)
      .ant-select-selector {
      border: 1px solid #0071bc !important;
      box-shadow: 0 0 0 2px rgb(0 113 188 / 20%) !important;
    }

    .ant-select-single.ant-select-show-arrow .ant-select-selection-item,
    .ant-select-single.ant-select-show-arrow .ant-select-selection-placeholder {
      font-weight: 500 !important;
      font-size: 14x !important;
      font-family: "Inter", sans-serif !important;
      color: #333 !important;
    }

    > i:first-child {
      margin-right: 10px;
    }
    > i:last-child {
      margin-left: 10px;
    }
    > span {
      font-weight: 700;
      font-size: 16px;
      /* font-family: "Century Gothic", sans-serif; */
      color: #333;
    }
    /* font-family: Century Gothic; */
    font-size: 14px;
    font-style: normal;
    font-weight: 700;
    line-height: 18px;
    letter-spacing: 0em;
    text-align: left;
    @media screen and (max-width: 468px) {
      order: 2;
    }
  }
`;
export const Medname = styled.div`
  max-width: 13rem;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  /* font-family: Century Gothic; */
  font-size: 14px;
  font-style: normal;
  font-weight: 700;
  line-height: 18px;
  letter-spacing: 0em;
  text-align: left;
  margin-right: 5px;
`;
export const Quantity = styled.div`
  display: flex;
  align-items: center;
  > i:first-child {
    margin-right: 10px;
  }
  > i:last-child {
    margin-left: 10px;
  }
  > span {
    font-weight: 700;
    font-size: 16px;
    /* font-family: "Century Gothic", sans-serif; */
    color: #333;
  }
  /* font-family: Century Gothic; */
  font-size: 14px;
  font-style: normal;
  font-weight: 700;
  line-height: 18px;
  letter-spacing: 0em;
  text-align: left;
`;

export const MedSecondRow = styled.div`
  display: flex;
  align-items: flex-start;
  justify-content: space-between;
  margin-bottom: 0.5rem;
`;
export const Price = styled.div`
  > span {
    /* font-family: "Century Gothic", sans-serif; */
    font-size: 14px;
    font-style: normal;
    font-weight: 400;
    line-height: 18px;
    letter-spacing: 0em;
    text-align: left;
  }
`;
export const MRP = styled.div`
  display: flex;
  > span {
    /* font-family: "Century Gothic", sans-serif; */
    font-size: 14px;
    font-style: normal;
    font-weight: 400;
    line-height: 13px;
    letter-spacing: 0em;
    text-align: left;
    color: #666666;
    > del {
      /* font-family: "Century Gothic", sans-serif; */
    }
  }
`;

export const Inclusive = styled.div`
  .medCardInclusive {
    font-weight: 400;
    font-size: 10px;
    color: #999;
  }
`;

export const Discount = styled.div`
  display: flex;
  align-items: center;
  position: relative;
  margin-left: 5px;
  > img {
    width: 60px;
  }
  > span {
    font-weight: 600;
    color: #333;
    font-size: 10px;
    /* font-family: "Century Gothic", sans-serif; */
    position: absolute;
    margin-left: 5px;
    color: #fff;
  }
`;
