import React from "react";
import { connect } from "react-redux";
import Router, { withRouter } from "next/router";

import {
  HealthWrapper,
  Header,
  ArticleList,
  ArticleWrapper,
} from "./HeadlessHealthBlogs.style";
import _ from "underscore";
import ReactPaginate from "react-paginate";
import ArticleCard from "./SubComponent/ArticleCard";
import ArticleCardShimmer from "./SubComponent/ArticleCardShimmer";
import { fetchCustomList } from "../../redux/HeadlessHealth/Action";
import { setCurrentPage } from "../../redux/Pincode/Actions";
import { PaginationWrap } from "./Pagination.style";
import { message } from "antd";
import ArticleSideBarComponent from "./SubComponent/ArticleSideBarComponent";
import BlogError from "./SubComponent/BlogError";
import { eventArticleViewed } from "../../Events/Events";
import window from "global";

export class CustomBlogsList extends React.Component {
  state = {
    option: 1,
    filterList: [],
    page: 1,
    article: this.props.article,
    count: this.props.count,
    loading: false,
    forcePage: 0,
    queryId: this.props.queryId ? this.props.queryId : "",
  };

  componentDidMount = () => {
    let html = document.getElementsByTagName("html");
    if (html?.length > 0) {
      html[0].style.scrollBehavior = "smooth";
    }

    this.props.setCurrentPage({ currentPage: "health-category-tag" });
    /* let type = this.props.match.params?.type;
    let value = this.props.match.params?.id;
    this.props
      .fetchCustomList(type, value, this.props.accessToken, this.state.page)
      .then(() => {
        this.setArticle();
      }); */
  };

  setArticle = () => {
    this.setState({
      article: this.props.article,
      loading: false,
    });
  };

  componentDidUpdate(prevProps, prevState) {
    if (this.props.article) {
      if (this.props.article.length == 0) {
        message.error("Sorry! No data Available");
      }
    }

    if (
      this.props.articleCustomListError &&
      this.props.articleCustomListError.message
    ) {
      message.error(this.props.articleCustomListError.message);
    }

    if (this.props.router?.query?.id !== prevProps.router?.query?.id) {
      this.props
        .fetchCustomList(
          this.props.type,
          this.props.router?.query?.id,
          this.props.accessToken,
          1
        )
        .then(() => {
          window.scrollTo(0, 0);
          this.setArticle();
          this.setState({
            page: 1,
            forcePage: 0,
            queryId: this.props.router?.query?.id,
          });
        });
    }
  }

  specificArticle = (data) => {
    Router.push(`/blog/${data.slug}`);
    // Event_Truemeds
    //new
    eventArticleViewed();
  };
  selectOption = (id) => {
    let result = this.props.article?.filter((data) =>
      data.categories.includes(id)
    );
    this.setState({ option: id, filterList: result });
  };

  handlePageClick = (e) => {
    this.setState({
      loading: true,
    });
    let type = this.props.type;
    let value = this.props.queryId;
    this.props
      .fetchCustomList(type, value, this.props.accessToken, e.selected + 1)
      .then(() => {
        window.scrollTo(0, 0);
        this.setArticle();
        this.setState({
          page: e.selected + 1,
          forcePage: e.selected,
        });
      });
  };
  render() {
    let { article } = this.state;

    return (
      <>
        <HealthWrapper>
          <Header>
            <h5>{this.state.queryId.replace(/-/g, " ")}</h5>
          </Header>
        </HealthWrapper>
        <ArticleWrapper>
          <ArticleList>
            {this.props.isMobile ? (
              <ArticleSideBarComponent
                id={this.state.queryId}
                type={this.props.type}
                category={this.props.category}
              />
            ) : (
              ""
            )}
            {this.state.loading ? (
              <ArticleCardShimmer />
            ) : article?.length > 0 ? (
              <>
                {article.map((data, index) => (
                  <ArticleCard
                    data={data}
                    specificArticle={this.specificArticle}
                    lastIndex={article.length !== index + 1}
                  />
                ))}
                {this.props.count && this.props.count > 0 ? (
                  <PaginationWrap>
                    <ReactPaginate
                      breakLabel="..."
                      nextLabel=">"
                      onPageChange={(e) => {
                        this.handlePageClick(e);
                      }}
                      pageRangeDisplayed={3}
                      pageCount={Math.ceil(this.props.count / 10)}
                      previousLabel="<"
                      renderOnZeroPageCount={null}
                      containerClassName={"containerWrap"}
                      pageClassName="page"
                      pageLinkClassName="pageLink"
                      activeClassName="active"
                      activeLinkClassName="activeLink"
                      previousClassName="previous"
                      nextClassName="next"
                      previousLinkClassName="previousLink"
                      nextLinkClassName="nextLinkClassName"
                      disabledClassName="disabled"
                      disabledLinkClassName="disabledLink"
                      forcePage={this.state.forcePage}
                    />
                  </PaginationWrap>
                ) : (
                  ""
                )}
              </>
            ) : (
              <BlogError type={1} />
            )}
          </ArticleList>
          {!this.props.isMobile ? (
            <ArticleSideBarComponent
              id={this.state.queryId}
              type={this.props.type}
              initCategory={this.props.initCategory}
            />
          ) : (
            ""
          )}
        </ArticleWrapper>
      </>
    );
  }
}
let mapStateToProps = (state) => ({
  article: state.articleHlDetails?.articleCustomList,
  count: state.articleHlDetails?.articleCustomListCount,
  accessToken: state.loginReducer.verifyOtpSuccess?.Response?.access_token,
  articleError: state.articleHlDetails?.articleError,
  articleCustomListError: state.articleHlDetails?.articleCustomListError,
});

export default withRouter(
  connect(mapStateToProps, { fetchCustomList, setCurrentPage })(CustomBlogsList)
);
