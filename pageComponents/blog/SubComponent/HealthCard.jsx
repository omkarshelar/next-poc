import React, { Component } from "react";
import optimizeImage from "../../../components/Helper/OptimizeImage";
import { HealthCardWrapper } from "./HealthCard.style";
import medicine from "./medicine.png";
export class HealthCard extends Component {
  // addDefaultSrc(ev) {
  //   ev.target.src = medicine;
  // }
  render() {
    let { data } = this.props;
    return (
      <HealthCardWrapper onClick={() => this.props.goToArticle(data)}>
        {data.image && (
          <div className="img-article">
            <img
              src={optimizeImage(data.image, "200")}
              alt="medicine"
              // onError={this.addDefaultSrc}
            />
          </div>
        )}

        <span className="tag-article">research</span>
        <p className="article-author">{data.name}</p>

        <p className="article-brief">{data.createdOn.slice(0, 12)}</p>
      </HealthCardWrapper>
    );
  }
}

export default HealthCard;
