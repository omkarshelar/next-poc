import styled from "styled-components";

export const ArticleSideBar = styled.div`
  box-sizing: border-box;
  width: 265px;
  padding: 10px;
  display: flex;
  flex-flow: column;
  position: unset;
  top: 5rem;

  .searchWrap .filterIconWrap {
    display: none;
  }

  /*  @media screen and (max-width: 1024px) {
    display: none;
  } */
  @media screen and (max-width: 768px) {
    display: block;
    width: 100%;
    position: relative;
    top: 0px;
    height: auto;

    .searchWrap {
      display: flex;
    }

    .searchWrap .filterIconWrap {
      height: 40px;
      width: 5%;
      display: flex;
      justify-content: flex-end;
      align-items: center;
      font-size: 18px;
      color: #888;
    }
  }
  @media screen and (max-width: 468px) {
    display: block;
    width: 100%;
    position: relative;
    top: 0px;
    height: auto;

    .searchWrap {
      display: flex;
    }

    .searchWrap .filterIconWrap {
      height: 40px;
      width: 10%;
      display: flex;
      justify-content: flex-end;
      align-items: center;
      font-size: 18px;
      color: #888;
    }
  }
`;

export const CatDailoqWrap = styled.div`
  .title {
    font-size: 16px;
    font-weight: 600;
    border-bottom: 1px solid #ccc;
    margin: 0px -20px;
    padding: 0px 20px;
    padding-bottom: 10px;
    margin-bottom: 5px;
  }
  a.itemWrap:last-child {
    padding-bottom: 10px;
  }
  a.itemWrap {
    display: flex;
    justify-content: space-between;
    color: #555;
    padding: 5px 0px;
    font-size: 14px;
    font-weight: 500;
  }
  a.itemWrap.selected {
    color: #0071bc;
    font-weight: 500;
  }
`;

export const CategoryListWrap = styled.div`
  padding: 0px;
  box-shadow: 0px 20px 40px rgb(87 100 173 / 10%);
  border-radius: 10px;

  .title {
    /* font-family: "Raleway", sans-serif; */
    font-weight: 500;
    font-size: 18px;
    border-bottom: 1px solid #f2f2f2;
    padding: 10px;
  }
  .itemWrap {
    display: flex;
    justify-content: space-between;
    border-bottom: 0px solid #f0f0f0;
    padding: 10px 10px;
    color: #333;
  }
  a.itemWrap.selected {
    color: #0071bc;
    font-weight: 500;
  }
  @media screen and (max-width: 768px) {
    display: none;
  }
`;

export const InputWrap = styled.div`
  position: relative;
  input {
    width: 100%;
    height: 40px;
    font-size: 13px;
    border-radius: 4px;
    margin-bottom: 10px;
    color: #555;
    border: 1px solid #d2d2d2 !important;
  }
  .searchIcon {
    position: absolute;
    right: 8px;
    top: 12px;
    bottom: 0;
    margin: auto;
    font-size: 18px;
    color: #ccc;
  }

  @media screen and (max-width: 768px) {
    width: 95%;
  }
  @media screen and (max-width: 468px) {
    width: 90%;
  }
`;

export const ArticleCategoryShimmer = styled.div`
  .itemName {
    width: 90%;
    height: 22px;
    border-radius: 4px;
    margin: 10px 10px 0px 10px;
  }
`;
