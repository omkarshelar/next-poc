import React, { Component } from "react";
import { connect } from "react-redux";
import CustomButton from "../../../../../../Components/CustomButton/CustomButton";
import { RaiseMedIssueStartAction } from "../../../../../../Redux/HelpFAQ/Action";
import {
  IssueContainer,
  SubmitSection,
  IssuePageContainer,
  PageHeader,
  MedContainer,
  OptionOther,
  AttachImg,
  UploadContainer,
  RecentTickets,
  AccordianCard,
  MedListForReturn,
} from "./RaiseIssue.style";
import "../../../../../HealthArticles/health.css";
import { CustomerService_URL } from "../../../../../../Url/Urls";
import Axios from "axios";
import { withRouter } from "react-router";
import NotProcessModal from "../../../TicketGenerate/NotProcessModal/NotProcessModal";
import { ScrollContainerWrapper } from "../../../../../UploadPrescription/UploadPrescription.style";
import gallery from "../../../../../../Assets/camera.png";
import Backdrop from "../../../../../UploadPrescription/UploadSubComponent/DiscardModal/BackDrop";
import AddressModal from "../../../../../AdressDetail/SubcomponentsAddress/AddressModal";
import RecentTicketCard from "./RecentTicketCard";
import { Accordion, Card } from "react-bootstrap";
import Loader from "../../../../../../Components/Loader/Loader";
import { message } from "antd";
import window from "global";

export class RaiseIssue extends Component {
  state = {
    checkedItems: [],
    raisedMedSuccess: false,
    msg: "",
    imgSet: [],
    deleteImageShow: false,
    removeImgIndex: null,
    medValidation: false,
    imgValidation: false,
    RecentTickets: [],
    accordianText: "View more",
    submitDisabled: false,
    loader: true,
  };
  componentDidMount() {
    const apiUrl = `${CustomerService_URL}/getAllIssueMedicines?orderId=${this.props.history.location.state.orderId}`;

    let config = {
      headers: {
        transactionId: "a",
        "Content-Type": "application/json",
        Authorization: "Bearer " + this.props.accessToken.Response.access_token,
        "Access-Control-Allow-Origin": "*",
      },
    };
    Axios.get(apiUrl, config)
      .then((response) => {
        if (response.status === 200) {
          let FilteredAraay = response.data.medicines.map((data) => {
            return {
              issueId: data.issueId,
              description: data.description,
              orderProductName: data.orderProductName,
              medicineId: data.medicineId,
              orderProductQty: data.orderProductQty,
              orderProductPrice: data.orderProducPrice,
              checked: false,
              isRaisedAlready: !data.issueId ? false : true,
            };
          });

          this.setState({
            checkedItems: FilteredAraay,
            RecentTickets: response.data.recentTickets,
            loader: false,
          });
        }
      })
      .catch((err) => {
        this.setState({ loader: false });
        console.error(err.response);
      });
  }

  FilterMed = (event, content) => {
    let isChecked = event.target.checked;
    let final = [];
    for (const data of this.state.checkedItems) {
      if (data.medicineId === content.medicineId) {
        if (isChecked) {
          final = [
            ...final,
            {
              ...data,
              checked: true,
              issueId: "7",
              description: "Broken Medicine",
            },
          ];
        } else {
          final = [...final, { ...data, checked: false }];
        }

        this.setState({ checkedItems: final });
      } else {
        final = [...final, { ...data }];
        this.setState({ checkedItems: final });
      }
    }
  };

  onChangeReason = (e, content, desc) => {
    let final = [];
    for (const data of this.state.checkedItems) {
      if (data.medicineId === content.medicineId) {
        final = [
          ...final,
          { ...data, issueId: e.target.value, description: desc },
        ];
        this.setState({ checkedItems: final });
      } else {
        final = [...final, { ...data }];
        this.setState({ checkedItems: final });
      }
    }
  };

  submitIssue = () => {
    let { userData, accessToken } = this.props;
    let raisedMeds = this.state.checkedItems.filter(
      (data) => data.checked === true
    );
    if (raisedMeds.length > 0 && userData && accessToken) {
      if (this.state.imgSet.length > 0) {
        this.setState({ submitDisabled: true });
        let medIssueDto = {
          medIssues: raisedMeds,
          images: this.state.imgSet,
        };
        const apiUrl = `${CustomerService_URL}/raiseOrderIssues?orderId=${this.props.history.location.state.orderId}&customerId=${userData.customerId}`;
        let config = {
          headers: {
            transactionId: "a",
            "Content-Type": "application/json",
            Authorization: "Bearer " + accessToken.Response.access_token,
            "Access-Control-Allow-Origin": "*",
          },
        };
        Axios.post(apiUrl, medIssueDto, config)
          .then((response) => {
            if (response.status === 200) {
              this.setState({
                raisedMedSuccess: true,
                msg: response.data[200],
                submitDisabled: false,
              });
            }
          })
          .catch((err) => {
            console.error(err.response);
            this.setState({ submitDisabled: false });
          });
      } else {
        this.setState({ imgValidation: true });
      }
    } else {
      this.setState({ medValidation: true });
    }
  };

  closeProcessModal = () => {
    this.setState({ raisedMedSuccess: false }, () => {
      if (this.props.location.state.formmyOrder) {
        Router.push("/myorders");
      } else {
        Router.push("/helpwithmyorder");
      }
    });
  };

  writeReason = (e, content) => {
    let { value } = e.target;
    let final = [];
    for (const data of this.state.checkedItems) {
      if (data.medicineId === content.medicineId) {
        final = [...final, { ...data, description: value }];
        this.setState({ checkedItems: final });
      } else {
        final = [...final, { ...data }];
        this.setState({ checkedItems: final });
      }
    }
  };
  onImageChange = (e) => {
    if (window.FileReader) {
      let file = e.target.files[0];
      let reader = new FileReader();
      if (file && file.type.match("image.*")) {
        reader.readAsDataURL(file);
      } else {
        message.error("Please upload valid image");
      }
      reader.onloadend = () => {
        this.setState({ imgSet: [...this.state.imgSet, reader.result] });
      };
    }
  };

  deleteImage = (ind) => {
    let arr = [...this.state.imgSet];
    for (var i = 0; i < arr.length; i++) {
      if (i === ind) {
        arr.splice(i, 1);
      }
    }
    this.setState({ imgSet: arr, deleteImageShow: false });
  };
  goToTicketDetails = (ticketId) => {
    let { userData, accessToken } = this.props;
    if (userData && accessToken && accessToken.Response)
      Router.push({
        pathname: "/ticketdetails",
        state: {
          customerId: userData.customerId,
          ticketid: ticketId,
          token: accessToken.Response.access_token,
        },
      });
  };
  changeAccordianText = () => {
    this.setState((prevState) => {
      if (prevState.accordianText === "View more") {
        return { accordianText: "View less" };
      } else if (prevState.accordianText === "View less") {
        return { accordianText: "View more" };
      }
    });
  };
  render() {
    return (
      <>
        {this.state.raisedMedSuccess && (
          <NotProcessModal
            handleClose={() => this.closeProcessModal()}
            Msg={this.state.msg}
            handleshow={this.state.raisedMedSuccess}
          />
        )}
        {this.state.medValidation && (
          <NotProcessModal
            handleClose={() => this.setState({ medValidation: false })}
            Msg="Select at least one medicine issue"
            handleshow={this.state.medValidation}
          />
        )}
        {this.state.imgValidation && (
          <NotProcessModal
            handleClose={() => this.setState({ imgValidation: false })}
            Msg="Please upload an image"
            handleshow={this.state.imgValidation}
          />
        )}
        {this.state.deleteImageShow && (
          <>
            <AddressModal
              deleteMethod={() => this.deleteImage(this.state.removeImgIndex)}
              Msg="Are you sure want to delete?"
              handleClose={() =>
                this.setState({
                  deleteImageShow: false,
                })
              }
            />
            <Backdrop show={this.state.deleteImageShow} />
          </>
        )}
        {this.state.loader ? (
          <Loader></Loader>
        ) : (
          <IssueContainer>
            <IssuePageContainer>
              {this.state.RecentTickets?.length > 0 && (
                <>
                  <div
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center",
                    }}
                  >
                    <RecentTicketCard
                      data={this.state.RecentTickets[0]}
                      goToDetails={() =>
                        this.goToTicketDetails(
                          this.state.RecentTickets[0].ticketId
                        )
                      }
                    ></RecentTicketCard>
                  </div>
                  {this.state.RecentTickets.length > 1 && (
                    <Accordion>
                      <AccordianCard>
                        <Accordion.Toggle
                          as={Card.Header}
                          eventKey="0"
                          onClick={this.changeAccordianText}
                        >
                          {this.state.accordianText}
                        </Accordion.Toggle>

                        <Accordion.Collapse eventKey="0">
                          <RecentTickets>
                            {this.state.RecentTickets.slice(1).map(
                              (data, index) => (
                                <RecentTicketCard
                                  data={data}
                                  goToDetails={() =>
                                    this.goToTicketDetails(data.ticketId)
                                  }
                                  key={index}
                                ></RecentTicketCard>
                              )
                            )}
                          </RecentTickets>
                        </Accordion.Collapse>
                      </AccordianCard>
                    </Accordion>
                  )}
                </>
              )}
              <PageHeader>
                <span>Raise issues for medicines</span>
              </PageHeader>
              <MedContainer>
                {this.state.checkedItems.map((data) => (
                  <MedListForReturn
                    key={data.medicineId}
                    disableArea={data.isRaisedAlready}
                  >
                    <label
                      style={{
                        marginBottom: "0",
                        display: "flex",
                        alignItems: "center",
                      }}
                    >
                      <input
                        type="checkbox"
                        className="med-raise-issue"
                        key={data.medicineId}
                        onChange={(e) => this.FilterMed(e, data)}
                        value={data.medicineId}
                      />
                      <span>{data.orderProductName}</span>
                    </label>
                    {data.checked && (
                      <OptionOther
                        style={{ display: "flex", flexFlow: "column" }}
                      >
                        <label>
                          <input
                            type="radio"
                            value="7"
                            name={`med${data.medicineId}`}
                            onChange={(e) =>
                              this.onChangeReason(e, data, "Broken Medicine")
                            }
                            defaultChecked={data.checked}
                            className="med-raise-issue"
                          />
                          {"  "}
                          Broken Medicine
                        </label>
                        <label>
                          <input
                            type="radio"
                            value="8"
                            name={`med${data.medicineId}`}
                            onChange={(e) =>
                              this.onChangeReason(e, data, "Expired Medicine")
                            }
                            className="med-raise-issue"
                          />
                          {"  "}
                          Expired Medicine
                        </label>
                        <label>
                          <input
                            type="radio"
                            value="9"
                            name={`med${data.medicineId}`}
                            onChange={(e) =>
                              this.onChangeReason(e, data, "Other")
                            }
                            className="med-raise-issue"
                          />
                          {"  "}
                          Other
                        </label>
                        {data.issueId === "9" && (
                          <textarea
                            onChange={(e) => this.writeReason(e, data)}
                            name={`other${data.medicineId}`}
                            style={{
                              border: "1px #b8b8b8 solid",
                              borderRadius: "5px",
                              marginTop: "3px",
                            }}
                          ></textarea>
                        )}
                      </OptionOther>
                    )}
                  </MedListForReturn>
                ))}
              </MedContainer>
              <AttachImg>
                <span>Attach Images</span>
                <UploadContainer>
                  <img src={gallery} alt="camera"></img>
                  <label htmlFor="upload-photo">Camera</label>
                  <input
                    type="file"
                    name="photo"
                    id="upload-photo"
                    onChange={this.onImageChange}
                    multiple
                    accept="image/*"
                  ></input>
                </UploadContainer>
                <ScrollContainerWrapper
                  className="scroll-container"
                  horizontal={true}
                  hideScrollbars={true}
                >
                  {this.state.imgSet.map((data, index) => (
                    <div key={index}>
                      <i
                        className="fas fa-times-circle close"
                        onClick={() =>
                          this.setState({
                            deleteImageShow: true,
                            removeImgIndex: index,
                          })
                        }
                      ></i>

                      <img src={data} alt="Prescription"></img>
                    </div>
                  ))}
                </ScrollContainerWrapper>
              </AttachImg>
            </IssuePageContainer>
            <SubmitSection>
              <CustomButton
                SubmitIssue
                onClick={() => this.submitIssue()}
                disabled={this.state.submitDisabled}
              >
                Submit
              </CustomButton>
            </SubmitSection>
          </IssueContainer>
        )}
      </>
    );
  }
}

let mapDispatchToProps = (dispatch) => ({
  fetchMeds: (data) => dispatch(RaiseMedIssueStartAction(data)),
});

const mapStateToProps = (state) => ({
  userData: state.otp.UserData,
  medList: state.RaiseIssue.med,
  accessToken: state.otp.msg,
});
export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(RaiseIssue)
);
