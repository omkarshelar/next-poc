import styled from "styled-components";
import ScrollContainer from "react-indiana-drag-scroll";

export const WholeImages = styled(ScrollContainer)`
  display: flex;
  margin: 0 auto;
  width: 70%;
  @media screen and (max-width: 568px) {
    width: 100%;
  }
`;

export const TicketDetailWrapper = styled.div`
  width: 50%;
  margin: 0 auto;
  @media screen and (max-width: 968px) {
    width: 60%;
  }
  @media screen and (max-width: 768px) {
    width: 70%;
  }
  @media screen and (max-width: 568px) {
    width: 95%;
  }
`;

export const HeaderTicket = styled.div`
  display: flex;
  justify-content: center;
  > span {
    font-weight: 600;
  }
`;
export const HeaderInfo = styled.div`
  display: flex;
  flex-flow: column;
  @media screen and (max-width: 568px) {
    font-size: small;
  }
`;
export const HeaderCard = styled.div`
  clear: both;
  display: flex;
  flex-flow: column;
  margin: 10px 5px;
  @media screen and (max-width: 568px) {
    font-size: small;
  }
`;
export const CartMed = styled.div`
  position: relative;
  border: 1.5px #0071bc solid;
  padding: 20px 5px 5px 5px;
  margin-top: 2rem;
  border-radius: 10px;
  > span {
    margin: 3px 0;
  }
`;

export const HeaderHistory = styled.div`
  position: absolute;
  background-color: #0071bc;
  color: white;
  width: 100px;
  padding: 5px 10px;
  text-align: center;
  border-radius: 10px;
  top: -15px;
  left: 50%;
  transform: translateX(-50%);
`;

export const TicketImageContainer = styled.div`
  > img {
    width: 90px;
    height: 120px;
    margin: 10px;
    object-fit: cover;
    border-radius: 15px;
  }
  @media screen and (max-width: 468px) {
    > img {
      width: 70px;
      height: 100px;
      margin: 10px;
      object-fit: cover;
    }
  }
`;
