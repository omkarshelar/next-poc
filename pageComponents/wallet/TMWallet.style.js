import styled from "styled-components";

export const WalletContainer = styled.div`
  width: 100%;
  box-sizing: border-box;
`;

export const SectionTitle = styled.h4`
  font-weight: 700;
  margin-bottom: 1.5em;
  z-index: 5;
  position: relative;

  @media only screen and (max-width: 767px) {
    font-size: large;
    margin-bottom: 1em;
  }
`;

export const WalletWrapper = styled.div`
  width: 100%;
  display: grid;
  grid-template-columns: 2fr 1fr;
  gap: 2em;
  margin: 2em 0;
  padding-bottom: 10vh;

  @media only screen and (max-width: 767px) {
    padding-bottom: 2em;
  }

  h1,
  h2,
  h3,
  h4,
  h5,
  h6,
  p {
    /* font-family: "Inter", sans-serif !important; */
  }

  .TMFAQSeparator {
    width: 100%;
    display: none;
  }

  @media only screen and (max-width: 767px) {
    grid-template-columns: auto;

    .TMFAQSeparator {
      display: block;
      margin-left: -15px;
      width: calc(100% + 30px);
    }
  }
`;
