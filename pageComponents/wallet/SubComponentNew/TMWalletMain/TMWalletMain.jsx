import { Button } from "antd";
import React, { Component } from "react";
import { AiFillRightCircle } from "react-icons/ai";
import walltImg from "../../SubComponentNew/Images/WalletBannerImg.svg";
import walltImgMob from "../../SubComponentNew/Images/WalletBannerImgMob.svg";
import { TMWalletMainSection } from "./TMWalletMain.styled";
import WalletItemLoaderCard from "../WalletItemLoader/WalletItemLoaderCard";
import Link from "next/link";
import { useRouter } from "next/router";
import Router, { withRouter } from "next/router";
import window from "global";
export class TMWalletMain extends Component {
  state = {
    newuser: false,
  };

  componentDidMount = () => {};

  goTo = (route) => {
    Router.push(`${route}`);
  };

  kFormatter = (num) => {
    return Math.abs(num) > 999
      ? Math.sign(num) * (Math.abs(num) / 1000).toFixed(1) + "k"
      : (Math.sign(num) * Math.abs(num)).toFixed(2);
  };

  newuser = () => {
    if (
      !this.props.transactionData &&
      Number(
        this.props.info?.payload?.truemedsCash +
          this.props.info?.payload?.truemedsCredit
      ) == 0
    ) {
      this.setState({ newuser: true });
    } else {
      this.setState({ newuser: false });
    }
  };

  render() {
    return this.props.info ? (
      <TMWalletMainSection>
        <div
          className={`topWalletSection ${
            this.props.transactionData &&
            this.props.transactionData.length == 0 &&
            Number(
              this.props.info?.payload?.truemedsCash +
                this.props.info?.payload?.truemedsCredit
            ) == 0.0
              ? "newuser"
              : ""
          }`}
        >
          <div className="walletBox">
            <div className="walletText">
              <h5>Wallet Balance</h5>

              <h1>
                ₹{" "}
                {Number(
                  this.props.info?.payload?.truemedsCash +
                    this.props.info?.payload?.truemedsCredit
                ).toFixed(2)}
              </h1>
            </div>

            <div className="walletBoxImg">
              <img
                src={window.innerWidth > 668 ? walltImg : walltImgMob}
                alt="wallet_img"
              />
              {/* <div className="walletImgBackground"></div> */}
            </div>
          </div>

          <p>
            Referrals, cashbacks and refunds in one place. A quick and
            convenient way to pay for your next order.
          </p>
        </div>

        {this.props.transactionData &&
        this.props.transactionData.length == 0 &&
        Number(
          this.props.info?.payload?.truemedsCash +
            this.props.info?.payload?.truemedsCredit
        ) == 0 ? (
          <br />
        ) : (
          <div className="transactionSection">
            {/* <Link href="/wallet/rewardBalance"> */}
            <div
              className="transactionBox"
              onClick={() => Router.push("/wallet/rewardBalance")}
            >
              <h6>TM Reward</h6>
              <h4>
                ₹{Number(this.props.info.payload.truemedsCash).toFixed(2)}
              </h4>

              {/* <Link href="/wallet/rewardBalance"> */}
              <Button
                type="link"
                style={{ color: "#0071BC" }}
                onClick={() => Router.push("/wallet/rewardBalance")}
              >
                View Transactions <AiFillRightCircle />
              </Button>
              {/* </Link> */}
            </div>
            {/* </Link> */}
            <div
              className="transactionBox"
              onClick={() => Router.push("/wallet/creditBalance")}
            >
              <h6>TM Credit</h6>
              <h4>
                ₹{Number(this.props.info.payload.truemedsCredit).toFixed(2)}
              </h4>

              <Button
                type="link"
                style={{ color: "#0071BC" }}
                onClick={() => Router.push("/wallet/creditBalance")}
              >
                View Transactions <AiFillRightCircle />
              </Button>
            </div>
          </div>
        )}
      </TMWalletMainSection>
    ) : (
      ""
    );
  }
}
