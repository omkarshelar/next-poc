import styled from "styled-components";
import { accent, checkFlexGap } from "../ReferEarn.styled";

export const SocialShareSwiperWrapper = styled.div`
  width: max-content;
  margin: 38px auto 20px auto;

  @media only screen and (max-width: 767px) {
    padding: 23px 0 19px 0;
    margin: 0 auto;
    gap: 14px 55px;
  }

  .SocialShareSwipper {
    width: 100%;
    position: relative;
    /* padding-bottom: 25px; */
    display: flex;
    flex-flow: column;

    max-width: 320px;
    overflow: hidden;

    ${checkFlexGap()
      ? `gap: 30px;`
      : `
    & > div:not(:last-child):not(:first-child) {
          margin-bottom: 30px;
        }
    `}

    /* //?Safari 6.00 - 10.00 */
    /* @media screen and (min-color-index: 0) and(-webkit-min-device-pixel-ratio:0) {
      @media {
        div:not(:last-child):not(:first-child) {
          margin-bottom: 30px;
        }
      }
    } */

    /* //?Safari 9.00+ */
    /* @supports (-webkit-hyphens: none) {
      
      & > div:not(:last-child):not(:first-child) {
        margin-bottom: 30px;
      }
    } */

    .swiper-pagination {
      position: relative;
      order: 2;
      display: flex;
      justify-content: center;
      align-items: center;

      .swiper-pagination-bullet-active {
        background: ${accent};
      }
    }

    .swiper-wrapper {
      .swiper-slide {
        width: max-content !important;
        display: grid;
        grid-template-columns: repeat(3, 1fr);
        gap: 18px 45px;

        @media only screen and (max-width: 767px) {
          width: 100% !important;
          /* padding: 23px 0; */
          gap: 14px 55px;
        }

        @supports not (gap: 45px) {
          margin: 18px 45px;

          @media only screen and (max-width: 767px) {
            margin: 14px 55px;
          }
        }

        button.transBtn {
          background: transparent;
          border: none;
        }

        .react-share__ShareButton,
        button.transBtn {
          display: flex;
          flex-flow: column;
          justify-content: center;
          align-items: center;
          width: 100%;
          position: relative;
          z-index: 5000;

          ${checkFlexGap()
            ? ` gap: 18px;`
            : `
           img {
              margin-bottom: 18px;
           }

           h5 {
                margin-top: 0;
              }
          `}

          @media only screen and (max-width: 767px) {
            ${checkFlexGap()
              ? ` gap: 14px;`
              : `
           img {
              margin-bottom: 14px;
           }

           h5 {
                margin-top: 0;
              }
          `}
          }

          img {
            width: 40px;
            height: 40px;

            @media only screen and (max-width: 767px) {
              width: 50px;
              height: 50px;
            }

            /* //?Safari 6.00 - 10.00 */
            /* @media screen and (min-color-index: 0) and(-webkit-min-device-pixel-ratio:0) {
              @media {
                margin-bottom: 18px;

                @media only screen and (max-width: 767px) {
                  margin-bottom: 14px;
                }
              }
            } */

            /* //?Safari 9.00+ */
            /* @supports (-webkit-hyphens: none) {
              margin-bottom: 18px;

              @media only screen and (max-width: 767px) {
                margin-bottom: 14px;
              }
            } */
          }

          /* //?Safari 6.00 - 10.00 */
          /* @media screen and (min-color-index: 0) and(-webkit-min-device-pixel-ratio:0) {
            @media {
              h5 {
                margin-top: 0;
              }
            }
          } */

          /* //?Safari 9.00+ */
          /* @supports (-webkit-hyphens: none) {
            h5 {
              margin-top: 0;
            }
          } */

          p {
            font-weight: 400;
            font-size: 14px;
            margin: 0;

            @media only screen and (max-width: 767px) {
              font-size: 12px;
              white-space: pre;
              word-break: keep-all;
            }
          }
        }
      }
    }
  }
`;

export const SocialShareModalHeading = styled.div`
  width: 100%;

  h5 {
    font-size: 16px;
    font-weight: 600;

    b {
      font-weight: 800;
    }
  }

  p {
    font-size: 14px;
    font-weight: 400;
  }
`;
