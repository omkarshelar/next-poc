import styled from "styled-components";
import {
  checkFlexGap,
  dark,
  darkGreen,
  darkLight,
  darkMid,
  referBorderRadius,
} from "../ReferEarn.styled";

export const EmptyWrapper = styled.div`
  width: 100%;
  min-height: 70vh;
  display: flex;
  flex-flow: column;
  justify-content: center;
  align-items: center;

  ${checkFlexGap()
    ? ` gap: 15px;`
    : `
  img {
        margin-bottom: 15px;
      }
  `}

  @media only screen and (max-width: 767px) {
    min-height: unset;
    margin: 25vh 0;
    ${checkFlexGap()
      ? ` gap: 29px;`
      : `
  img {
        margin-bottom: 0 !important;
      }

      button.bbtn {
          margin-bottom: 29px;
          
        }
  `}
  }

  /* //?Safari 6.00 - 10.00 */
  /* @media screen and (min-color-index: 0) and(-webkit-min-device-pixel-ratio:0) {
    @media {
      img {
        margin-bottom: 15px;

        @media only screen and (max-width: 767px) {
          margin-bottom: 29px;
        }
      }

      @media only screen and (max-width: 767px) {
        button.bbtn {
          margin-bottom: 29px;
        }
      }
    }
  } */

  /* //?Safari 9.00+ */
  /* @supports (-webkit-hyphens: none) {
    padding: 11px 16px;
  } */

  p {
    margin-top: 30px;
    color: ${darkMid};
    font-size: 14px;
    /* line-height: 3.04px; */
    font-weight: 600;

    @media only screen and (max-width: 767px) {
      margin-top: 20px;
      margin-bottom: 0;

      ${!checkFlexGap() &&
      `
  margin-top:20px;
  margin-bottom:0px;
  `}
    }
  }

  .bbtn {
    max-width: 300px;
  }
`;

export const MyReferralWrapper = styled.div`
  width: 100%;

  @media only screen and (max-width: 767px) {
    margin-top: 50px;
  }

  .ant-collapse {
    background-color: #fff;
  }

  .MyRefCollapse {
    border: none;
    .myRefCollapse-panel {
      overflow: hidden;
      margin-bottom: 14px;
      border: 1px solid ${darkLight};
      border-radius: ${referBorderRadius};

      @media only screen and (max-width: 767px) {
        margin-bottom: 12px;

        .ant-steps-item:last-child .ant-steps-item-content {
          min-height: 35px;
        }
      }

      .deliverStep .ant-steps-item-title {
        color: ${dark} !important;
        font-weight: 600 !important;
      }

      .ant-collapse-header {
        display: flex;
        flex-flow: row nowrap;
        gap: 1em;
        justify-content: space-between;
        align-items: center;
        padding: 14px 20px;

        @media only screen and (max-width: 767px) {
          padding: 11px 5px;

          ${!checkFlexGap() &&
          ` padding: 11px 0;
        `};

          /* //?Safari 6.00 - 10.00 */
          /* @media screen and (min-color-index: 0) and(-webkit-min-device-pixel-ratio:0) {
            @media {
              padding: 11px 16px;

              h5 {
                margin: 0 !important;
              }
            }
          } */

          /* //?Safari 9.00+ */
          /* @supports (-webkit-hyphens: none) {
            padding: 11px 0px;
            h5 {
              margin: 0 !important;
            }
          } */
        }

        h5 {
          margin-bottom: 0;
          font-size: 16px;
          line-height: 24px;
          font-weight: 600;

          @media only screen and (max-width: 767px) {
            font-size: 14px;
            line-height: 17px;
            margin: 0;
          }
        }

        p {
          margin: 0;
          color: ${darkMid};
          font-size: 14px;
          line-height: 24px;
          font-weight: 400;

          @media only screen and (max-width: 767px) {
            font-size: 12px;
          }
        }

        p.success {
          color: ${darkGreen};

          svg {
            color: inherit !important ;
          }
        }

        button.bbtn {
          font-size: 14px;
          padding-top: 8px;
          padding-bottom: 8px;
          width: 128px;

          @media only screen and (max-width: 767px) {
            font-size: 12px;
            width: 94px;
          }

          svg {
            font-size: 16px;

            @media only screen and (max-width: 767px) {
              font-size: 14px;
            }
          }
        }

        h5.extraInfo {
          color: ${darkGreen};
          font-weight: 600;
          font-size: 16px;
          line-height: 24px;
          text-transform: capitalize;
          margin: 0 !important;

          @media only screen and (max-width: 767px) {
            font-size: 14px;
          }
        }

        .ant-collapse-header-text {
          flex-grow: 1;
        }
      }

      .ant-collapse-content-box {
        padding: 20px 30px 5px 30px;

        @media only screen and (max-width: 767px) {
          padding: 8px 16px 0px 16px;
          height: 140px;
        }

        .ant-steps-item-tail {
          margin-left: 17px;
          margin-top: 5px;

          @media only screen and (max-width: 767px) {
            margin-left: 15px;
            margin-top: 2px;
          }

          &::after {
            width: 3px;
            height: 19px;
            margin: 0;
            background-color: ${darkMid};
            border-radius: 10px;
            margin-top: -4px;
          }
        }

        .ant-steps-item-icon {
          margin-bottom: 8px;
          margin-top: 5px;

          .ant-steps-icon-dot {
            width: 16px;
            height: 16px;
            margin-left: 1.5px;
            background-color: ${darkMid};

            @media only screen and (max-width: 767px) {
              width: 12px;
              height: 12px;
            }
          }

          .checkedIcon {
            color: ${darkGreen};
            font-size: 20px;

            @media only screen and (max-width: 767px) {
              font-size: 16px;
            }
          }
        }

        .ant-steps-item-content {
          padding-left: 5px;
          display: flex;
          flex-flow: row nowrap;
          gap: 1em;
          justify-content: space-between;
          align-items: flex-start;

          > div {
            color: ${darkMid};
            font-size: 14px;
            line-height: 24px;
            font-weight: 600;

            @media only screen and (max-width: 767px) {
              font-size: 12px;
              line-height: 22px;
            }
          }

          .ant-steps-item-title {
            margin-top: 0;
          }
          .ant-steps-item-description {
            padding-bottom: 0;
            margin-top: 2px;
            font-weight: 400;
          }
        }

        .ant-steps-item-active {
          .ant-steps-item-title {
            font-weight: 600;
            color: ${dark};
          }
        }

        @media only screen and (max-width: 767px) {
          .deliverStep .ant-steps-item-title,
          .ant-steps-item-active .ant-steps-item-title {
            margin-top: 3px !important;
          }
        }

        .ant-steps-item-finish {
          .ant-steps-item-tail {
            &::after {
              background-color: ${darkGreen};
            }
          }
        }
      }
    }
  }
`;
