///? Default IMPORTS
import React, { Component } from "react";
import { connect } from "react-redux";
import Router, { withRouter } from "next/router";

///? Service IMPORTS

///? Component & Styling IMPORTS
import { Collapse, Steps } from "antd";
import EmptyReferral from "../../../src/Assets/EmptyReferral.svg";
import { EmptyWrapper, MyReferralWrapper } from "./MyReferral.styled";
import { BsBell, BsFillCheckCircleFill } from "react-icons/bs";
import { IoChevronDown, IoChevronUp } from "react-icons/io5";
import ReferModal from "../ReferModal/ReferModal";
import NormalChevron from "../../../src/Assets/ReferBucket/NormalChevron.svg";
import ActiveChevron from "../../../src/Assets/ReferBucket/GreenChevron.svg";
import BellIcon from "../../../src/Assets/ReferBucket/Bell.svg";

///? Constants
const { Panel } = Collapse;
const { Step } = Steps;
export class MyReferral extends Component {
  state = {
    activeMyRefPanel: null,
    myReferralData: null,
    referModalView: false,
  };

  componentDidMount = () => {
    window.scrollTo(0, 0);

    let status = {
      Registered: 1,
      "Order Placed": 2,
      "Order Delivered": 3,
    };

    if (this.props.referralStatusData) {
      this.setState({
        myReferralData: this.props.referralStatusData.sort(
          (a, b) => status[a.status] - status[b.status]
        ),
      });
    }
  };

  customProgress = (dot, { status, index }) =>
    status === "finish" || status === "process" ? (
      <BsFillCheckCircleFill className="checkedIcon" />
    ) : (
      dot
    );

  closeReferModal = () => {
    this.setState({ referModalView: false });
  };

  render() {
    return (
      <>
        {this.state.myReferralData && this.state.myReferralData.length > 0 ? (
          <MyReferralWrapper>
            <Collapse
              className="MyRefCollapse"
              collapsible="header"
              onChange={(key) => this.setState({ activeMyRefPanel: key })}
            >
              {this.state.myReferralData.map((i, p) => (
                <Panel
                  key={p}
                  header={
                    <>
                      <h5 className="title">
                        {i.registeredCustomerName
                          ? i.registeredCustomerName
                          : i.registeredCustomerMobile}
                      </h5>
                      <p
                        className={i.status === "Order Delivered" && "success"}
                      >
                        {i.status}
                        {this.state.activeMyRefPanel &&
                        this.state.activeMyRefPanel.find((j) => j == p) ? (
                          i.status === "Order Delivered" ? (
                            <img src={ActiveChevron} alt="chevron" />
                          ) : (
                            <img src={NormalChevron} alt="chevron" />
                          )
                        ) : i.status === "Order Delivered" ? (
                          <img
                            src={ActiveChevron}
                            alt="chevron"
                            style={{ transform: "rotate(180deg)" }}
                          />
                        ) : (
                          <img
                            src={NormalChevron}
                            alt="chevron"
                            style={{ transform: "rotate(180deg)" }}
                          />
                        )}
                      </p>
                    </>
                  }
                  showArrow={false}
                  className="myRefCollapse-panel"
                  extra={
                    i.status === "Registered" && !i.referralAmt ? (
                      <button
                        className="bbtn"
                        onClick={() => this.setState({ referModalView: true })}
                      >
                        Remind <img src={BellIcon} alt="bell" />
                      </button>
                    ) : i.status === "Order Placed" && !i.referralAmt ? (
                      <h5 className="extraInfo">reward pending</h5>
                    ) : i.status === "Order Delivered" && i.referralAmt ? (
                      <h5 className="extraInfo">
                        + ₹{i.referralAmt.toFixed(2)}
                      </h5>
                    ) : (
                      ""
                    )
                  }
                >
                  <Steps
                    direction="vertical"
                    // size="small"
                    current={
                      i.status === "Registered"
                        ? 0
                        : i.status === "Order Placed"
                        ? 1
                        : i.status === "Order Delivered"
                        ? 2
                        : 0
                    }
                    progressDot={this.customProgress}
                  >
                    <Step
                      title="Registered"
                      className={`${
                        i.status === "Order Delivered" ||
                        i.status === "Order Placed"
                          ? "deliverStep"
                          : ""
                      }`}
                      description={i.registeredDate}
                    />
                    <Step
                      title="Order Placed"
                      className={`${
                        i.status === "Order Delivered" ||
                        i.status === "Order Placed"
                          ? "deliverStep"
                          : ""
                      }`}
                      description={i.orderPlacedDate}
                    />
                    <Step
                      title="Order Delivered"
                      className={`${
                        i.status === "Order Delivered" ? "deliverStep" : ""
                      }`}
                      description={i.deliveredDate}
                    />
                  </Steps>
                </Panel>
              ))}
            </Collapse>

            <ReferModal
              visible={this.state.referModalView}
              closeModal={this.closeReferModal}
              payload={this.props.info.payload}
              remind
            />
          </MyReferralWrapper>
        ) : (
          <EmptyWrapper>
            <img src={EmptyReferral} alt="emptyReferral" />
            <p>Sorry ! No Referrals found</p>

            <button
              className="bbtn"
              onClick={() => {
                this.props.changeActiveKey("1");
                window.scrollTo(0, 0);
              }}
            >
              Refer a Friend
            </button>
          </EmptyWrapper>
        )}
      </>
    );
  }
}

const mapStateToProps = (state) => ({});

const mapDispatchToProps = {};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(MyReferral)
);
