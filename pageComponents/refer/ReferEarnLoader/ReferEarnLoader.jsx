import React, { Component } from "react";
import Shimmer from "react-shimmer-effect";
import "./ReferEarnLoader.css";

export class ReferEarnLoader extends Component {
  render() {
    return (
      <div className="refer-list-shimmer-container">
        <div className="refer-item-shimmer-container bannerContainer">
          <div className="first-row-shimmer bannerMainShimmer">
            <Shimmer>
              <p className="row-shimmer-line"></p>
            </Shimmer>
          </div>
        </div>
        <div className="refer-item-shimmer-container bannerBottomContainer">
          <div className="first-row-shimmer bannerBottomShimmer">
            <Shimmer>
              <p className="row-shimmer-line"></p>
              <p className="row-shimmer-line bannerBottomInput"></p>
            </Shimmer>
          </div>
          <div className="second-row-shimmer bannerBottomShimmer">
            <Shimmer>
              <p className="row-shimmer-line bannerBottomInput"></p>
            </Shimmer>
          </div>
        </div>
        <div className="refer-item-shimmer-container">
          <div className="first-row-shimmer">
            <Shimmer>
              <p className="row-shimmer-line"></p>
              <p className="row-shimmer-line"></p>
            </Shimmer>
          </div>
          <div className="second-row-shimmer">
            <Shimmer>
              <p className="row-shimmer-line"></p>
              <p className="row-shimmer-line"></p>
            </Shimmer>
          </div>
          <div className="third-row-shimmer">
            <Shimmer>
              <p className="row-shimmer-line"></p>
            </Shimmer>
          </div>
        </div>
        <div className="refer-item-shimmer-container">
          <div className="first-row-shimmer">
            <Shimmer>
              <p className="row-shimmer-line"></p>
              <p className="row-shimmer-line"></p>
            </Shimmer>
          </div>
          <div className="second-row-shimmer">
            <Shimmer>
              <p className="row-shimmer-line"></p>
              <p className="row-shimmer-line"></p>
            </Shimmer>
          </div>
          <div className="third-row-shimmer">
            <Shimmer>
              <p className="row-shimmer-line"></p>
            </Shimmer>
          </div>
        </div>
      </div>
    );
  }
}

export default ReferEarnLoader;
