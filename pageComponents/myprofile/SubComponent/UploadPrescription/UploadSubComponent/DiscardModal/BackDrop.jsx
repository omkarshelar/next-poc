import React from "react";
import "./BackDrop.style.css";

const Backdrop = (props) =>
  props.show ? <div className="Upload-Backdrop-modal"></div> : null;

export default Backdrop;
