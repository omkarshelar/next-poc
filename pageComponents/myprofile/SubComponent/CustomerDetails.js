import React, { Component } from "react";
import * as Yup from "yup";
import { Formik } from "formik";
import Button from "../../../components/WebComponents/Button/Button";
import {
  ProfileFirstColumnCard,
  CardWrapper,
  ProfileButtonWrapper,
  ProfileDetailsField,
} from "../SubComponent/PatientDetails/SubComponent/PatientDetailsCard/PatientDetailsCard.style";
import {
  RadioEnable,
  RadioPatient,
} from "../SubComponent/PatientDetails/SubComponent/PatientListCard/PatientListCard.style";
import { message } from "antd";
import Tick from "../../../src/Assets/tick.png";
import "../Profile.css";
import { Router } from "next/router";

let genderArr = [
  { label: "Male", value: "8" },
  { label: "Female", value: "9" },
  { label: "Other", value: "10" },
];

export class CustomerDetails extends Component {
  state = {
    initialState: {
      fullName: this.props.fullName ? this.props.fullName : "",
      age: this.props.age ? this.props.age : "",
      email: this.props.email ? this.props.email : "",
    },
    gender: this.props.gender ? String(this.props.gender) : "",
  };

  genderHandleChange = (gender) => {
    this.setState({ gender: gender });
  };

  handleSubmitHandler = (val) => {
    if (!this.state.gender) {
      message.error("Please select your gender");
    } else {
      let data = {
        customerName: val.fullName,
        mobileNo: "",
        ageGroupId: "",
        ageGroupName: "",
        languageId: "",
        languageName: "",
        emailAddress: val.email,
        //val
        profileImage: "",
        profileImageUrl: "",
        age: Number(val.age),
        gender: Number(this.state.gender),
        genderName: "",
        addressId: "",
        history: Router,
        access_token: this.props.token,
      };
      this.props.updateDetails(data);
    }
  };

  componentDidUpdate(prevProp) {
    if (
      prevProp.fullName !== this.props.fullName ||
      prevProp.age !== this.props.age ||
      prevProp.email !== this.props.email
    ) {
      this.setState({
        initialState: {
          fullName: this.props.fullName,
          age: this.props.age,
          email: this.props.email,
        },
      });
    }
  }

  render() {
    return (
      <div>
        <Formik
          initialValues={this.state.initialState}
          enableReinitialize={true}
          validationSchema={Yup.object().shape({
            email: Yup.string()
              .email("Must be a valid email")
              .max(255)
              .required("email is required"),
            age: Yup.number()
              .required("please enter your age")
              .min(1, "You must be at least 1 years")
              .max(150, "You must be at most 150 years"),
            fullName: Yup.string().required("name is required"),
          })}
          onSubmit={(values) => {
            this.handleSubmitHandler(values);
          }}
        >
          {({ errors, handleSubmit, touched, values, handleChange }) => (
            <form noValidate onSubmit={handleSubmit}>
              <CardWrapper>
                <ProfileFirstColumnCard>
                  <ProfileDetailsField
                    id="standard-basic"
                    name="fullName"
                    label="Full Name"
                    value={values.fullName}
                    onChange={handleChange}
                    error={Boolean(touched.fullName && errors.fullName)}
                    helperText={touched.fullName && errors.fullName}
                    className="full-name-input"
                    autoComplete="off"
                  />

                  <ProfileDetailsField
                    id="standard-basic"
                    name="age"
                    label="Age"
                    value={values.age}
                    onChange={handleChange}
                    error={Boolean(touched.age && errors.age)}
                    helperText={touched.age && errors.age}
                    autoComplete="off"
                  />
                </ProfileFirstColumnCard>
                <ProfileFirstColumnCard>
                  <ProfileDetailsField
                    id="standard-basic"
                    type="email"
                    name="email"
                    label="Email ID"
                    value={values.email}
                    onChange={handleChange}
                    error={Boolean(touched.email && errors.email)}
                    helperText={touched.email}
                    autoComplete="off"
                  />
                  <div className="profileCustomerDetailsGenderContainer">
                    <span>Gender</span>
                    <div className="profileCustomerDetailsGenderInideContainer">
                      {genderArr.map((data) =>
                        data.value === this.state.gender ? (
                          <div>
                            <RadioEnable>
                              <img
                                style={{ width: "11px" }}
                                src={Tick}
                                alt="Tick"
                              />
                            </RadioEnable>
                            <span>{data.label}</span>
                          </div>
                        ) : (
                          <div
                            onClick={() => this.genderHandleChange(data.value)}
                          >
                            <RadioPatient />
                            <span>{data.label}</span>
                          </div>
                        )
                      )}
                    </div>
                  </div>
                </ProfileFirstColumnCard>
              </CardWrapper>
              <ProfileButtonWrapper>
                <Button BtnContinue type="submit">
                  Save
                </Button>
              </ProfileButtonWrapper>
            </form>
          )}
        </Formik>
      </div>
    );
  }
}

export default CustomerDetails;
