import styled from "styled-components";
import FormControl from "@material-ui/core/FormControl";
import TextField from "@material-ui/core/TextField";

export const AddAddressWrapper = styled.div`
  box-sizing: border-box;
  background: #ffffff;
  box-shadow: 0px 20px 50px rgba(87, 100, 173, 0.22);
  border-radius: 4px;
  padding: 30px 15px 15px 15px;
  margin: 15px 0;
  min-height: 270px;
  > header {
    /* font-family: "Raleway", sans-serif; */
    font-style: normal;
    font-weight: 600;
    font-size: 16px;
    line-height: 130.2%;
  }
  @media screen and (max-width: 568px) {
    padding: 15px 15px;
    > header {
      font-size: 14px;
      letter-spacing: 0.7px;
    }
  }

  > header > span {
    margin-left: 15px;
    /* font-family: "Raleway", sans-serif; */
    font-style: italic;
    font-weight: normal;
    font-size: 14px;
    line-height: 130.2%;
    color: #dd2200;

    @media screen and (max-width: 968px) {
      display: block;

      margin: 5px 0;
    }
  }
`;

export const FirstColumnCard = styled.div`
  box-sizing: border-box;
  display: flex;
  flex-flow: column;
  flex-grow: 1;
  margin-right: 10px;
  @media screen and (max-width: 568px) {
    margin-right: 0px;
  }
`;

export const SecondColumnCard = styled.div`
  box-sizing: border-box;
  display: flex;
  flex-flow: column;
  flex-grow: 1;
  margin-left: 10px;
  @media screen and (max-width: 568px) {
    margin-left: 0px;
  }
`;

export const CardWrapper = styled.div`
  display: flex;
  @media screen and (max-width: 568px) {
    flex-flow: column;
  }
`;

export const Relation = styled(FormControl)`
  box-sizing: border-box;
  min-width: 38%;
  margin-left: 2%;
  @media screen and (max-width: 568px) {
    margin-left: 0;
  }
`;

export const FullName = styled(TextField)`
  box-sizing: border-box;
  min-width: 48%;
  margin-right: 2%;
  @media screen and (max-width: 568px) {
    margin-right: 0;
  }
`;

export const ButtonWrapper = styled.div`
  display: flex;
  gap: 20px;
  margin: 10px 10px 0px 0px;
  width: 40%;
  max-height: 60px;
  @media screen and (max-width: 568px) {
    width: 90%;
  }
`;

export const ThirdRow = styled.div``;

export const TextFieldCustomAddress = styled(TextField)`
  margin-top: 8px !important;
  margin-bottom: 8px !important;
`;
