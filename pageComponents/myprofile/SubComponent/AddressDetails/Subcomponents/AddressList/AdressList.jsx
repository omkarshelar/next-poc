import React, { Component } from "react";
import {
  AddressListWrapper,
  RadioEnable,
  RadioPatient,
} from "./AdressList.style";
import Tick from "../../../../../../src/Assets/tick.png";
import KebabAddress from "../Kebab/Kebab";

export class AdressList extends Component {
  state = {
    isSelected: false,
  };
  componentDidUpdate(prevProps, prevState) {}
  componentDidMount = () => {
    if (this.props.pincodeSelectedAddress) {
      this.props.saveAddressForCheckout(
        this.props.pincodeSelectedAddress.addressId
      );
    }
  };
  render() {
    let { data, savePincode, handleSelectAddress, saveAddressForCheckout } =
      this.props;
    let { isSelected } = this.state;
    return (
      <AddressListWrapper>
        <div
          style={{ width: "100%" }}
          onClick={() => {
            // saveAddressForCheckout(data.addressId);
            let params = {
              pincode: data.pincode,
              city: data.cityName,
            };
            // savePincode(params);
            handleSelectAddress(data, params);
          }}
        >
          <div style={{ display: "flex", alignItems: "center" }}>
            {!this.props.profile && (
              <>
                {data.selected ? (
                  <RadioEnable>
                    <img style={{ width: "11px" }} src={Tick} alt="Tick"></img>
                  </RadioEnable>
                ) : (
                  <RadioPatient />
                )}
              </>
            )}
            <div className="patient-info">
              <span>{data.addressType}</span>
              <span>
                {data.addressline1}, {data.landmark}, {data.pincode}
              </span>
            </div>
          </div>
        </div>
        <KebabAddress
          deleteAddressHandler={this.props.deleteAddressHandler}
          addressId={data.addressId}
          toggleAddressSection={this.props.toggleAddressSection}
          data={data}
        />
      </AddressListWrapper>
    );
  }
}

export default AdressList;
