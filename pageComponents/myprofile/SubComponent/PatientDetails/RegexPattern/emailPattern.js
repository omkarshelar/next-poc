export let EMAIL_PATTERN =
  "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
  "\\@" +
  "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
  "(" +
  "\\." +
  "[a-zA-Z0-9][a-zA-Z0-9\\-]{1,25}" +
  ")+";

export let NAME_PATTERN = "^[a-zA-z]+([s][a-zA-Z]+)*$";
