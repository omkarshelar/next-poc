import React, { Component } from "react";
import {
  FirstColumnCard,
  PatientDetailsWrapper,
  CardWrapper,
  SecondColumnCard,
  ButtonWrapper,
  FullName,
  Relation,
  TextFieldCustom,
} from "./PatientDetailsCard.style";
import "./Material.css";
import * as Yup from "yup";
import { Formik } from "formik";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import Button from "../../../../../../components/WebComponents/Button/Button";
import { EMAIL_PATTERN, NAME_PATTERN } from "../../RegexPattern/emailPattern";
// import ScrollContainer from "react-indiana-drag-scroll";
// import { Image_URL } from "../../../../Url/Urls";

export class PatientDetailsCard extends Component {
  state = {
    isSubmitted: false,
  };

  handleSubmitHandler = (data) => {
    if (this.props.data.relationId === 5) {
      this.props.updateProfileHandler(data);
    } else {
      this.props.submitHandler(data);
    }
  };
  render() {
    let { initialState } = this.props;
    return (
      <PatientDetailsWrapper>
        <header>{this.props.header}</header>
        <Formik
          initialValues={initialState}
          validationSchema={Yup.object().shape({
            email:
              this.props.data.relationId === 5 &&
              Yup.string()
                .matches(EMAIL_PATTERN, "Must be a valid email")
                .email("Must be a valid email")
                .max(255)
                .required("email is required"),
            age: Yup.number()
              .typeError("Age must be a number")
              .required("please enter your age")
              .min(1, "You must be at least 1 years")
              .max(120, "You must be at most 120 years"),
            fullName: Yup.string()
              .required("name is required")
              .matches(NAME_PATTERN, "Only Alphabets Allowed"),
            relation: Yup.string().ensure().required("relation is required."),
          })}
          onSubmit={(values) => {
            this.handleSubmitHandler(values);
          }}
        >
          {({ errors, handleSubmit, touched, values, handleChange }) => (
            <form noValidate onSubmit={handleSubmit}>
              <CardWrapper>
                <FirstColumnCard>
                  <FullName
                    id="standard-basic"
                    name="fullName"
                    label="Full Name"
                    value={values.fullName}
                    onChange={handleChange}
                    error={Boolean(touched.fullName && errors.fullName)}
                    helperText={touched.fullName && errors.fullName}
                    className="full-name-input"
                    disabled={this.props.data.relationId !== 5 ? true : false}
                    inputProps={{
                      maxLength: 50,
                    }}
                    variant="outlined"
                    autoComplete="off"
                  />

                  <TextFieldCustom
                    id="standard-basic"
                    name="age"
                    label="Age"
                    value={values.age}
                    onChange={handleChange}
                    error={Boolean(touched.age && errors.age)}
                    helperText={touched.age && errors.age}
                    style={{ minWidth: "10%" }}
                    inputProps={{ maxLength: 3 }}
                    variant="outlined"
                    autoComplete="off"
                  />
                  <Relation variant="outlined">
                    {this.props.data.relationId === 5 ? (
                      <>
                        <TextFieldCustom
                          id="standard-basic"
                          type="email"
                          name="email"
                          label="Email ID"
                          value={values.email}
                          onChange={handleChange}
                          error={Boolean(touched.email && errors.email)}
                          helperText={touched.email}
                          variant="outlined"
                          autoComplete="off"
                        />
                      </>
                    ) : (
                      <>
                        <InputLabel id="demo-simple-select-outlined-label">
                          Who is this for?
                        </InputLabel>
                        <Select
                          labelId="demo-simple-select-outlined-label"
                          id="demo-simple-select-filled"
                          value={values.relation}
                          name="relation"
                          onChange={handleChange}
                          error={Boolean(touched.relation && errors.relation)}
                          helperText={touched.relation && errors.relation}
                        >
                          <MenuItem value={1}>Grandparent</MenuItem>
                          <MenuItem value={2}>Parent</MenuItem>
                          <MenuItem value={3}>Sibling</MenuItem>
                          <MenuItem value={4}>Spouse</MenuItem>
                          <MenuItem value={6}>Child</MenuItem>
                          <MenuItem value={7}>Grandchild</MenuItem>
                          <MenuItem value={8}>Other</MenuItem>
                        </Select>
                      </>
                    )}
                  </Relation>
                </FirstColumnCard>

                <SecondColumnCard>
                  <RadioGroup
                    aria-label="gender"
                    name="gender"
                    onChange={handleChange}
                    value={values.gender}
                    style={{
                      flexDirection: "row",
                      alignItems: "center",
                      color: "#4F4F4F",
                      minWidth: "50%",
                    }}
                  >
                    <FormControlLabel
                      value="8"
                      control={<Radio />}
                      label="Male"
                    />
                    <FormControlLabel
                      value="9"
                      control={<Radio />}
                      label="Female"
                    />
                    <FormControlLabel
                      value="10"
                      control={<Radio />}
                      label="Other"
                    />
                  </RadioGroup>
                </SecondColumnCard>
              </CardWrapper>
              {/* {this.props.prescriptions?.ActiveRx?.length > 0 && (
                <PrescriptionWrapper>
                  <p>
                    {this.props.prescriptions.ActiveRx.length} Prescriptions
                  </p>
                  <ScrollContainer
                    horizontal={true}
                    hideScrollbars={true}
                    style={{ display: "flex", gap: "1rem" }}
                  >
                    {this.props.prescriptions.ActiveRx.map((data) => (
                      <img
                        src={`${Image_URL}${data.imagePath}`}
                        alt="images"
                        style={{ width: "80px" }}
                      />
                    ))}
                  </ScrollContainer>
                </PrescriptionWrapper>
              )} */}
              <ButtonWrapper>
                <Button
                  BtnAdd
                  onClick={() => {
                    this.props.closePatientDetailsSection();
                  }}
                >
                  close
                </Button>
                <Button BtnContinue type="submit">
                  Save
                </Button>
              </ButtonWrapper>
            </form>
          )}
        </Formik>
      </PatientDetailsWrapper>
    );
  }
}

export default PatientDetailsCard;
