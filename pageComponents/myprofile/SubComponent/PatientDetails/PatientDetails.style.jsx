import styled from "styled-components";

export const PatientWrapper = styled.div`
  width: 100%;

  > .patientDetailsAddLoader {
    width: 32%;
    height: 25px;
    border-radius: 4px;
  }

  > header {
    font-weight: 600;
    font-size: large;
    @media screen and (max-width: 568px) {
      margin-left: 10px;
      font-size: 14px;
    }
  }
  > header > span {
    color: #0071bc;
    font-size: medium;
    margin-left: 10px;
    cursor: pointer;
    font-size: 14px;
  }
`;
export const PatientList = styled.div``;

export const SubmitWrapper = styled.div`
  display: flex;
  background-color: #fff;
  padding: 10px;
  gap: 20px;
  @media screen and (max-width: 768px) {
    position: fixed;
    bottom: 0;
    z-index: 10000;
    width: 100vw;
    padding: 5px;
  }
  @media screen and (max-width: 480px) {
    position: fixed;
    bottom: 0;
    left: 0;
    z-index: 10000;
    width: 100vw;
    padding: 5px 1rem;
    gap: 10px;
  }
`;
