import React from "react";
import "./ModificationLog.module.css";
import { Button, Alert } from "antd";
import warnIcon from "../../../src/Assets/warnIcon.svg";

const ModificationLog = (props) => {
  let warningMsg = null;

  warningMsg = (
    <div className={`${"orderDetails_warningWrapper"}`}>
      <div className={"orderDetails_title"}>
        {props.isMyOrderDetails
          ? "Items on your order were modified"
          : "There are some changes in your order"}
      </div>{" "}
      <Button
        type="link"
        className={"orderDetails_linkBtn"}
        onClick={() => {
          window.openSideBar(true, 12, {
            orderData: props.compareOrderData,
            medData: props.medData,
          });
          if (props.isMyOrderDetails) {
          }
        }}
      >
        View Modification log
      </Button>
    </div>
  );

  return (
    <>
      {props.compareOrderData?.isModified && (
        <>
          <Alert
            type="warning"
            className={
              props.isMyOrderDetails
                ? "orderDetails_warnAlertOD"
                : "orderDetails_warnAlert"
            }
            message={warningMsg}
            icon={<img src={warnIcon} alt="warnIcon" />}
            showIcon
          />
        </>
      )}
    </>
  );
};

export default ModificationLog;
