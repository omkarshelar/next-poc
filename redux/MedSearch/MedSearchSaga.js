import { takeLatest, put, all, call } from "redux-saga/effects";
import Axios from "axios";
import {
  MedActions,
  FetchMedFailureActionUpdate,
  FetchMedSuccessActionUpdate,
  RemoveMedSuccessAction,
  RemoveMedFailureAction,
} from "./action";

import {
  ElasticSearch_URL,
  OrderManagementService_URL,
} from "../../constants/Urls";
import { EndPoints } from "./EndPoints";

export function* medFetchSagaUpdate({ payload: { medData } }) {
  try {
    const Uri = `${ElasticSearch_URL}${EndPoints.Org_Sub_Meds_Search}`;

    let response = yield call(() =>
      Axios.post(Uri, medData, {
        auth: {
          username: EndPoints.Elastic_Uname,
          password: EndPoints.Elastic_Pwd,
        },
      })
    );
    yield put(FetchMedSuccessActionUpdate(response.data));
  } catch (error) {
    yield put(FetchMedFailureActionUpdate(error));
  }
}
export function* onMedFetchStartUpdate() {
  yield takeLatest(MedActions.FetchMedStartUpdate, medFetchSagaUpdate);
}

// remove medicine
export function* removeMedSaga({
  payload: { orderId, access_token, medData },
}) {
  try {
    const Uri = `${OrderManagementService_URL}${EndPoints.Edit_Medicine}?orderId=${orderId}`;
    const config = {
      headers: {
        transactionId: "a",
        "Content-Type": "application/json",
        Authorization: "Bearer " + access_token,
      },
    };
    let response = yield call(() => Axios.post(Uri, medData, config));
    yield put(RemoveMedSuccessAction(response.data));
  } catch (error) {
    yield put(RemoveMedFailureAction(error));
  }
}

export function* onRemoveMedicineStart() {
  yield takeLatest(MedActions.RemoveMedStart, removeMedSaga);
}

export function* MedSagas() {
  yield all([call(onMedFetchStartUpdate), call(onRemoveMedicineStart)]);
}
