import { MedActions } from "./action";

let FetchMedStateUpdate = {
  medData: null,
  error: null,
};

export const MedDataReducerUpdate = (
  initialState = FetchMedStateUpdate,
  action
) => {
  switch (action.type) {
    case MedActions.FetchMedSuccessUpdate:
      return { ...initialState, medData: action.payload, error: null };

    case MedActions.FetchMedFailureUpdate:
      return { ...initialState, medData: null, error: action.payload };

    case MedActions.RemoveMedSuccess:
      return { ...initialState, error: null };

    case MedActions.RemoveMedFailure:
      return { ...initialState, error: action.payload };

    default:
      return initialState;
  }
};

let MedResults = {
  resultMed: [],
};

export const MedResultReducer = (initialState = MedResults, action) => {
  switch (action.type) {
    case MedActions.MedResultSuccess:
      return {
        resultMed: action.payload,
      };
    case MedActions.MedResultFailure:
      return {
        resultMed: [],
      };

    default:
      return initialState;
  }
};
