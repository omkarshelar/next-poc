import { CustomerService_URL } from "../../constants/Urls";
import { makeApiCall } from "../makeApiCall";
import { EndPoints } from "./EndPoints";

export const TrackOrderActions = {
  TRACK_ORDER_SUCCESS: "TRACK_ORDER_SUCCESS",
  TRACK_ORDER_FAILURE: "TRACK_ORDER_FAILURE",
};

export function TrackOrderSuccessAction(props) {
  return (dispatch) => {
    dispatch({
      type: TrackOrderActions.TRACK_ORDER_SUCCESS,
      payload: props,
    });
  };
}

export function TrackOrderFailureAction(error) {
  return {
    type: TrackOrderActions.TrackOrder_FAILURE,
    payload: error,
  };
}

// Track order
export function trackOrderThunk(props) {
  const reqHeader = {
    transactionId: "a",
    "Content-Type": "application/json",
    Authorization: "Bearer " + props.access_token,
  };
  const postUrl = `${CustomerService_URL}${EndPoints.Get_Order_Track_On_Home}`;
  return makeApiCall(
    postUrl,
    TrackOrderSuccessAction,
    TrackOrderFailureAction,
    "POST",
    null,
    reqHeader,
    props.history
  );
}
