import { CustomerService_URL } from "../../constants/Urls";
import { makeApiCall } from "../makeApiCall";
import { EndPoints } from "./endpoints";

export const RNEActions = {
  REFFERAL_SUCCESS: "REFFERAL_SUCCESS",
  REFFERAL_FAILURE: "REFFERAL_FAILURE",
  REDEEM_SUCCESS: "REDEEM_SUCCESS",
  REDEEM_FAILURE: "REDEEM_FAILURE",
  REFERRAL_STATUS_SUCCESS: "REFERRAL_STATUS_SUCCESS",
  REFERRAL_STATUS_FAILURE: "REFERRAL_STATUS_FAILURE",
  redeemModal: "Toggle_Modal",
  closeRedeemModalPopup: "close_RedeemModal_Popup",
  conflictPopup: "Conflict_Pop_up",
  closeconflictPopup: "close_conflict_Pop_up",
  claimedPopup: "claimed_Pop_up",
  closeclaimedPopup: "close_claimed_Pop_up",
  restrictPopup: "restrict_Popup",
  closerestrictPopup: "close_restrict_Popup",
};

export function referralSuccessAction(props) {
  return (dispatch) => {
    dispatch({
      type: RNEActions.REFFERAL_SUCCESS,
      payload: props,
    });
  };
}

export function referralFailureAction(error) {
  return {
    type: RNEActions.REFFERAL_FAILURE,
    payload: error,
  };
}

// Referral link
export function referralThunk(props) {
  const reqHeader = {
    transactionId: "a",
    "Content-Type": "application/json",
    Authorization: "Bearer " + props.access_token,
    "Access-Control-Allow-Origin": "*",
  };
  const postUrl = `${CustomerService_URL}${EndPoints.Wallet}`;
  return makeApiCall(
    postUrl,
    referralSuccessAction,
    referralFailureAction,
    "POST",
    null,
    reqHeader,
    props.history
  );
}

export function redeemCodeSuccessAction(props) {
  return (dispatch) => {
    dispatch({
      type: RNEActions.REDEEM_SUCCESS,
      payload: props,
    });
  };
}

export function redeemCodeFailureAction(error) {
  return {
    type: RNEActions.REDEEM_FAILURE,
    payload: error,
  };
}

// Redeem code
export function redeemCodeThunk(props) {
  const reqHeader = {
    transactionId: "a",
    "Content-Type": "application/json",
    Authorization: "Bearer " + props.access_token,
    "Access-Control-Allow-Origin": "*",
  };
  const postUrl = `${CustomerService_URL}${EndPoints.Redeem}?mobile=${props.mobile}&customerId=${props.customerId}&referCode=${props.referCode}`;
  return makeApiCall(
    postUrl,
    redeemCodeSuccessAction,
    redeemCodeFailureAction,
    "POST",
    null,
    reqHeader,
    props.history
  );
}

////////////////? Referral Data

export function referralStatusSuccess(props) {
  return (dispatch) => {
    dispatch({
      type: RNEActions.REFERRAL_STATUS_SUCCESS,
      payload: props,
    });
  };
}

export function referralStatusFailure(error) {
  return {
    type: RNEActions.REFERRAL_STATUS_FAILURE,
    payload: error,
  };
}

export function referralStatusThunk(props) {
  const reqHeader = {
    transactionId: "a",
    "Content-Type": "application/json",
    Authorization: "Bearer " + props.access_token,
    "Access-Control-Allow-Origin": "*",
  };
  const postUrl = `${CustomerService_URL}${
    EndPoints.ReferralStatus
  }?customerId=${props.customerId}${
    props.isNotified ? `&isNotified=${props.isNotified}` : ""
  }`;
  return makeApiCall(
    postUrl,
    referralStatusSuccess,
    referralStatusFailure,
    "GET",
    null,
    reqHeader,
    props.history
  );
}
