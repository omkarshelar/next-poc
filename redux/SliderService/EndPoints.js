export const EndPoints = {
  Doctor_All: "/doctor/all",
  Get_Customer_Order_Listing:'/getCustomerOrderlisting',
  Get_My_Susbstitutes_Info_By_Order_Id:'/getMySubstitutesInfoByOrderId',
  Get_PP:'/getPP',
  About_Us:'/aboutUs'
};
