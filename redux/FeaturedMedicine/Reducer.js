import { LoginActions } from "../Login/Action";
import { SuggestMedActions } from "./Action";

const initState = {
  featuredMedicineList: null,
  featuredMedicineError: null,
  PastMedicineList: null,
  PastMedicineError: null,
};

export const suggestMedReducer = (state = initState, { type, payload }) => {
  switch (type) {
    case SuggestMedActions.FEATURED_MED_SUCCESS:
      return {
        ...state,
        featuredMedicineList: payload,
        featuredMedicineError: null,
      };

    case SuggestMedActions.FEATURED_MED_ERROR:
      return {
        ...state,
        featuredMedicineList: null,
        featuredMedicineError: payload,
      };
    case SuggestMedActions.PAST_PURCHASE_SUCCESS:
      return {
        ...state,
        PastMedicineList: payload,
        PastMedicineError: null,
      };

    case SuggestMedActions.PAST_PURCHASE_ERROR:
      return {
        ...state,
        PastMedicineList: null,
        PastMedicineError: payload,
      };
    case LoginActions.USER_LOGOUT:
      return {
        ...state,
        PastMedicineList: null,
        PastMedicineError: null,
      };
    default:
      return state;
  }
};
