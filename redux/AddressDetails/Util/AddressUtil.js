export const markSelectedAddress = (existingData, responseData) => {
  const existingAddressItem = existingData.find(
    (item) => item.addressId === responseData.addressId
  );
  if (existingAddressItem) {
    return existingData.map((data) =>
      data.addressId === responseData.addressId
        ? { ...data, selected: true }
        : { ...data, selected: false }
    );
  }

  return [...existingData];
};
