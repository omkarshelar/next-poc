import { tmWalletActions } from "./action";

let tmWalletState = {
  tmCash: null,
  tmCashError: null,
  walletHistory: null,
  walletHistoryError: null,
  loading: true,
};

export const tmWalletReducer = (state = tmWalletState, { type, payload }) => {
  switch (type) {
    case tmWalletActions.WALLET_SUCCESS:
      return {
        ...state,
        tmCash: payload,
        tmCashError: null,
        loading: false,
      };
    case tmWalletActions.WALLET_FAILURE:
      return { ...state, tmCash: null, tmCashError: payload, loading: false };

    case tmWalletActions.WALLET_HISTORY_SUCCESS:
      return {
        ...state,
        walletHistory: payload,
        walletHistoryError: null,
        loading: false,
      };
    case tmWalletActions.WALLET_HISTORY_FAILURE:
      return {
        ...state,
        walletHistory: null,
        walletHistoryError: payload,
        loading: false,
      };

    default:
      return state;
  }
};
