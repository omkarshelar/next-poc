import { faqCategoryActions, faqActions } from "./action";

let bannerStateAction = {
  FAQCategory: null,
  FAQData: null,
  isLoading: true,
};

export const FAQReducer = (initialState = bannerStateAction, action) => {
  switch (action.type) {
    case faqCategoryActions.FETCH_FAQ_CATEGORY_FULFILLED:
      return {
        ...initialState,
        FAQCategory: action.payload,
        errorMessage: false,
        isLoading: false,
      };

    case faqCategoryActions.FETCH_FAQ_CATEGORY_REJECTED:
      return {
        ...initialState,
        errorMessage: action.payload,
        isLoading: false,
      };

    case faqActions.FETCH_FAQ_FULFILLED:
      return {
        ...initialState,
        FAQData: action.payload,
        errorMessage: false,
        isLoading: false,
      };

    case faqActions.FETCH_FAQ_REJECTED:
      return {
        ...initialState,
        errorMessage: action.payload,
        isLoading: false,
      };

    default:
      return initialState;
  }
};
