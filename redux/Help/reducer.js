import { HelpActions } from "./action";

let HelpState = {
  HelpData: null,
  error: null,
  HelpCategoryData: null,
  HelpCategoryerror: null,
  HelpDetailsData: null,
  HelpDetailsError: null,
};

export const helpReducer = (state = HelpState, { type, payload }) => {
  switch (type) {
    case HelpActions.HELP_SUCCESS:
      return { ...state, HelpData: payload, error: null };

    case HelpActions.HELP_FAILURE:
      return { ...state, HelpData: null, error: payload };

    case HelpActions.HELP_CATEGORY_SUCCESS:
      return {
        ...state,
        HelpCategoryData: payload,
        HelpCategoryerror: null,
      };

    case HelpActions.HELP_CATEGORY_FAILURE:
      return {
        ...state,
        HelpCategoryData: null,
        HelpCategoryerror: payload,
      };

    case HelpActions.HELP_DETAILS_SUCCESS:
      return {
        ...state,
        HelpDetailsData: payload,
        HelpDetailsError: null,
      };

    case HelpActions.HELP_DETAILS_FAILURE:
      return {
        ...state,
        HelpDetailsData: null,
        HelpDetailsError: payload,
      };

    default:
      return state;
  }
};

let medRaiseIssue = {
  med: null,
  error: null,
};

export const RaiseIssueReducer = (state = medRaiseIssue, { type, payload }) => {
  switch (type) {
    case HelpActions.RAISE_MED_ISSUE_SUCCESS:
      return { ...state, med: payload, error: null };
    case HelpActions.RAISE_MED_ISSUE_FAILURE:
      return { ...state, med: null, error: payload };

    default:
      return state;
  }
};
