import { PatientDataActions } from "./Action";
import { selectPatientUtil } from "./Util/PatientDetail";

let PatientState = {
  patientAll: [],
  updateProfile: null,
  newPatient: null,
  saveRelation: null,
  patientAllErr: null,
  updateProfileErr: null,
  newPatientErr: null,
  saveRelationErr: null,
  deletePatient: null,
  DeletePatientFailure: null,
};

export const PatientReducer = (initialState = PatientState, action) => {
  switch (action.type) {
    case PatientDataActions.PatientDetSuccess:
      return {
        ...initialState,
        patientAll: action.payload,
        patientAllErr: null,
      };

    case PatientDataActions.selectPatient:
      return {
        ...initialState,
        patientAll: selectPatientUtil(initialState.patientAll, action.payload),
      };

    case PatientDataActions.PatientDetFailure:
      return {
        ...initialState,
        patientAll: null,
        patientAllErr: action.payload,
      };

    case PatientDataActions.UpdatePatientSuccess:
      return {
        ...initialState,
        saveRelation: action.payload,
        saveRelationErr: null,
      };

    case PatientDataActions.UpdatePatientFailure:
      return {
        ...initialState,
        saveRelation: null,
        saveRelationErr: action.payload,
      };

    case PatientDataActions.UploadProfileSuccess:
      return {
        ...initialState,
        updateProfile: action.payload,
        updateProfileErr: null,
      };
    case PatientDataActions.UploadProfileFailure:
      return {
        ...initialState,
        updateProfile: null,
        updateProfileErr: action.payload,
      };

    case PatientDataActions.AddPatientSuccess:
      return {
        ...initialState,
        newPatient: action.payload,
        newPatientErr: null,
      };

    case PatientDataActions.AddPatientFailure:
      return {
        ...initialState,
        newPatient: null,
        newPatientErr: action.payload,
      };
    case PatientDataActions.DeletePatientSuccess:
      return {
        ...initialState,
        deletePatient: action.payload,
        deletePatientErr: null,
      };

    case PatientDataActions.DeletePatientFailure:
      return {
        ...initialState,
        deletePatient: null,
        deletePatientErr: action.payload,
      };
    default:
      return initialState;
  }
};
