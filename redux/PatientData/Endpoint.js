export const Get_All_Patient = "/getAllPatient";
export const Update_Profile = "/updateProfile";
export const Add_Patient = "/addPatient";
export const UpdatePatientId_In_SubOrder = "/updatePatientIdInSubOrder";
export const Delete_Patient = "/deletePatient";
