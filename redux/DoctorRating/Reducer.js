import { LoginActions } from "../Login/Action";
import { doctorRatingActions } from "./Action";

let doctorState = {
  getDoctorRatingSuccess: null,
  getDoctorRatingError: null,
  setDoctorRatingSuccess: null,
  setDoctorRatingError: null,
};

export const doctorRatingReducer = (initialState = doctorState, action) => {
  switch (action.type) {
    case doctorRatingActions.GET_DOCTOR_RATING_DETAILS_SUCCESS:
      return {
        ...initialState,
        getDoctorRatingSuccess: action.payload,
        getDoctorRatingError: null,
      };
    case doctorRatingActions.GET_DOCTOR_RATING_DETAILS_FAILURE:
      return {
        ...initialState,
        getDoctorRatingSuccess: null,
        getDoctorRatingError: action.payload,
      };
    case doctorRatingActions.SET_DOCTOR_RATING_DETAILS_SUCCESS:
      return {
        ...initialState,
        setDoctorRatingSuccess: action.payload,
        setDoctorRatingError: null,
      };
    case doctorRatingActions.SET_DOCTOR_RATING_DETAILS_FAILURE:
      return {
        ...initialState,
        setDoctorRatingSuccess: null,
        setDoctorRatingError: action.payload,
      };

    case LoginActions.USER_LOGOUT:
      return {
        ...doctorState,
      };

    default:
      return initialState;
  }
};
