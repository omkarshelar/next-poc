export const LoaderActions = {
  LOADING_START: "LOADING_START",
  LOADING_STOP: "LOADING_STOP",
  LOADING_START_1: "LOADING_START_1",
  LOADING_STOP_1: "LOADING_STOP_1",
  LOADING_START_2: "LOADING_START_2",
  LOADING_STOP_2: "LOADING_STOP_2",
};

export const startLoading = () => ({
  type: LoaderActions.LOADING_START,
});

export const stopLoading = () => ({
  type: LoaderActions.LOADING_STOP,
});

export const startLoading1 = () => ({
  type: LoaderActions.LOADING_START_1,
});

export const stopLoading1 = () => ({
  type: LoaderActions.LOADING_STOP_1,
});

export const startLoading2 = () => ({
  type: LoaderActions.LOADING_START_2,
});

export const stopLoading2 = () => ({
  type: LoaderActions.LOADING_STOP_2,
});
