import { CustomerService_URL } from "../../constants/Urls";
import { makeApiCall } from "../makeApiCall";

export const productDetailsActions = {
  FETCH_PRODUCT_DETAILS_SUCCESS: "FETCH_PRODUCT_DETAILS_SUCCESS",
  FETCH_PRODUCT_DETAILS_FAILURE: "FETCH_PRODUCT_DETAILS_FAILURE",
};

export function FetchProductDetailsSuccessAction(props) {
  return (dispatch) => {
    dispatch({
      type: productDetailsActions.FETCH_PRODUCT_DETAILS_SUCCESS,
      payload: props,
    });
  };
}

export function FetchProductDetailsFailureAction(error) {
  return {
    type: productDetailsActions.FETCH_PRODUCT_DETAILS_FAILURE,
    payload: error,
  };
}

//get medicine details from product code
export function fetchProductDetailsThunk(props) {
  const reqHeader = {
    "Content-Type": "application/json",
  };
  const postUrl = `${CustomerService_URL}/fetchMedicineDetails?warehouseId=${props.warehouseId}`;
  return makeApiCall(
    postUrl,
    FetchProductDetailsSuccessAction,
    FetchProductDetailsFailureAction,
    "POST",
    props.productCd,
    reqHeader,
    props.history
  );
}
