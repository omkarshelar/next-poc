import { allActions } from "./Action";

let initialState = {
  orderStatusDetailsData: null,
};

export const storeDataReducer = (state = initialState, action) => {
  switch (action.type) {
    case allActions.ADD_ORDER_STATUS_DETAILS_DATA:
      return {
        ...state,
        orderStatusDetailsData: action.payload,
      };
    case allActions.CLEAR_ORDER_STATUS_DETAILS_DATA:
      return {
        ...state,
        orderStatusDetailsData: null,
      };

    default:
      return state;
  }
};
