import { bannerAction } from "./Action";

let bannerStateAction = {
  BannerData: null,
  isLoading: true,
};

export const DynamicBannersReducer = (
  initialState = bannerStateAction,
  action
) => {
  switch (action.type) {
    case bannerAction.FETCH_BANNER_FULFILLED:
      return {
        ...initialState,
        BannerData: action.payload,
        errorMessage: false,
        isLoading: false,
      };

    case bannerAction.FETCH_BANNER_REJECTED:
      return {
        ...initialState,
        errorMessage: action.payload,
        isLoading: false,
      };

    default:
      return initialState;
  }
};
