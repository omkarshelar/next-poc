import { articleHLActions } from "./Action";

const initState = {
  article: null,
  articleError: null,
  isLoading: true,
  articleList: null,
  categoryList: null,
  categoryListError: null,
  articleListError: null,
  articleCustomList: null,
  articleCustomListCount: 0,
  articleCustomListError: null,
  totalPosts: null,
  totalPages: null,
};

export const articleHlReducer = (state = initState, { type, payload }) => {
  switch (type) {
    case articleHLActions.LOADING_START:
      return {
        ...state,
        iaLoading: true,
      };
    case articleHLActions.FETCH_ARTICLE_DETAILS_SUCCESS:
      return {
        ...state,
        article: payload,
        articleError: null,
        isLoading: false,
      };

    case articleHLActions.FETCH_ARTICLE_DETAILS_ERROR:
      return {
        ...state,
        article: null,
        articleError: payload,
        isLoading: false,
      };

    case articleHLActions.FETCH_ARTICLE_LIST_SUCCESS:
      return {
        ...state,
        articleList: payload.article,
        articleListError: null,
        isLoading: false,
        totalPages: payload.totalPages,
        totalPosts: payload.totalPosts,
      };

    case articleHLActions.FETCH_ARTICLE_LIST_FAILURE:
      return {
        ...state,
        articleList: null,
        articleListError: payload,
        isLoading: false,
        totalPages: null,
        totalPosts: null,
      };

    case articleHLActions.FETCH_ARTICLE_CUSTOM_LIST_SUCCESS:
      return {
        ...state,
        articleCustomList: payload.article,
        articleCustomListError: null,
        articleCustomListCount: payload.count,
        isLoading: false,
      };

    case articleHLActions.FETCH_ARTICLE_CUSTOM_LIST_FAILURE:
      return {
        ...state,
        articleCustomList: null,
        articleCustomListError: payload,
        articleCustomListCount: 0,
        isLoading: false,
      };

    case articleHLActions.FETCH_CATEGORY_LIST_SUCCESS:
      return {
        ...state,
        categoryList: payload,
        isLoading: false,
        categoryListError: null,
      };
    case articleHLActions.FETCH_CATEGORY_LIST_FAILURE:
      return {
        ...state,
        categoryList: [],
        isLoading: false,
        categoryListError: payload,
      };

    default:
      return state;
  }
};
