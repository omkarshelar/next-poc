export const recentSearchActions = {
  ADD_RECENT_SEARCH: "ADD_RECENT_SEARCH",
  CLEAR_RECENT_SEARCH: "CLEAR_RECENT_SEARCH",
  DELETE_RECENT_SEARCH: "DELETE_RECENT_SEARCH",
};

export const addRecentSearchAction = (item) => ({
  type: recentSearchActions.ADD_RECENT_SEARCH,
  payload: item,
});

export const deleteRecentSearchItemAction = (item) => ({
  type: recentSearchActions.DELETE_RECENT_SEARCH,
  payload: item,
});

export const removeRecentSearchAction = () => ({
  type: recentSearchActions.CLEAR_RECENT_SEARCH,
});
