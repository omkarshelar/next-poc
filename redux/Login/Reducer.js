import { HYDRATE } from "next-redux-wrapper";
import { LoginActions } from "./Action";

const initState = {
  fetchSessionTokenSuccess: null,
  fetchSessionTokenError: null,
  fetchDefaultTokenSuccess: null,
  fetchDefaultTokenError: null,
  generateOtpSuccess: null,
  generateOtpError: null,
  verifyOtpSuccess: null,
  verifyOtpError: null,
  isUserLogged: false,
  customerDetails: null,
  customerDetailsError: null,
  checkStatus: null,
  checkStatusError: null,
  acceptStatus: null,
  acceptStatusError: null,
};

// const persistedState = getCookieStore('reducerOne', initState);

export const loginReducer = (state = initState, { type, payload }) => {
  switch (type) {
    case HYDRATE:
      return { ...state, ...payload };
    case LoginActions.FETCH_SESSION_TOKEN_SUCCESS:
      return {
        ...state,
        fetchSessionTokenSuccess: payload,
        fetchSessionTokenError: null,
      };

    case LoginActions.FETCH_SESSION_TOKEN_FAILURE:
      return {
        ...state,
        fetchSessionTokenSuccess: null,
        fetchSessionTokenError: payload,
      };

    case LoginActions.FETCH_DEFAULT_TOKEN_SUCCESS:
      return {
        ...state,
        fetchDefaultTokenSuccess: payload,
        fetchDefaultTokenError: null,
      };

    case LoginActions.FETCH_DEFAULT_TOKEN_FAILURE:
      return {
        ...state,
        fetchDefaultTokenSuccess: null,
        fetchDefaultTokenError: payload,
      };

    case LoginActions.GENERATE_OTP_SUCCESS:
      return {
        ...state,
        generateOtpSuccess: payload,
        generateOtpError: null,
      };

    case LoginActions.GENERATE_OTP_FAILURE:
      return {
        ...state,
        generateOtpSuccess: null,
        generateOtpError: payload,
      };

    case LoginActions.VERIFY_OTP_SUCCESS:
      return {
        ...state,
        verifyOtpSuccess: payload,
        verifyOtpError: null,
        isUserLogged: true,
      };

    case LoginActions.VERIFY_OTP_FAILURE:
      return {
        ...state,
        verifyOtpSuccess: null,
        verifyOtpError: payload,
        isUserLogged: false,
      };

    case LoginActions.FETCH_CUSTOMER_DETAILS:
      return {
        ...state,
        customerDetails: payload,
        customerDetailsError: null,
      };

    case LoginActions.FETCH_CUSTOMER_DETAILS_FAILURE:
      return {
        ...state,
        customerDetails: null,
        customerDetailsError: payload,
      };

    case LoginActions.UPDATE_CUSTOMER_DETAILS:
      return {
        ...state,
        customerDetailsError: null,
      };

    case LoginActions.UPDATE_CUSTOMER_DETAILS_FAILURE:
      return {
        ...state,
        customerDetailsError: payload,
      };

    case LoginActions.CHECK_PP_OR_TNC_STATUS:
      return {
        ...state,
        checkStatus: payload,
        checkStatusError: null,
      };

    case LoginActions.CHECK_PP_OR_TNC_STATUS_FAILURE:
      return {
        ...state,
        checkStatus: null,
        checkStatusError: payload,
      };

    case LoginActions.ACCEPT_PP_OR_TNC:
      return {
        ...state,
        acceptStatus: payload,
        acceptStatusError: null,
      };

    case LoginActions.ACCEPT_PP_OR_TNC_FAILURE:
      return {
        ...state,
        acceptStatus: null,
        acceptStatusError: payload,
      };

    case LoginActions.USER_LOGOUT:
      return {
        ...initState,
      };

    default:
      return state;
  }
};
