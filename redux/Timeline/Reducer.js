import { ConfirmOrderActions } from "../ConfirmOrder/Action";
import { LoginActions } from "../Login/Action";
import { stepperAction } from "./Action";
import { getCookie, setCookie } from "../../components/Helper/parseCookies";

let TruemedStepperState = {
  toCartSection: false,
  toUploadPrescription: false,
  toPatientDetails: false,
  toAddressDetails: false,
  toOrderSummary: false,
  onOrderSummary: false,
};

let cookieData = getCookie("stepperData");
export const TruemedStepperReducer = (
  initialState = cookieData ? cookieData : TruemedStepperState,
  action
) => {
  switch (action.type) {
    case stepperAction.TO_CART_SECTION:
      setCookie("stepperData", {
        ...initialState,
        toCartSection: true,
        toUploadPrescription: false,
        toPatientDetails: false,
        toAddressDetails: false,
        toOrderSummary: false,
      });
      return {
        ...initialState,
        toCartSection: true,
        toUploadPrescription: false,
        toPatientDetails: false,
        toAddressDetails: false,
        toOrderSummary: false,
      };

    case stepperAction.TO_UPLOAD_PRESCRIPTION_SECTION:
      setCookie("stepperData", {
        ...initialState,
        toCartSection: "done",
        toUploadPrescription: true,
        toPatientDetails: false,
        toAddressDetails: false,
        toOrderSummary: false,
      });
      return {
        ...initialState,
        toCartSection: "done",
        toUploadPrescription: true,
        toPatientDetails: false,
        toAddressDetails: false,
        toOrderSummary: false,
      };
    case stepperAction.TO_PATIENT_DETAILS_SECTION:
      setCookie("stepperData", {
        ...initialState,
        toCartSection: "done",
        toUploadPrescription: "done",
        toPatientDetails: true,
        toAddressDetails: false,
        toOrderSummary: false,
      });
      return {
        ...initialState,
        toCartSection: "done",
        toUploadPrescription: "done",
        toPatientDetails: true,
        toAddressDetails: false,
        toOrderSummary: false,
      };

    case stepperAction.TO_ADDRESS_DETAILS_SECTION:
      setCookie("stepperData", {
        ...initialState,
        toCartSection: "done",
        toUploadPrescription: "done",
        toPatientDetails: "done",
        toAddressDetails: true,
        toOrderSummary: false,
      });
      return {
        ...initialState,
        toCartSection: "done",
        toUploadPrescription: "done",
        toPatientDetails: "done",
        toAddressDetails: true,
        toOrderSummary: false,
      };
    case stepperAction.TO_ORDER_SUMMARY_SECTION:
      setCookie("stepperData", {
        ...initialState,
        toCartSection: "done",
        toUploadPrescription: "done",
        toPatientDetails: "done",
        toAddressDetails: "done",
        toOrderSummary: true,
        onOrderSummary: true,
      });
      return {
        ...initialState,
        toCartSection: "done",
        toUploadPrescription: "done",
        toPatientDetails: "done",
        toAddressDetails: "done",
        toOrderSummary: true,
        onOrderSummary: true,
      };

    case stepperAction.ONLY_ADDRESS_DONE:
      setCookie("stepperData", {
        ...initialState,
        toAddressDetails: "done",
      });
      return {
        ...initialState,
        toAddressDetails: "done",
      };

    case stepperAction.CLEAR_SECTION:
      setCookie("stepperData", {
        toCartSection: false,
        toUploadPrescription: false,
        toPatientDetails: false,
        toAddressDetails: false,
        toOrderSummary: false,
        onOrderSummary: false,
      });
      return {
        toCartSection: false,
        toUploadPrescription: false,
        toPatientDetails: false,
        toAddressDetails: false,
        toOrderSummary: false,
        onOrderSummary: false,
      };

    case stepperAction.NOT_ON_ORDER_SUMMARY:
      setCookie("stepperData", {
        ...initialState,
        onOrderSummary: false,
      });
      return {
        ...initialState,
        onOrderSummary: false,
      };

    case ConfirmOrderActions.CONFIRM_ORDER_SUCCESS:
      return {
        ...TruemedStepperState,
      };

    case LoginActions.USER_LOGOUT:
      return {
        ...TruemedStepperState,
      };

    default:
      return initialState;
  }
};
