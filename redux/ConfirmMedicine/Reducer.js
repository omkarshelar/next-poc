import { ConfirmOrderActions } from "../ConfirmOrder/Action";
import { comfirmMedicineActions } from "./Action";
import { LoginActions } from "../Login/Action";
import { reorderActions } from "../Reorder/Action";

const initState = {
  ConfirmMedData: null,
  ConfirmMedError: null,
};

export const confirmMedicineReducer = (
  state = initState,
  { type, payload }
) => {
  switch (type) {
    case comfirmMedicineActions.CONFIRM_MEDICINE_SUCCESS:
      return {
        ...state,
        ConfirmMedData: payload,
        ConfirmMedError: null,
      };

    case comfirmMedicineActions.CONFIRM_MEDICINE_FAILURE:
      return {
        ...state,
        ConfirmMedData: null,
        ConfirmMedError: payload,
      };

    case comfirmMedicineActions.CLEAR_MEDICINE_DETAILS:
      return {
        ...state,
        ConfirmMedData: null,
        ConfirmMedError: null,
      };

    // Reorder
    case reorderActions.REORDER_SUCCESS:
      return {
        ...state,
        ConfirmMedData: payload,
        ConfirmMedError: null,
      };

    // Logout
    case LoginActions.USER_LOGOUT:
      return {
        ...initState,
      };

    case ConfirmOrderActions.CONFIRM_ORDER_SUCCESS:
      return {
        ...initState,
      };
    // Confirm Order
    // Discard Order
    default:
      return state;
  }
};
