import { saveMoreAction } from "./Action";
import { LoginActions } from "../Login/Action";
import { ConfirmOrderActions } from "../ConfirmOrder/Action";

let saveMoreSectionState = {
  saveMore: false,
};

export const SaveMoreSectionReducer = (
  initialState = saveMoreSectionState,
  action
) => {
  switch (action.type) {
    case saveMoreAction.SET_SAVE_MORE:
      return {
        ...initialState,
        saveMore: true,
      };
    case saveMoreAction.RESET_SAVE_MORE:
      return {
        ...initialState,
        saveMore: false,
      };

    case LoginActions.USER_LOGOUT:
      return {
        ...initialState,
        saveMore: false,
      };

    case ConfirmOrderActions.CONFIRM_ORDER_SUCCESS:
      return {
        ...initialState,
        saveMore: false,
      };

    case ConfirmOrderActions.DISCARD_ORDER_SUCCESS:
      return {
        ...initialState,
        saveMore: false,
      };

    default:
      return initialState;
  }
};
