import React from "react";
import { connect } from "react-redux";
import "./DeleteModal.Style.css";
import CustomButton from "../../../../Components/CustomButton/CustomButton";
import {
  DeleteUploadImageStartAction,
  lastImageClearAction,
} from "../../../../Redux/UploadImage/Action";

function DeleteModal({
  Msg,
  handleClose,
  dispatch,
  data,
  access_token,
  orderIds,
  imgUpload,
}) {
  const deleteMethod = () => {
    if (imgUpload.length === 1) {
      dispatch(
        DeleteUploadImageStartAction({
          imgIdSet: [data.imgId],
          access_token: access_token,
          orderIds: orderIds,
        })
      );
      dispatch(lastImageClearAction());
      handleClose();
    } else {
      dispatch(
        DeleteUploadImageStartAction({
          imgIdSet: [data.imgId],
          access_token: access_token,
          orderIds: orderIds,
        })
      );
      handleClose();
    }
  };

  return (
    <div className="customModalWrapper">
      <div className="ModalBody">
        <h6>{Msg}</h6>
        <div>
          <CustomButton onClick={() => handleClose()} delNo>
            No
          </CustomButton>
          <CustomButton onClick={deleteMethod} delYes>
            Yes
          </CustomButton>
        </div>
      </div>
    </div>
  );
}
let mapStateToProps = (state) => ({
  imgUpload: state.uploadImage.uploadHistory,
});

export default connect(mapStateToProps)(DeleteModal);
