import React, { useEffect, useState } from "react";
import "./AddressPatientSection.css";
import location from "../../../../src/Assets/Location.svg";
import prompt from "../../../../src/Assets/Prompt.svg";
import window from "global";

const AddressPateintSection = (props) => {
  const [isServiceableError, setServiciableError] = useState(false);

  useEffect(() => {
    if (
      props.isServiceable &&
      props.isServiceable.isServiceable &&
      props.isServiceable.pincode ===
        props.updateDetails?.AddressDetails?.pincode
    ) {
      setServiciableError(false);
    } else {
      if (
        props.isServiceable &&
        props.isServiceable.pincode ===
          props.updateDetails?.AddressDetails?.pincode
      ) {
        setServiciableError(true);
      }
    }
  }, [props.isServiceable, props.updateDetails]);

  const handleChangeClick = () => {
    window.openSideBar(true, 3);
  };

  let changeDiv = (
    <div
      onClick={() => {
        handleChangeClick();
      }}
    >
      Add Patient & Address
    </div>
  );
  if (
    props.updateDetails?.AddressDetails &&
    props.patientData &&
    props.patientData?.patientName
  ) {
    changeDiv = (
      <div
        onClick={() => {
          handleChangeClick();
        }}
      >
        Change
      </div>
    );
  } else if (props.patientData && props.patientData.patientName) {
    changeDiv = (
      <div
        onClick={() => {
          handleChangeClick();
        }}
      >
        Add Address
      </div>
    );
  } else if (props.updateDetails?.AddressDetails) {
    changeDiv = <div onClick={() => handleChangeClick()}>Add Patient</div>;
  }
  return (
    <div
      className={`${"addressPatient_deliverySection__Wrap"} ${
        isServiceableError && "addressPatient_nonDelivery__wrap"
      }`}
    >
      <div
        className={`${"addressPatient_dropPointer"}  ${
          isServiceableError && "addressPatient_dropPointerError"
        }`}
      ></div>
      {isServiceableError && (
        <div className={"addressPatient_errorPrompt_wrap"}>
          <div className={"addressPatient_errorPrompt_img"}>
            <img src={prompt} />
          </div>
          <div className={"addressPatient_errorPrompt_text"}>
            This address is no longer serviceable. Please change your address
          </div>
        </div>
      )}
      <div className={"addressPatient_delivery__header"}>
        <div className={"addressPatient_headerLeftTitle"}>
          <span>
            <img src={location} />
          </span>
          <span className={"addressPatient_title"}>Deliver To</span>
        </div>
        {!props.viewDetails && (
          <div className={"addressPatient_headerRightTitle"}>{changeDiv}</div>
        )}
      </div>
      {((props.patientData && props.patientData?.patientName) ||
        props.updateDetails?.AddressDetails) && (
        <div className={"addressPatient_detail__wrap"}>
          <div className={"addressPatient_address__wrap"}>
            {props.patientData && props.patientData?.patientName && (
              <div className={"addressPatient_address_title"}>
                {props.patientData?.patientName}
              </div>
            )}
            {props.updateDetails?.AddressDetails && (
              <div className={"addressPatient_address_details"}>
                {`${props.updateDetails.AddressDetails.addressline1}, ${props.updateDetails.AddressDetails.addressline2}, ${props.updateDetails.AddressDetails.landmark}, ${props.updateDetails.AddressDetails.cityName}, ${props.updateDetails.AddressDetails.pincode}`}
              </div>
            )}
          </div>
        </div>
      )}
      {props.noPatientAddress ? (
        <div className={"addressPatient_address_patient_coachmark"}>
          <div />
          <p>Please add patient and address details</p>
        </div>
      ) : null}
    </div>
  );
};

export default AddressPateintSection;
