import React, { useEffect } from "react";
import { connect } from "react-redux";
import { withRouter } from "next/router";
import "./DoctorCards.css";
import doctor from "../../../../src/Assets/doctorAni.svg";
import doctorMob from "../../../../src/Assets/doctorAniMob.svg";
import phoneMob from "../../../../src/Assets/phoneDocMob.svg";
import phoneWeb from "../../../../src/Assets/phoneDocWeb.svg";
import subsSavingsPercent from "../../../../src/Assets/subsSavingsPercent.svg";
import BoldSeperator from "./BoldSeperator";
import AndroidSeperator from "../../AndroidSeperator/AndroidSeperator";
import window from "global";

const DoctorCard = (props) => {
  const calculateSavings = (arr) => {
    let savings = 0;
    for (let i in arr) {
      let med = arr[i];
      if (
        med.orgProductCd !== med.subsProductCd &&
        !med.coldChainDisabled &&
        !med.disabled &&
        med.medActive
      ) {
        savings =
          savings +
          (med.orgMrp -
            med.orgMrp * (med.orgDiscount / 100) -
            med.subsSellingPrice);
      }
    }
    return savings.toFixed(2);
  };

  const getSaveMoreValue = () => {
    let saveMore = 0;
    if (props.updateDetail) {
      if (
        props.updateDetail?.productSubsMappingList &&
        props.updateDetail.productSubsMappingList.length > 0
      ) {
        saveMore = Number(
          calculateSavings(props.updateDetail.productSubsMappingList)
        ).toFixed(2);
      }

      if (
        props.updateDetail.ImageMasterDto &&
        props.updateDetail.ImageMasterDto.length > 0 &&
        props.updateDetail.productSubsMappingList.length == 0
      ) {
        saveMore = 0;
      }
    }
    if (saveMore > 0) {
      return `Save ₹${saveMore} More`;
    } else {
      return `Save More`;
    }
  };

  if (
    (props.updateDetail?.ImageMasterDto.length > 0 &&
      props.updateDetail?.productSubsMappingList.length === 0) ||
    props.updateDetail?.productSubsMappingList.filter(
      (e) => e.subsProductCd !== e.orgProductCd
    ).length > 0
  ) {
    return (
      <>
        {window.innerWidth > 468 && <BoldSeperator />}
        <div className={"docCard_DoctorCard__wrap"}>
          <div className={"docCard_saveMoreWrap"}>
            <div>
              <img src={subsSavingsPercent} />
            </div>
            <div className={"docCard_saveMoreText"}>{getSaveMoreValue()}</div>
          </div>
          <div className={"docCard_DoctoImageWeb"}>
            <img src={doctor} />
            <img src={phoneWeb} className={"docCard_phoneIconWeb"} />
          </div>
          <div className={"docCard_doctor_details"}>
            <div className={"docCard_doctorTitle"}>
              Choose Truemeds Recommended Medicines
            </div>
            <div className={"docCard_doctorSubTitle"}>
              <div className={"docCard_DoctoImageMob"}>
                <img src={doctorMob} />
                <img src={phoneMob} className={"docCard_phoneIconMob"} />
              </div>
              <div className={"docCard_subText"}>
                <span>You will get a call from a trusted doctor for </span>
                <span>
                  <del>₹249</del>
                </span>
                <span className={"docCard_free"}> FREE</span>
              </div>
            </div>
          </div>
        </div>
        <AndroidSeperator showMob={true} />
      </>
    );
  } else {
    return <div></div>;
  }
};

let mapStateToProps = (state) => ({
  updateDetail: state.orderStatusUpdate?.OrderStatusData,
});

export default withRouter(connect(mapStateToProps)(DoctorCard));
