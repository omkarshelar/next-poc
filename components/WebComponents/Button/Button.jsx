import React from "react";
import { CustomButtonContainer, SidePanelContainer } from "./Button.style";
import loader from "../../../src/Assets/button-loader.svg";
import window from "global";

function Button({ children, ...props }) {
  if (props.isSidePanel) {
    return (
      <div
        style={{
          padding: "16px",
          position: "fixed",
          bottom: "0",
          width: window.innerWidth > 768 ? "360px" : "100%",
          backgroundColor: "#fff",
          filter: "drop-shadow(0px -3px 12px rgba(0, 0, 0, 0.11))",
        }}
      >
        <CustomButtonContainer
          {...props}
          className={props.className ? props.className : "btn"}
          style={{ width: "100%" }}
        >
          {props.loading && (
            <img src={loader} alt="loading.." className="loaderImg" />
          )}
          {children}
        </CustomButtonContainer>
      </div>
    );
  } else {
    return (
      <CustomButtonContainer
        {...props}
        className={props.className ? props.className : "btn"}
      >
        {props.loading && (
          <img src={loader} alt="loading.." className="loaderImg" />
        )}
        {children}
      </CustomButtonContainer>
    );
  }
}

export default Button;
