import React, { useEffect, useState } from "react";
import "./Timeline.module.css";
import orderStatus from "../../../../src/Assets/orderStatus.svg";
import ActiveTile from "./ActiveTile";
import DoctorCall from "../../../../src/Assets/DoctorCall.svg";
import pharmacistCall from "../../../../src/Assets/pharmacistCall.svg";
import orderProscessed from "../../../../src/Assets/orderProscessed.svg";
import moment from "moment";
import PaymentPendingTile from "./PayentPendingTile";
import ProcessingCard from "./ProcessingCard";
import Dispatched from "../../../../src/Assets/Dispatched.svg";
import DispatchCard from "./DispatchCard";
import outForDelivery from "../../../../src/Assets/outForDelivery.svg";
import getDeliveryDate from "../../../Helper/EstimatedDateFormat";
import doctor_mweb from "../../../../src/Assets/doctor_mweb.svg";
import chevron_rate_doctor from "../../../../src/Assets/chevronMwebRating.svg";
import pharmacist_mweb from "../../../../src/Assets/pharmacist_mweb.svg";
import Slide from "@material-ui/core/Slide";
import window from "global";

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const Timeline = (props) => {
  useEffect(() => {
    if (props?.orderStatus?.orderTracking?.statusId === 142) {
    } else if (
      props?.orderStatus?.orderTracking?.orderTypeStatus === 52 &&
      (props?.orderStatus?.orderTracking?.statusId === 1 ||
        props?.orderStatus?.orderTracking?.statusId === 2)
    ) {
    } else if (
      (!props?.orderStatus?.orderTracking?.orderStatus
        ?.map((e) => e.statusId)
        .includes(142) &&
        !props?.orderStatus?.orderTracking?.drCallTime &&
        props?.orderStatus?.orderTracking?.statusId !== 1 &&
        props?.orderStatus?.orderTracking?.statusId !== 2) ||
      props?.orderStatus?.orderTracking?.statusId === 39
    ) {
    } else if (
      (props.orderStatus?.orderTracking?.statusId === 60 &&
        !props.orderStatus?.deliveryPartnerName) ||
      props.orderStatus?.orderTracking?.statusId === 66
    ) {
    } else if (
      props.orderStatus?.orderTracking?.statusId === 60 &&
      props.orderStatus?.deliveryPartnerName
    ) {
    }
  }, []);

  let checkStatusForArray = [1, 2, 39, 58, 55, 60, 66, 142];
  let dateType = 1;
  if (
    props.orderStatus?.orderTracking?.orderStatus &&
    props.orderStatus?.orderTracking?.orderStatus.length > 0
  ) {
    props.orderStatus?.orderTracking?.orderStatus.map((e) => {
      if (checkStatusForArray.includes(e.statusId)) {
        if (moment().diff(moment(e.modifiedOn), "days") > 7) {
          dateType = 2;
        }
      }
    });
  }

  return (
    <div className={"Timeline_timelineWrap"}>
      <TimelineTile
        done={true}
        title={"Order Placed"}
        active={false}
        date={
          props.orderStatus?.orderTracking?.orderStatus?.filter(
            (e) => e.statusId === 1 || e.statusId === 2 || e.statusId === 39
          ).length > 0 &&
          props.orderStatus?.orderTracking?.orderStatus?.filter(
            (e) => e.statusId === 1 || e.statusId === 2 || e.statusId === 39
          )[0]?.date
        }
        tileType={0} //no Active state
        dateType={dateType}
        customerId={props.customerId}
      />

      {props?.orderStatus?.orderTracking?.orderTypeStatus === 52 && (
        <>
          {!props?.orderStatus?.orderTracking?.orderStatus
            ?.map((e) => e.statusId)
            .includes(142) &&
            (props?.orderStatus?.orderTracking?.statusId === 1 ||
              props?.orderStatus?.orderTracking?.statusId === 2) && (
              <div className={"Timeline_dummyHeight"}></div>
            )}
          <TimelineTile
            done={props?.orderStatus?.orderTracking?.orderStatus
              ?.map((e) => e.statusId)
              .includes(39)}
            title={
              props?.orderStatus?.orderTracking?.orderStatus
                ?.map((e) => e.statusId)
                .includes(39)
                ? "Pharmacist Call"
                : "Call with Pharmacist"
            }
            active={
              !props?.orderStatus?.orderTracking?.orderStatus
                ?.map((e) => e.statusId)
                .includes(142) &&
              (props?.orderStatus?.orderTracking?.statusId === 1 ||
                props?.orderStatus?.orderTracking?.statusId === 2)
            }
            imgLogo={
              window.innerWidth >= 768 ? pharmacistCall : pharmacist_mweb
            }
            subText={"You will receive a pharmacist call"}
            desc={
              "They will call to confirm the medicines in your prescription"
            }
            tileType={1} //has Static Active Component
            date={
              props.orderStatus?.orderTracking?.orderStatus?.filter(
                (e) => e.statusId === 39
              ).length > 0 &&
              props.orderStatus?.orderTracking?.orderStatus?.filter(
                (e) => e.statusId === 39
              )[0]?.modifiedOn
            }
            dateType={dateType}
            customerId={props.customerId}
          />
        </>
      )}
      {window.innerWidth <= 768 &&
        !props?.orderStatus?.orderTracking?.orderStatus
          ?.map((e) => e.statusId)
          .includes(142) &&
        !props?.orderStatus?.orderTracking?.drCallTime &&
        props?.orderStatus?.orderTracking?.statusId !== 1 &&
        props?.orderStatus?.orderTracking?.statusId !== 2 && (
          <div className={"Timeline_dummyHeight"}></div>
        )}
      {props.updateDetail?.WorkflowStatusId === null ? (
        <TimelineTile
          done={
            (props?.orderStatus?.orderTracking?.orderStatus
              ?.map((e) => e.statusId)
              .includes(142) ||
              props?.orderStatus?.orderTracking?.drCallTime) &&
            props?.orderStatus?.orderTracking?.statusId !== 2
          }
          active={
            (!props?.orderStatus?.orderTracking?.orderStatus
              ?.map((e) => e.statusId)
              .includes(142) &&
              !props?.orderStatus?.orderTracking?.drCallTime &&
              props?.orderStatus?.orderTracking?.statusId !== 1 &&
              props?.orderStatus?.orderTracking?.statusId !== 2) ||
            props?.orderStatus?.orderTracking?.statusId === 39
          }
          imgLogo={window.innerWidth >= 768 ? DoctorCall : doctor_mweb}
          title={
            props?.orderStatus?.orderTracking?.orderStatus
              ?.map((e) => e.statusId)
              .includes(142) || props?.orderStatus?.orderTracking?.drCallTime
              ? "Doctor Consultation"
              : "FREE  Doctor Consultation"
          }
          subText={"You will receive a doctor call"}
          desc={
            "Make sure you ask our doctor about cost  effective options to save more on your order"
          }
          tileType={1} //has Static Active Component
          date={
            props.orderStatus?.orderTracking?.drCallTime &&
            props.orderStatus?.orderTracking?.orderTypeStatus !== 52
              ? props.orderStatus?.orderTracking?.drCallTime
              : props.orderStatus?.orderTracking?.orderStatus?.filter(
                  (e) => e.statusId === 142
                ).length > 0 &&
                props.orderStatus?.orderTracking?.orderStatus?.filter(
                  (e) => e.statusId === 142
                )[0]?.modifiedOn
          }
          dateType={dateType}
          isDocRating={
            (window.innerWidth <= 768 &&
              props?.orderStatus?.orderTracking?.orderStatus
                ?.map((e) => e.statusId)
                .includes(142)) ||
            props?.orderStatus?.orderTracking?.drCallTime
          }
          doctorData={props.doctorData}
          orderId={props.orderId}
          customerId={props.customerId}
          serviceCall={props.serviceCall}
        />
      ) : null}

      <TimelineTile
        done={
          props?.orderStatus?.orderTracking?.orderStatus
            ?.map((e) => e.statusId)
            .includes(142) && props.orderStatus?.orderTracking?.statusId !== 142
        }
        title={
          props?.orderStatus?.orderTracking?.orderStatus
            ?.map((e) => e.statusId)
            .includes(142) && props.orderStatus?.orderTracking?.statusId !== 142
            ? "Order Processed"
            : "Order is Being Processed"
        }
        active={props.orderStatus?.orderTracking.statusId == 142}
        date={
          props.orderStatus?.orderTracking?.orderStatus?.filter(
            (e) => e.statusId === 142
          ).length > 0 &&
          props.orderStatus?.orderTracking?.orderStatus?.filter(
            (e) => e.statusId === 142
          )[0]?.modifiedOn
        }
        imgLogo={window.innerWidth >= 768 ? orderProscessed : ""}
        tileType={2} //has Custom Active Component
        modifiedOn={
          props.orderStatus?.orderTracking?.orderStatus?.filter(
            (e) => e.statusId === 142
          ).length > 0 &&
          props.orderStatus?.orderTracking?.orderStatus?.filter(
            (e) => e.statusId === 142
          )[0]?.modifiedOn
        }
        dateType={dateType}
        customerId={props.customerId}
      >
        <ProcessingCard
          modifiedOn={
            props.orderStatus?.orderTracking?.orderStatus?.filter(
              (e) => e.statusId === 142
            ).length > 0 &&
            props.orderStatus?.orderTracking?.orderStatus?.filter(
              (e) => e.statusId === 142
            )[0]?.modifiedOn
          }
        />
      </TimelineTile>
      {/* {props.orderStatus?.orderTracking?.statusId == 58 &&
        !props.orderStatus?.orderTracking?.paymentTime && (
          <div className={"Timeline_tileWrap"}>
            <PaymentPendingTile
              orderId={props.orderId}
              access_token={props.access_token}
              customerId={props.customerId}
              history={props.history}
            />
            <div
              className={
                props.done ? "Timeline_tileSeperatorDone" : "Timeline_tileSeperator"
              }
            ></div>
          </div>
        )} */}
      {/* {props.orderStatus?.orderTracking?.paymentTime && (
        <TimelineTile
          done={true}
          title={"Payment Done"}
          active={false}
          date={props.orderStatus?.orderTracking?.paymentTime}
          tileType={0} //no Active state
          dateType={dateType}
        />
      )} */}
      {props.updateDetail?.paymentMode?.serialId === 16 &&
        props.updateDetail?.finalCalcAmt?.finalAmount >= 1 && (
          <TimelineTile
            done={
              props.orderStatus?.orderTracking?.paymentTime ||
              props?.orderStatus?.orderTracking?.orderStatus
                ?.map((e) => e.statusId)
                .includes(60) ||
              props?.orderStatus?.orderTracking?.orderStatus
                ?.map((e) => e.statusId)
                .includes(66)
            }
            title={
              props.orderStatus?.orderTracking?.paymentTime ||
              props?.orderStatus?.orderTracking?.orderStatus
                ?.map((e) => e.statusId)
                .includes(60) ||
              props?.orderStatus?.orderTracking?.orderStatus
                ?.map((e) => e.statusId)
                .includes(66)
                ? "Payment Done"
                : "Payment Pending"
            }
            active={
              props.orderStatus?.orderTracking?.statusId == 58 &&
              !props.orderStatus?.orderTracking?.paymentTime
            }
            imgLogo={window.innerWidth >= 768 ? "" : ""}
            date={props.orderStatus?.orderTracking?.paymentTime}
            tileType={3} //has Custom Active Shape Component
            dateType={dateType}
            customerId={props.customerId}
          >
            <div className={"Timeline_tileWrap"}>
              {props.orderStatus?.orderTracking?.statusId == 58 &&
              !props.orderStatus?.orderTracking?.paymentTime ? (
                <div className={"Timeline_dummyActiveTileHeight"}></div>
              ) : null}
              <PaymentPendingTile
                orderId={props.orderId}
                access_token={props.access_token}
                customerId={props.customerId}
                history={props.history}
                myOrder={props.myOrder}
                handlePaymentChange={props.handlePaymentChange}
              />
              <div
                className={
                  props.done
                    ? "Timeline_tileSeperatorDone"
                    : "Timeline_tileSeperator"
                }
              ></div>
            </div>
          </TimelineTile>
        )}

      <TimelineTile
        done={
          (props?.orderStatus?.orderTracking?.orderStatus
            ?.map((e) => e.statusId)
            .includes(60) &&
            props.orderStatus?.deliveryPartnerName) ||
          (props?.orderStatus?.orderTracking?.orderStatus
            ?.map((e) => e.statusId)
            .includes(60) &&
            props?.orderStatus?.orderTracking?.orderStatus
              ?.map((e) => e.statusId)
              .includes(55))
        }
        title={"Order Dispatched"}
        active={
          (props.orderStatus?.orderTracking?.statusId === 60 &&
            !props.orderStatus?.deliveryPartnerName) ||
          props.orderStatus?.orderTracking?.statusId === 66
        }
        imgLogo={window.innerWidth >= 768 ? Dispatched : ""}
        date={
          props.orderStatus?.orderTracking?.orderStatus?.filter(
            (e) => e.statusId === 60
          ).length > 0 &&
          props.orderStatus?.orderTracking?.orderStatus?.filter(
            (e) => e.statusId === 60
          )[0]?.modifiedOn
        }
        tileType={2} //has Custom Active Component
        dateType={dateType}
        customerId={props.customerId}
      >
        <DispatchCard
          subTitleArray={[
            {
              title: "Courier:",
              value: props.orderStatus?.orderTracking?.orderStatus?.filter(
                (e) => e.statusId === 60
              )[0]?.getShipmentSummaryResponse?.deliveryPartner,
            },
            {
              title: "Tracking Id:",
              value: props.orderStatus?.orderTracking?.orderStatus?.filter(
                (e) => e.statusId === 60
              )[0]?.getShipmentSummaryResponse?.awbNbr,
            },
          ]}
          title={"Order Dispatched"}
          clickPostTrackingUrl={props.orderStatus?.clickPostTrackingUrl}
          isPendingInfo={props.orderStatus?.orderTracking?.statusId === 66}
        />
      </TimelineTile>
      <TimelineTile
        done={
          props?.orderStatus?.orderTracking?.orderStatus
            ?.map((e) => e.statusId)
            .includes(55) && props.orderStatus?.orderTracking?.statusId !== 55
        }
        title={"Out for Delivery"}
        active={
          props.orderStatus?.orderTracking?.statusId === 60 &&
          props.orderStatus?.deliveryPartnerName
        }
        last={true}
        date={
          props.orderStatus?.orderTracking?.orderStatus?.filter(
            (e) => e.statusId === 55
          ).length > 0 &&
          props.orderStatus?.orderTracking?.orderStatus?.filter(
            (e) => e.statusId === 55
          )[0]?.modifiedOn
        }
        tileType={2}
        imgLogo={window.innerWidth >= 768 ? outForDelivery : ""}
        isSub={true}
        edd={props.updateDetail?.deliveryDate}
        dateType={dateType}
        customerId={props.customerId}
      >
        <DispatchCard
          subTitleArray={[
            {
              title: "Delivery Partner:",
              value: props.orderStatus?.deliveryPartnerName,
            },
            {
              title: "Contact No:",
              value: props.orderStatus?.deliveryPartnerNumber,
            },
          ]}
          title={"Out for Delivery"}
          clickPostTrackingUrl={props.orderStatus?.clickPostTrackingUrl}
        />
      </TimelineTile>
      {!props?.orderStatus?.orderTracking?.orderStatus
        ?.map((e) => e.statusId)
        .includes(55) &&
      !props.orderStatus?.orderTracking?.statusId !== 55 &&
      !props.orderStatus?.deliveryPartnerName ? (
        <div className={"Timeline_eddText"}>
          <span>{`Estimated Delivery By `}</span>
          <span>{getDeliveryDate(props.updateDetail?.deliveryDate, 2)}</span>
        </div>
      ) : (
        ""
      )}
    </div>
  );
};

export default Timeline;

const TimelineTile = (props) => {
  // const [openDialog, setOpenDialog] = useState(false);

  if (props.tileType === 3 && props.active) {
    return props.children;
  } else {
    return (
      <div className={"Timeline_tileWrap"}>
        {props.active ? (
          <div className={"Timeline_activeTile"}>
            <ActiveTile
              imgLogo={props.imgLogo}
              title={props.title}
              subText={props.subText}
              desc={props.desc}
              tileType={props.tileType}
              modifiedOn={props.modifiedOn}
            >
              {props.children}
            </ActiveTile>
          </div>
        ) : props.done ? (
          <DoneComponent
            title={props.title}
            date={props.date}
            dateType={props.dateType}
            isDocRating={props.isDocRating}
          />
        ) : (
          <div className={"Timeline_tileContentWrap"}>
            <div className={"Timeline_tileLogo"}>
              <div className={"Timeline_tilePendingLogo"}></div>
            </div>
            <div className={"Timeline_tileContentTitlePending"}>
              {props.title}
            </div>
          </div>
        )}

        {!props.last && (
          <>
            {props.doctorData && props.isDocRating && window.innerWidth <= 768 && (
              <div className={"Timeline_ratingMobDoc"}>
                <div
                  onClick={() => {
                    window.openSideBar(true, 11, {
                      doctorData: props.doctorData,
                      orderId: props.orderId,
                      customerId: props.customerId,
                      ratings: 0,
                      serviceCall: props.serviceCall,
                    });
                    // setOpenDialog(true);
                  }}
                  className={"Timeline_ratingMobDocText"}
                >
                  View doctor details
                </div>
                <div className={"Timeline_ratingMobDocIcon"}>
                  <img src={chevron_rate_doctor} />
                </div>
              </div>
            )}
            <div
              className={
                props.done
                  ? "Timeline_tileSeperatorDone"
                  : "Timeline_tileSeperator"
              }
            ></div>
          </>
        )}

        {/* {window.innerWidth <= 768 ? (
          <Dialog
            fullScreen={false}
            open={openDialog}
            aria-labelledby="responsive-dialog-title"
            classes={{ paper: "cancel-rectangle" }}
            id="responsive-dialog-title"
            onClose={() => setOpenDialog(false)}
            TransitionComponent={Transition}
            transitionDuration={400}
          >
            <div>
              <div className={"Timeline_doctorHeaderWrapper"}>
                <div className="closeBtn" onClick={() => setOpenDialog(false)}>
                  <img src={crossCloseMob} alt="close" />
                </div>
                <div className={"Timeline_doctorHeaderWrapperTitle"}>
                  Doctor Details
                </div>
              </div>
              <div className={"Timeline_doctorDetailWrap"}>
                <DoctorDetails
                  doctorData={props.doctorData}
                  orderId={props.orderId}
                  customerId={props.customerId}
                  ratings={0}
                  serviceCall={props.serviceCall}
                />
              </div>
            </div>
          </Dialog>
        ) : null} */}
      </div>
    );
  }
};

const DoneComponent = (props) => {
  return (
    <div className={"Timeline_doneWrap"}>
      <div className={"Timeline_tileContentWrap"}>
        <div className={"Timeline_tileLogo"}>
          <img src={orderStatus} />
        </div>
        <div className={"Timeline_tileContentTitle"}>{props.title}</div>
      </div>
      <div className={"Timeline_dateWrap"}>
        {props.date &&
          (props.dateType === 1
            ? moment(props.date).format("dddd, h:mm A")
            : moment(props.date).format("Do MMM, h:mm A"))}
      </div>
    </div>
  );
};
