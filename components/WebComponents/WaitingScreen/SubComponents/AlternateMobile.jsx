///? Default IMPORTS
import React, { useCallback, useEffect, useState } from "react";
import { connect } from "react-redux";
import { withRouter } from "next/router";
import Dialog from "@material-ui/core/Dialog";
import Slide from "@material-ui/core/Slide";
///? Service IMPORTS
import { saveAltMobThunk } from "../../../../redux/PatientDetails/Action";
import { orderStatusThunk } from "../../../../redux/OrderDetail/Action";
import window from "global";

///? Component & Styling IMPORTS
import { Button, Input, message } from "antd";
import {
  AddMobWrapper,
  AltMobileWrapper,
  RegisterMobWrapper,
  TextLinks,
} from "./AlternateMobile.styled";
import { Field, Formik } from "formik";
import "./AlternateMobile.css";
import crossCloseMob from "../../../WebComponents/Navigation/mobX.svg";
import closeCross from "../../../WebComponents/Navigation/closecross.svg";
///? Constants

let isAltService = false;

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const AlternateMobile = (props) => {
  const [altNumber, setAltNumber] = useState("");
  const [isEditScreenShow, setIsEditScreenShow] = useState(false);
  const [isAltEdit, setIsAltEdit] = useState(false);
  const [isMobAltModal, setMobAltModal] = useState(false);
  const [openKeyBoard, setOpenKeyBoardn] = useState(false);

  useEffect(() => {
    if (isMobAltModal) {
      setTimeout(() => {
        setOpenKeyBoardn(true);
      }, 2000);
    }
  }, [isMobAltModal]);

  useEffect(() => {
    if (props.altMob) {
      // setAltNumber(null);
      setAltNumber(props.altMob);
      setIsEditScreenShow(true);
    }
  }, [props.altMob, altNumber]);

  const cancelFunc = () => {
    if (isAltEdit) {
      setIsAltEdit(false);
    } else {
      setIsEditScreenShow(false);
    }
  };

  const validateMobile = (value) => {
    let error;
    if (!value) {
      error = "Enter a mobile number";
    } else if (!/^[6-9]\d{9}$/.test(value)) {
      error = "Enter a valid mobile number";
    } else if (value == props.currentMobNo) {
      error = "This mobile number is already registered";
    } else {
      error = "";
    }
    return error;
  };

  //* AltMobile Success Msg
  useEffect(() => {
    message.destroy();

    if (props.altMobCallSuccess && isAltService) {
      isAltService = false;
      message.success(props.altMobCallSuccess);
    }
  }, [props.altMobCallSuccess]);

  //* AltMobile Error Msg
  useEffect(() => {
    message.destroy();

    if (props.altMobCallError && isAltService) {
      isAltService = false;
      message.success(props.altMobCallError);
    }
  }, [props.altMobCallError]);

  //* Add/Update mobile function
  const submitAltMobileFunc = (value) => {
    message.loading("Loading...", 0);
    props
      .saveAltMobThunk({
        access_token: props.accessToken,
        orderId: props.orderId,
        alternateNumber: value,
        history: props.history,
      })
      .then(() => {
        isAltService = true;
        props.serviceCall(true);
        setMobAltModal(false);
        if (props.altMobCallError) {
        } else {
          props.serviceCall(true);
          cancelFunc();
        }
      });
  };

  return (
    <AltMobileWrapper>
      <RegisterMobWrapper>
        <div className="registeredNumberTitleWrap">
          <div className="registeredNumberTitle">Registered Mobile No:</div>
          <div className="registeredNumberValue">{props.currentMobNo}</div>
        </div>
        <TextLinks
          hide={altNumber && !isAltEdit ? altNumber : null}
          onClick={() => {
            window.innerWidth >= 768
              ? setIsEditScreenShow(true)
              : setMobAltModal(true);
          }}
        >
          Add Alternate Contact
        </TextLinks>
      </RegisterMobWrapper>

      {isEditScreenShow && altNumber && !isAltEdit ? (
        <RegisterMobWrapper altNum>
          <div className="registeredMobHeader">
            <div className="registeredNumberTitleWrap">
              <div className="registeredNumberTitle">Alternate Mobile No:</div>
              <div className="registeredNumberValue">{altNumber}</div>
            </div>
            <TextLinks
              onClick={() => {
                window.innerWidth >= 768
                  ? setIsAltEdit(true)
                  : setMobAltModal(true);
              }}
            >
              Edit
            </TextLinks>
          </div>
          <p className="registeredNumberMsg">
            You will get notified on this Mobile number
          </p>
        </RegisterMobWrapper>
      ) : isEditScreenShow ? (
        <AddMobWrapper>
          <div className="addFormWrap">
            <Formik
              initialValues={{ mobile: altNumber }}
              onSubmit={(values) => {
                let data = values.mobile.toString();
                submitAltMobileFunc(data);
              }}
            >
              {({ errors, handleSubmit, touched, values }) => (
                <form
                  noValidate
                  onSubmit={handleSubmit}
                  className="registeredNumberForm"
                >
                  <div
                    className={
                      errors.mobile && touched.mobile
                        ? "firstContentError"
                        : "firstContent"
                    }
                  >
                    <Field
                      label="Enter your mobile number"
                      placeholder={
                        errors.mobile && touched.mobile
                          ? ""
                          : "Enter your mobile number"
                      }
                      type="number"
                      style={{ textIndent: "0.5em", fontWeight: "600" }}
                      name="mobile"
                      value={values.mobile}
                      validate={validateMobile}
                      autoFocus={true}
                    />
                  </div>
                  <button className="bbtn snAlternateBtn" type="submit">
                    Save Number
                  </button>

                  <div className="ctaWrap">
                    {errors.mobile && touched.mobile ? (
                      <div className="registeredNumberErrorMsg">
                        {errors.mobile}
                      </div>
                    ) : (
                      ""
                    )}

                    <TextLinks
                      hide={altNumber && !isAltEdit ? altNumber : null}
                      onClick={() => cancelFunc()}
                    >
                      Cancel
                    </TextLinks>
                  </div>
                </form>
              )}
            </Formik>
          </div>
        </AddMobWrapper>
      ) : (
        ""
      )}
      <Dialog
        open={isMobAltModal}
        onClose={() => setMobAltModal(false)}
        aria-labelledby="responsive-dialog-title"
        className="addAlternateWrap"
        TransitionComponent={Transition}
        transitionDuration={400}
      >
        <div className="dailogueWrap">
          <div className="closeBtn" onClick={() => setMobAltModal(false)}>
            <img
              src={window.innerWidth > 468 ? closeCross : crossCloseMob}
              alt="close"
            />
          </div>
          <div className="headerAlternateNumber">Add Alternate Number</div>
          <div className="addFormWrap">
            <Formik
              initialValues={{ mobile: altNumber }}
              onSubmit={(values) => {
                let data = values.mobile.toString();
                submitAltMobileFunc(data);
              }}
            >
              {({ errors, handleSubmit, touched, values }) => (
                <form
                  noValidate
                  onSubmit={handleSubmit}
                  className="registeredNumberForm"
                >
                  <div
                    className={
                      errors.mobile && touched.mobile
                        ? "firstContentError"
                        : "firstContent"
                    }
                  >
                    <Field
                      label="Enter your mobile number"
                      placeholder={
                        errors.mobile && touched.mobile
                          ? ""
                          : "Enter your mobile number"
                      }
                      type="number"
                      style={{
                        textIndent: "0.5em",
                        fontWeight: "600",
                      }}
                      name="mobile"
                      value={values.mobile}
                      validate={validateMobile}
                      autoFocus={true}
                    />
                    <div className="ctaWrap">
                      {errors.mobile && touched.mobile ? (
                        <div className="registeredNumberErrorMsg">
                          {errors.mobile}
                        </div>
                      ) : (
                        ""
                      )}
                    </div>
                  </div>

                  <button className="bbtn snAlternateBtn" type="submit">
                    Save Number
                  </button>
                </form>
              )}
            </Formik>
          </div>
        </div>
      </Dialog>
    </AltMobileWrapper>
  );
};

const mapStateToProps = (state) => ({
  accessToken: state.loginReducer?.verifyOtpSuccess?.Response?.access_token,
  userDto: state.loginReducer?.verifyOtpSuccess?.CustomerDto,
  altMobCallSuccess: state.patientData?.saveAltMobSuccess,
  altMobCallError: state.patientData?.saveAltMobError,
});

export default withRouter(
  connect(mapStateToProps, { saveAltMobThunk, orderStatusThunk })(
    AlternateMobile
  )
);
