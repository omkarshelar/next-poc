import React, { Component } from "react";
import "./SavingsCardLoader.css";
import Shimmer from "react-shimmer-effect";

export default class SavingsCardLoader extends Component {
  render() {
    return (
      <Shimmer>
        <div className="savingsCardShimmer" />
      </Shimmer>
    );
  }
}
