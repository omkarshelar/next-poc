import Shimmer from "react-shimmer-effect";
import React from "react";
import "./MyOrderShimmer.module.css";
import StatusCardLoader from "../../../pageComponents/myorderdetails/ShimmerComponents/StatusCardLoader";
import ProductCardLoader from "./ComponentWise/ProductCardLoader";
import OrderStatusSummaryLoader from "./ComponentWise/OrderStatusSummaryLoader";
import window from "global";

const MyOrdersShimmer = (props) => {
  return (
    <div className={"myOrders_MyOrdersShimmerWrapper"}>
      <StatusCardLoader />
      <div className={"myOrders_orderHeaderWrapper"}>
        <div className={"myOrders_orderHeaderWrapperRow"}>
          <Shimmer>
            <div className={"myOrders_orderHeaderLeftdiv"}></div>
          </Shimmer>
          <Shimmer>
            <div className={"myOrders_orderHeaderRightdiv"}></div>
          </Shimmer>
        </div>
        <div className={"myOrders_orderHeaderWrapperRow"}>
          <Shimmer>
            <div className={"myOrders_orderHeaderLeftdiv"}></div>
          </Shimmer>
          <Shimmer>
            <div className={"myOrders_orderHeaderRightdiv"}></div>
          </Shimmer>
        </div>
        <div className={"myOrders_orderHeaderWrapperRow"}>
          <Shimmer>
            <div className={"myOrders_orderHeaderLeftdiv"}></div>
          </Shimmer>
          <Shimmer>
            <div className={"myOrders_orderHeaderRightdiv"}></div>
          </Shimmer>
        </div>
      </div>
      {window.innerWidth <= 768 ? (
        <div>
          <Shimmer>
            <div className={"myOrders_savingsMobWrapper"}></div>
          </Shimmer>
        </div>
      ) : null}
      <div className={"myOrders_productListWrapper"}>
        <Shimmer>
          <div className={"myOrders_productListHeader"}></div>
        </Shimmer>
        <ProductCardLoader />
      </div>
      <div>
        <Shimmer>
          <div className={"myOrders_AddressSection"}></div>
        </Shimmer>
      </div>
      <div>
        <Shimmer>
          <div className={"myOrders_paymentSection"}></div>
        </Shimmer>
      </div>
      <OrderStatusSummaryLoader />
      <div>
        <Shimmer>
          <div className={"myOrders_returnWrapper"}></div>
        </Shimmer>
      </div>
    </div>
  );
};

export default MyOrdersShimmer;
