import React, { Component } from "react";
import "./MedCardLoader.css";
import Skeleton from "@mui/material/Skeleton";

export default class MedCardLoader extends Component {
  render() {
    return (
      <div>
        <div className="orderFlowMedCardLoaderMainContainer">
          <div className="orderFlowMedCardSquareMainLoader">
            <div className="orderFlowMedCardSquareLoader">
              <Skeleton animation="wave" height={90} />
            </div>
            <div className="orderFlowMedCardLineMainLoader">
              <div className="orderFlowMedCardFirstLineLoader">
                <Skeleton animation="wave" height={25} />
              </div>
              <div className="orderFlowMedCardFirstLineLoader">
                <Skeleton animation="wave" height={25} />
              </div>
              <div className="orderFlowMedCardFirstLineLoader">
                <Skeleton animation="wave" height={25} />
              </div>
            </div>
          </div>
          <div className="orderFlowMedCardLineMainLoader">
            <div className="orderFlowMedCardSecondLineLoader">
              <Skeleton animation="wave" height={25} />
            </div>
            <div className="orderFlowMedCardSecondLineLoader">
              <Skeleton animation="wave" height={25} />
            </div>
          </div>
          <div className="orderFlowMedCardRectangleLoader">
            <Skeleton animation="wave" height={60} />
          </div>
        </div>
        <div className="orderFlowMedCardLoaderSubsMainContainer">
          <Skeleton animation="wave" height={90} />
        </div>
      </div>
    );
  }
}
