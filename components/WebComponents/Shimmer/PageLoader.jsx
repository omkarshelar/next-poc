import React, { Component } from "react";
import Shimmer from "react-shimmer-effect";
import {
  BigRectangle,
  MediumRectangle,
  PageLoaderWrapper,
  SmallRectangle,
} from "./PageLoader.style";

export default class PageLoader extends Component {
  render() {
    return (
      <PageLoaderWrapper>
        <Shimmer>
          <MediumRectangle />
          <MediumRectangle />
          <MediumRectangle />
          <SmallRectangle />
          <SmallRectangle />
          <SmallRectangle />
          <SmallRectangle />
        </Shimmer>
      </PageLoaderWrapper>
    );
  }
}
