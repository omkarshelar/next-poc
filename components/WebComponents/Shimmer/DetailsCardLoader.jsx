import React, { Component } from "react";
import Shimmer from "react-shimmer-effect";
import "./DetailsCard.css";

export default class DetailsCardLoader extends Component {
  render() {
    return (
      <>
        {this.props.onlyCard && (
          <Shimmer>
            <div className="detailsCardMainLoader" />
          </Shimmer>
        )}
        {this.props.allDetails && (
          <div className="allDetailsCardMainLoader">
            <Shimmer>
              <div className="allDetailsCardFirstLineLoader" />
              <div className="allDetailsCardRectLoader" />
              <div className="allDetailsCardSecondLineLoader" />
              <div className="allDetailsCardButtonLoader" />
            </Shimmer>
          </div>
        )}
      </>
    );
  }
}
