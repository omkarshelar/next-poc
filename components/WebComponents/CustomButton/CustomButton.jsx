import React from "react";
import { CustomButtonContainer } from "./CustomButton.Style";

function CustomButton({ children, ...props }) {
  return (
    <CustomButtonContainer {...props} className="btn">
      {children}
    </CustomButtonContainer>
  );
}

export default CustomButton;
