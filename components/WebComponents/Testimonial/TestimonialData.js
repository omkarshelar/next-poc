export const TestimonialContent = [
  {
    id: 1,
    data:
      "Perfect. The more I use this app, the more I fall in love with it. Doctors are very professional and customer friendly.",
    author: "Subhash Sehgal",
  },
  {
    id: 2,
    data:
      "Excellent app. Have used this regularly and found it very easy to use. All info is readily available and the response after order placement for validation of medicines required was prompt",
    author: "Snehal Shah",
  },
  {
    id: 3,
    data:
      "Truemeds is the best ... during the Lockdown this app do not reduce the discount, which shows the customer friendly nature of the TrueMeds. Thank You!!",
    author: "Laksh Kankariya",
  },
  {
    id: 4,
    data: "Affordable medicines on this app. Truemeds is true.",
    author: "Zahiruddin Warekar",
  },
  {
    id: 5,
    data:
      "I found the app very useful and easy to use, when anyone adds medicine it also shows affordable substitutes which is really appreciable. Very few companies really care for customer like Trumeds.",
    author: "Raj Kamal gupta",
  },
  {
    id: 6,
    data: "Got all my meds on time and at such a lower cost! Love this brand!",
    author: "Sumit Kumar",
  },
  {
    id: 7,
    data:
      "This app has a user friendly interface. It has really great service of quick delivery. All the prices are affordable and there are regular discounts on the medicines so that i doesn't face any type of difficulty in purchasing the required medications.",
    author: "Ashish Bhatia",
  },
  {
    id: 8,
    data:
      "Loved the experience of ordering medicines from TrueMeds. Great UX and could upload prescription easily. The doctor called within 15 mins to confirm and suggest alternatives. Very prompt and professional keep it up.",
    author: "Dada bhai",
  },
];
