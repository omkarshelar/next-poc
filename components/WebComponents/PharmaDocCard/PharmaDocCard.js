import React from "react";
import "./PharmaDocCard.css";
import pharmaIcon from "../../../src/Assets/pharma-call.svg";
import docIcon from "../../../src/Assets/doc-call.svg";

export default function PharmaDocCard(props) {
  return (
    <div className="outerWrap">
      <div
        className={props.isDoc ? "pharmaDocContainerNew" : "pharmaDocContainer"}
      >
        {props.isDoc ? (
          <div className="docCard">
            <img src={docIcon} alt="doctor" />
            <p>
              Don't worry! You will get a call from a trusted Truemeds Doctor
              for <span>₹249</span> <span>FREE</span>
            </p>
          </div>
        ) : (
          <div className="pharmaCard">
            <img src={pharmaIcon} alt="pharmacist" />
            <p>
              Our pharmacist will call to confirm the medicines in your
              prescription.
            </p>
          </div>
        )}
      </div>
    </div>
  );
}
