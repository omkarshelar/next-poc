export let featuredMedicineData = [
  {
    id: 1,
    status: "Deals For You",
    checkDefault: true,
  },
  {
    id: 2,
    status: "Medicines you bought",
    checkDefault: false,
  },
];

export let featuredMedicineOnly = [
  {
    id: 1,
    status: "Deals For You",
    checkDefault: true,
  },
];
