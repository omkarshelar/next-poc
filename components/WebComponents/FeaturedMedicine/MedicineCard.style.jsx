import styled from "styled-components";
import { CarouselItem, Carousel } from "react-bootstrap";

export const CardContainer = styled.div`
  box-sizing: border-box;
  min-width: 262px;
  border-radius: 10px;
  background-color: #ffffff;
  margin: 10px;
  padding: 20px 10px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  > div[class="card-imag-featured"] {
    width: 150px;
    height: 150px;
    margin: auto;
  }
  @media screen and (max-width: 568px) {
    min-width: 240px;
  }
  @media screen and (max-width: 468px) {
    min-width: 150px;
    width: 150px;
    padding: 10px;
    > div[class="card-imag-featured"] {
      width: 50px;
      height: 50px;
      margin: auto;
    }
  }
`;

export const CardInfo = styled.div`
  text-align: center;
  margin: 10px 0;
  display: flex;
  flex-flow: column;
  flex-grow: 1;
  > h6 {
    /* font-family: "Century Gothic", sans-serif; */
    font-size: 14px;
    font-style: normal;
    font-weight: 400;
    line-height: 20px;
    letter-spacing: 0px;
    text-align: center;
  }
  > p {
    font-size: 16px;
    font-style: normal;
    font-weight: 400;
    line-height: 21px;
    letter-spacing: 0em;
    text-align: center;
    color: #999999;
  }
  del {
    /* font-family: "Century Gothic", sans-serif; */
    font-size: 16px;
    font-style: normal;
    font-weight: 700;
    line-height: 29px;
    letter-spacing: 0px;
    text-align: center;
  }

  > span {
    /* font-family: "Century Gothic", sans-serif; */
    font-size: 16px;
    font-style: normal;
    font-weight: 700;
    line-height: 29px;
    letter-spacing: 0px;
    text-align: center;
    margin-bottom: 0.7rem;
    display: block;
  }

  .h6 {
    font-size: 16px;
    font-style: normal;
    line-height: 20px;
    letter-spacing: 0px;
    text-align: center;
    margin-top: 0;
    margin-bottom: 0.5em;
    color: rgba(0, 0, 0, 0.85);
    font-weight: 400;
  }
  @media screen and (max-width: 468px) {
    > h6,
    p,
    del,
    span {
      font-size: 12px;
      margin-bottom: 0px;
    }
    > span {
      line-height: 20px;
    }
  }
`;

export const CarouselItemStyle = styled(CarouselItem)`
  width: 150px;
  height: 150px;
  margin: auto;
  > img {
    width: 100%;
    height: 100%;
    object-fit: contain;
  }
  @media screen and (max-width: 468px) {
    width: 50px;
    height: 50px;
    margin: auto;
  }
`;

export const CarouseStyle = styled(Carousel)`
  .carousel-inner {
    position: relative;
    width: 100%;
    overflow: hidden;
    height: 150px !important;
  }
  .carousel-control-prev-icon {
    background-image: ${(props) => `url(${props.prev})`};
    width: 30px;
    height: 30px !important;
  }

  .carousel-control-next-icon {
    background-image: ${(props) => `url(${props.next})`};
    width: 30px;
    height: 30px !important;
  }
  .carousel-control-prev {
    transform: translateX(-32px);
    opacity: 1;
    width: 30px !important;
  }
  .carousel-control-next {
    transform: translateX(32px);
    opacity: 1;
    width: 30px !important;
  }
  @media screen and (max-width: 468px) {
    .carousel-inner {
      height: 50px !important;
    }
    .carousel-control-prev-icon {
      width: 25px;
      height: 25px !important;
    }

    .carousel-control-next-icon {
      width: 25px;
      height: 25px !important;
    }
    .carousel-control-prev {
      width: 25px !important;
    }
    .carousel-control-next {
      width: 25px !important;
    }
  }
`;

export const ItemAdded = styled.div`
  display: flex;
  width: 100%;
  justify-content: space-between;

  .css-b62m3t-container {
    width: 100px !important;
    /* position: unset !important; */
    @media screen and (max-width: 468px) {
      width: 80px !important;
    }
  }

  .css-1s2u09g-control,
  .css-1pahdxg-control {
    border: 1.6px solid #0071bc !important;
    border-radius: 6px;
    height: 44px !important;
    @media screen and (max-width: 468px) {
      height: 30px !important;
    }
  }

  .css-1okebmr-indicatorSeparator {
    display: none !important;
  }

  .css-tj5bde-Svg,
  .css-qc6sy-singleValue {
    color: #0071bc !important;
    font-family: "Inter";
  }

  .css-1pahdxg-control:hover {
    border-color: #0071bc !important;
  }

  .css-2613qy-menu div {
    color: #0071bc !important;
  }

  .css-1n7v3ny-option {
    background-color: #f5f5f5;
    color: #0071bc !important;
    font-size: 14px;
    font-weight: 400;
    font-family: "Inter";
  }

  .css-tlfecz-indicatorContainer {
    color: #0071bc !important;
  }
  .css-9gakcf-option {
    background-color: #e6f7ff;
    color: #0071bc !important;
  }

  .css-yt9ioa-option {
    color: #0071bc !important;
    font-size: 14px;
    font-weight: 400;
    font-family: "Inter";
  }

  .css-14el2xx-placeholder {
    color: #0071bc !important;
    font-size: 14px;
    font-weight: 400;
    font-family: "Inter";
  }

  .css-26l3qy-menu {
    z-index: 2;
    width: 100px;
    /* top: unset; */
  }

  .css-4ljt47-MenuList::-webkit-scrollbar {
    width: 5px;
  }

  .css-4ljt47-MenuList::-webkit-scrollbar-thumb {
    border-radius: 73px;
    background: #a9a9a9;
  }

  .css-4ljt47-MenuList::-webkit-scrollbar-track {
    background: transparent;
  }

  > div[class="remove"] {
    order: 2;
    display: flex;
    align-items: center;
    > img {
      width: 25px;
    }
    span {
      color: #dd2200;
      /* font-family: "Raleway", sans-serif; */
      font-size: 14px;
      font-style: normal;
      font-weight: 600;
      line-height: 16px;
      letter-spacing: 0.02em;
      text-align: center;
      padding-left: 4px;
    }
    @media screen and (max-width: 468px) {
      order: 1;
      img {
        display: none;
      }
    }
  }
  > div[class="quntity-select"] {
    display: flex;
    align-items: center;
    justify-content: flex-end;
    order: 2;
    font-size: 14px;
    font-style: normal;
    font-weight: 700;
    line-height: 18px;
    .medCardQuantityContainer .ant-select {
      width: 80px !important;
    }

    .medCardQuantityContainer
      .ant-select:not(.ant-select-customize-input)
      .ant-select-selector {
      border: 1px solid #999 !important;
    }

    .medCardQuantityContainer
      .ant-select-focused:not(.ant-select-disabled).ant-select:not(.ant-select-customize-input)
      .ant-select-selector {
      border: 1px solid #0071bc !important;
      box-shadow: 0 0 0 2px rgb(0 113 188 / 20%) !important;
    }

    .medCardQuantityContainer
      .ant-select-single.ant-select-show-arrow
      .ant-select-selection-item,
    .ant-select-single.ant-select-show-arrow .ant-select-selection-placeholder {
      font-weight: 500 !important;
      font-size: 14px !important;
      font-family: "Inter", sans-serif !important;
      color: #333 !important;
    }
  }
`;
