import styled from "styled-components";

export const AddressListWrapper = styled.div`
  > div {
    width: 100%;
    max-width: 300px;
    max-height: 140px;
    overflow-y: auto;
  }
  > div::-webkit-scrollbar {
    width: 2px;
  }
  > div::-webkit-scrollbar-thumb {
    background: #0071bc;
    border-radius: 10px;
  }

  > div::-webkit-scrollbar-track {
    background: #c6e8ff;
  }
  > div > div[class="outerWrapper"],
  > div > div[class="outerWrapper selected"] {
    padding: 10px;
    border: 1px solid #ccc;
    border-bottom: 0px;
    cursor: pointer;
    position: relative;
    > div[class="addressWrapper"] {
      display: flex;
      /* font-family: "Century Gothic"; */
      color: #003055;
      font-weight: 600;
      font-size: 14px;
      > div[class="seperator"] {
        padding: 0px 3px;
      }
    }
    > div[class="defaultSelect"] {
      font-size: 11px;
      color: #999999;
      /* font-family: "Raleway"; */
    }
    > div[class="orderFlowSteperSelectedElement"] {
      width: 18px;
      height: 18px;
      display: flex;
      align-items: center;
      justify-content: center;
      background-color: #30b94f;
      border-radius: 20px;
      margin-right: 10px;
      position: absolute;
      right: 10px;
      top: 0;
      bottom: 0;
      margin: auto;
      > img {
        width: 10px;
      }
    }
  }

  > div > div[class="outerWrapper"]:first-child,
  > div > div[class="outerWrapper selected"]:first-child {
    border-top-left-radius: 6px;
    border-top-right-radius: 6px;
  }

  > div > div[class="outerWrapper"]:last-child,
  > div > div[class="outerWrapper selected"]:last-child {
    border-bottom-left-radius: 6px;
    border-bottom-right-radius: 6px;
    border-bottom: 1px solid #ccc;
  }

  > div > div[class="outerWrapper selected"] {
    background-color: #f5fbff;
    border: 1px solid #0071bc;
  }

  > div > div[class="outerWrapper selected"]:last-child {
    background-color: #f5fbff;
    border: 1px solid #0071bc;
    border-bottom: 1px solid #0071bc;
  }

  @media screen and (max-width: 468px) {
    > div {
      max-width: 100%;
    }
  }
`;

export const SpinnerWrapper = styled.div`
  position: absolute;
  right: 10px;
  top: 0;
  bottom: 0;
  margin: auto;
  display: flex;
  align-items: center;
  ${AddressListWrapper}::-webkit-scrollbar {
    width: 2px;
  }
`;

export const ScrollBarProps = styled.div`
  ${AddressListWrapper}::-webkit-scrollbar {
    width: 2px;
  }
`;
