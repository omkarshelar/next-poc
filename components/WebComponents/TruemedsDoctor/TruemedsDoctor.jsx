import React, { Component, createRef } from "react";
import DoctorCard from "./DoctorCard";
import { DoctorWrapper, Heading, DoctorList } from "./TruemedsDoctor.style";
import next from "../Upload/next.svg";
import prev from "../Upload/prev.svg";
import Button from "../../WebComponents/Button/Button";
import DoctorCardLoader from "./DoctorCardLoader";

export default class TruemedsDoctor extends Component {
  constructor(props) {
    super(props);
    this.ListRef = createRef();
  }
  scrollRight = () => {
    if (this.ListRef.current) {
      this.ListRef.current.scrollLeft = this.ListRef.current.scrollLeft + 400;
    }
  };
  scrollLeft = () => {
    if (this.ListRef.current) {
      this.ListRef.current.scrollLeft = this.ListRef.current.scrollLeft - 400;
    }
  };

  render() {
    let { doctor } = this.props;
    return (
      <DoctorWrapper>
        <Heading>
          <h2>Meet our doctors</h2>
          <p>
            Our empanelled team of doctors will help you save up to 72% on every
            order.
          </p>
          <div className="scroll-btn">
            <Button onClick={this.scrollLeft}>
              <img src={prev} alt="left"></img>
            </Button>
            <Button onClick={this.scrollRight}>
              <img src={next} alt="right"></img>
            </Button>
          </div>
        </Heading>
        <DoctorList ref={this.ListRef}>
          {this.props.isLoading1 ? (
            <DoctorCardLoader />
          ) : doctor ? (
            doctor.map((data) => (
              <DoctorCard data={data} isMobile={this.props.isMobile} />
            ))
          ) : (
            <DoctorCardLoader />
          )}
        </DoctorList>
      </DoctorWrapper>
    );
  }
}
