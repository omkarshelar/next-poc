import React, { Component } from "react";
import Shimmer from "react-shimmer-effect";
import "./DoctorCardLoader.css";
export class DoctorCardLoader extends Component {
  render() {
    return (
      <div className="blank-med-loader-doctor-card-container">
        <div className="blank-med-loader-doctor-card-wrapper">
          <Shimmer>
            <div className="blank-med-loader-card-item-doctor-circle"></div>
            <div className="blank-med-loader-card-item-doctor-line-one"></div>
            <div className="blank-med-loader-card-item-doctor-line-two"></div>
            <div className="blank-med-loader-card-item-doctor-line-one"></div>
          </Shimmer>
        </div>
        <div className="blank-med-loader-doctor-card-wrapper">
          <Shimmer>
            <div className="blank-med-loader-card-item-doctor-circle"></div>
            <div className="blank-med-loader-card-item-doctor-line-one"></div>
            <div className="blank-med-loader-card-item-doctor-line-two"></div>
            <div className="blank-med-loader-card-item-doctor-line-one"></div>
          </Shimmer>
        </div>
        <div className="blank-med-loader-doctor-card-wrapper">
          <Shimmer>
            <div className="blank-med-loader-card-item-doctor-circle"></div>
            <div className="blank-med-loader-card-item-doctor-line-one"></div>
            <div className="blank-med-loader-card-item-doctor-line-two"></div>
            <div className="blank-med-loader-card-item-doctor-line-one"></div>
          </Shimmer>
        </div>
      </div>
    );
  }
}

export default DoctorCardLoader;
