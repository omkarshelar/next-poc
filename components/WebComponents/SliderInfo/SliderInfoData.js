import first from "../../../src/Assets/FirstSlider.webp";
import second from "../../../src/Assets/SecondSlider.webp";
import third from "../../../src/Assets/ThirdSlider.webp";
export let sliderInfoData = [
  {
    id: 1,
    header: "Compare options",
    Info: "Tell us which medicine you're looking to buy and our doctors will recommend quality alternatives for your branded medicines.",
    imgSrc: first,
    haveButton: false,
  },
  {
    id: 2,
    header: "Select option",
    Info: "Choose a medicine that's effective on your health and cost effective.",
    imgSrc: second,
    haveButton: false,
  },
  {
    id: 3,
    header: "Buy your medicines",
    Info: "Confirm your preferred choice of medicines and have delivered to you for free.",
    imgSrc: third,
    haveButton: true,
  },
];
