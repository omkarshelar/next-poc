import React from "react";
import { connect } from "react-redux";
import "./PaymetModal.Style.css";
import circleClose from "../../../src/Assets/CircleClose.png";

function ChangePaymentModal({
  handleClose,
  cashFreeService,
  paymentModeChange,
}) {
  return (
    <div className="paymentModalWrapper">
      <div className="paymentModalBody">
        <span className="top-right" onClick={() => handleClose()}>
          <img
            src={circleClose}
            alt="cancel"
            style={{ width: "22px", height: "22px" }}
          ></img>
        </span>
        <div className="paymentHeader">
          <span className="paymentSpan">
            <span id="pay1">Please</span>
            <span id="pay2"> confirm</span>
          </span>
        </div>
        <div className="change-payment">
          Are you sure you want to change payment mode to COD for this order ?
        </div>
        <div className="button-div">
          <button onClick={paymentModeChange}>Change to COD</button>
          <button onClick={cashFreeService} className="pay-btn">
            Make Online Payment
          </button>
        </div>
      </div>
    </div>
  );
}

export default connect(null)(ChangePaymentModal);
