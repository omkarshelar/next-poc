import styled from "styled-components";

export const RecommendationWrapper = styled.div`
  margin-top: 1rem;
`;
export const Recommendation = styled.div`
  display: flex;
  box-sizing: border-box;
  width: 70%;
  margin: 0 auto;
  background-color: #ffffff;
  padding: 3rem 5rem;
  border-radius: 10px;
  > div[class="recommend-header"] {
    box-sizing: border-box;
    width: 50%;
    > h2 {
      width: 70%;
      /* font-family: "Century Gothic", sans-serif; */
      font-size: 30px;
      font-style: normal;
      font-weight: 700 !important;
      line-height: 49px;
      letter-spacing: 0px;
      text-align: left;
      color: #003055;
      letter-spacing: 1px;
    }
  }
  > div[class="company"] {
    box-sizing: border-box;
    width: 50%;
    > img {
      width: 100%;
    }
  }
  @media screen and (max-width: 568px) {
    width: 100%;
    padding: 3rem 1rem;
    > div[class="recommend-header"] {
      width: 100%;
      > h2 {
        text-align: center;
        width: 100%;
      }
    }
  }
`;

export const Alternative = styled.div`
  > h3 {
    /* font-family: "Century Gothic", sans-serif; */
    font-size: 25px;
    font-style: normal;
    font-weight: 700;
    line-height: 37px;
    letter-spacing: 0px;
    text-align: center;
    color: #003055;
  }

  width: 70%;

  margin: 0 auto;
  @media screen and (max-width: 568px) {
    width: 95%;
  }
  @media screen and (max-width: 968px) {
    display: none;
  }
`;

export const FeatureList = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: flex-start;
  @media screen and (max-width: 568px) {
    flex-flow: column;
    justify-content: center;
  }
`;
export const Feature = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  box-sizing: border-box;
  padding: 10px;
  width: 33%;
  margin: 10px 0;
  > div[class="feature-img"] {
    box-sizing: border-box;
    width: 53px;
  }
  > span {
    font-size: 16px;
    font-style: normal;
    font-weight: 600;
    line-height: 21px;
    letter-spacing: 0em;
    text-align: left;
    margin-left: 15px;
  }
  @media screen and (max-width: 768px) {
    width: 95%;
    display: flex;
    justify-content: flex-start;
    margin: auto;
    > div[class="feature-img"] {
      box-sizing: border-box;
      width: 53px;
      > img {
        width: 40px;
        height: 40px;
      }
    }
  }
`;
