import React from "react";
import {
  FirstRow,
  FooterContainer,
  Partner,
  SecondRow,
  Sitemap,
  Social,
  Line,
  Wrap,
} from "./Footer.style";
import facebook from "./facebook.svg";
import Linkedin from "./Linkedin.svg";
import youtube from "./youtube.svg";
import Router, { withRouter } from "next/router";
import { connect } from "react-redux";
import LazyLoad from "react-lazyload";
import window from "global";
import { Imagekit_URL } from "../../../constants/Urls";
import optimizeImage from "../../Helper/OptimizeImage";

function Footer(props) {
  const navigateHelp = () => {
    if (
      !props.decide &&
      props.orderHistory &&
      (props.orderHistory?.currentOrder?.length > 0 ||
        props.orderHistory?.pastOrder?.length > 0)
    ) {
      Router.push("/help/order");
    } else {
      Router.push("/help");
    }
  };
  return (
    <>
      <Line></Line>
      <FooterContainer>
        <FirstRow>
          <Partner>
            <span>Our Payment Partners</span>
            <LazyLoad once offset={30}>
              <img
                src={optimizeImage(
                  `${Imagekit_URL}/Images/Assets/payment_footer.svg`,
                  window.innerWidth > 467 ? 340 : 420
                )}
                alt="Download"
              ></img>
            </LazyLoad>
          </Partner>
          <Social>
            <a
              href="https://www.facebook.com/truemedsindia"
              target="_blank"
              rel="noopener noreferrer"
            >
              {" "}
              <LazyLoad once offset={30}>
                <img src={facebook} alt="Download"></img>
              </LazyLoad>
            </a>

            <a
              href="https://www.youtube.com/channel/UCkkDCdWIciQfdcQ7p-7P8tA"
              target="_blank"
              rel="noopener noreferrer"
            >
              <LazyLoad once offset={30}>
                <img src={youtube} alt="Download"></img>
              </LazyLoad>
            </a>
            {/* <a
              href="https://twitter.com/Truemedsindia"
              target="_blank"
              rel="noopener noreferrer"
            >
              <img src={twitter} alt="Download"></img>
            </a> */}
            <a
              href="http://linkedin.com/company/truemeds-india"
              target="_blank"
              rel="noopener noreferrer"
            >
              <LazyLoad once offset={30}>
                <img src={Linkedin} alt="Download"></img>
              </LazyLoad>
            </a>
          </Social>
          <Sitemap>
            {/* <span onClick={() => Router.push("/option/aboutus")}>
              About Us
            </span> */}
            <span onClick={() => navigateHelp()}>Need Help</span>
            <span onClick={() => Router.push("/blog")}>Health Articles</span>
          </Sitemap>
        </FirstRow>
        <SecondRow>
          <Wrap>
            <span>©2021 - Truemeds | All right reserved</span>
            <span>
              <span
                onClick={() => {
                  Router.push("/option/legals/policy");
                }}
              >
                Privacy Policy
              </span>{" "}
              |{" "}
              <span
                onClick={() => {
                  Router.push("/option/legals/tnc");
                }}
              >
                Terms & Conditions
              </span>
            </span>
          </Wrap>
        </SecondRow>
      </FooterContainer>
    </>
  );
}
const mapStateToProps = (state) => ({
  decide: state.myOrder.orderTrackerError,
  orderHistory: state.myOrder.allOrdersSuccess,
});

export default withRouter(connect(mapStateToProps)(Footer));
