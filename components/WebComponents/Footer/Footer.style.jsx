import styled from "styled-components";

// Parent wrapper.
export const FooterContainer = styled.div`
  display: flex;
  flex-direction: column;
`;

export const Line = styled.div`
  height: 5px;
  width: 100%;
  background: linear-gradient(90deg, #0071bc 3.86%, #6ec1fd 85.64%);
`;
export const FirstRow = styled.div`
  display: flex;
  width: 80%;
  height: 105px;
  margin: 0 auto;
  align-items: center;
  justify-content: space-between;
  @media screen and (max-width: 968px) {
    width: 100%;
    flex-flow: column;
    height: auto;
  }
`;
export const Wrap = styled.div`
  width: 80%;
  height: inherit;
  margin: 0 auto;
  align-items: center;
  display: flex;
  justify-content: space-between;
  > span {
    font-style: normal;
    font-weight: normal;
    font-size: 12px;
    line-height: 14px;
    text-align: right;
    letter-spacing: 0.02em;
    color: #ffffff;
    margin: 10px 0;
    cursor: pointer;
  }
  @media screen and (max-width: 968px) {
    width: 100%;
    flex-flow: column;
    padding: 10px 0;
  }
`;

export const SecondRow = styled.div`
  height: 38px;
  width: 100%;

  background: #003055;
  @media screen and (max-width: 968px) {
    height: auto;
  }
`;
export const Partner = styled.div`
  display: flex;
  flex-direction: column;
  width: 35%;
  box-sizing: border-box;
  > div > img {
    height: 50px;
    width: 100%;
  }
  > span {
    font-size: 14px;
    font-style: normal;
    font-weight: 400;
    line-height: 14px;
    letter-spacing: 0.02em;
    text-align: left;
    color: #003055;
    margin-bottom: 5px;
  }
  @media screen and (max-width: 968px) {
    width: 100%;
    padding: 10px;
    align-items: center;
  }
`;
export const Social = styled.div`
  width: 30%;
  box-sizing: border-box;
  display: flex;
  justify-content: center;

  > a > div > img {
    margin: 10px;
  }
  @media screen and (max-width: 968px) {
    width: 100%;
  }
`;
export const Sitemap = styled.div`
  width: 35%;
  box-sizing: border-box;
  display: flex;
  justify-content: flex-end;
  > span {
    font-size: 12px;
    font-style: normal;
    font-weight: 600;
    line-height: 14px;
    letter-spacing: 0.02em;
    text-align: left;
    flex-grow: 0;
    margin: 0px 10px;
    cursor: pointer;
  }
  > span:last-child {
    margin: 0px 0 0 20px;
  }
  @media screen and (max-width: 968px) {
    width: 60%;
    justify-content: center;
  }
  @media screen and (max-width: 568px) {
    width: 100%;
    padding: 10px 5px;
    justify-content: space-between;
    > span:first-child {
      margin: 0px 20px 0 0px;
    }
  }
`;
