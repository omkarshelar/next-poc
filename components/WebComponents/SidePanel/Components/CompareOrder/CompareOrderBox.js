import { Alert } from "antd";
import React from "react";
import checkMedType from "../../../../Helper/CheckMedType";
import optimizeImage from "../../../../Helper/OptimizeImage";
import { CompareOrderBoxStyle } from "./CompareOrder.style";

const Compareorderbox = (props) => {
  const separateImageHandler = (links) => {
    if (links) {
      let result = links.split(",");
      let filter = result.find((data) => {
        let lastIndexOfSlash = data.lastIndexOf("/");
        return data.substring(lastIndexOfSlash + 1).includes("_1");
      });
      if (filter) {
        return filter;
      } else {
        return result[0];
      }
    } else {
      return null;
    }
  };

  return (
    <CompareOrderBoxStyle>
      {props.data?.type === 1 && (
        <p className="tmText">Truemeds recommended Brand</p>
      )}

      <div className="mainSection">
        <div className="medImg">
          {props.data?.prodImg ? (
            <img
              alt="medicine-img"
              src={optimizeImage(
                separateImageHandler(props.data?.prodImg),
                "80"
              )}
            />
          ) : (
            <img
              src={checkMedType(props.data?.drugType)}
              alt="medicine_image"
            />
          )}
        </div>

        <div className="medInfo">
          {props.data?.type === 1 ? (
            <p className="medTitle">{props.data?.originalMedName}</p>
          ) : (
            <p className="medTitle">{props.data?.currProductName}</p>
          )}
          {props.data?.type === 1 ? (
            <p className="medCompany">{props.data?.originalCompanyName}</p>
          ) : (
            <p className="medCompany">{props.data?.medCompanyName}</p>
          )}

          {props.data?.type !== 2 && <p className="warn">Out Of Stock</p>}

          {props.data?.type === 2 && (
            <p className="medCost">₹{props.data?.currProductSellingPrice}</p>
          )}

          {props.data?.type === 2 && (
            <div className="medCostWrapper">
              {((props.data?.currProductQty * props.data?.currProductMrp -
                props.data?.currProductSellingPrice) *
                100) /
                props.data?.currProductMrp >
              0 ? (
                <div>
                  <span className="strikeMrp">
                    ₹{props.data?.currProductMrp * props.data?.currProductQty}
                  </span>
                  <span className="active">
                    {(
                      ((props.data?.currProductQty *
                        props.data?.currProductMrp -
                        props.data?.currProductSellingPrice) *
                        100) /
                      props.data?.currProductMrp
                    ).toFixed(2)}
                    % OFF
                  </span>
                </div>
              ) : null}
              <span>Selected Quantity : {props.data?.ogQty}</span>
            </div>
          )}

          {props.data?.type === 3 && (
            <Alert
              className="errorAlert"
              message="Removed from your order"
              type="error"
            />
          )}

          {props.data?.type === 2 && (
            <Alert
              className="successAlert"
              message={`Updated Quantity: ${props.data?.currProductQty}`}
              description="Limited stock available"
              type="success"
            />
          )}
        </div>
      </div>
      {props.data?.type === 1 && (
        <div className="replacedMedWrapper">
          <p className="medTitle">Replaced with original</p>

          <div className="replacedMedBox">
            <p className="medTitle">{props.data?.currProductName}</p>
            <p className="medCompany">{props.data?.medCompanyName}</p>

            <p className="medCost">₹{props.data?.currProductSellingPrice}</p>

            <div className="medCostWrapper">
              {((props.data?.currProductMrp * props.data?.currProductQty -
                props.data?.currProductSellingPrice) *
                100) /
                props.data?.currProductMrp >
              0 ? (
                <>
                  <span className="strikeMrp">
                    ₹{props.data?.currProductMrp * props.data?.currProductQty}
                  </span>
                  <span className="active">
                    {(
                      ((props.data?.currProductMrp *
                        props.data?.currProductQty -
                        props.data?.currProductSellingPrice) *
                        100) /
                      props.data?.currProductMrp
                    ).toFixed(2)}
                    % OFF
                  </span>
                </>
              ) : null}
              <span>Quantity : {props.data?.currProductQty}</span>
            </div>
          </div>
        </div>
      )}
    </CompareOrderBoxStyle>
  );
};

export default Compareorderbox;
