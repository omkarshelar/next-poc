import React, { Component } from "react";
import { connect } from "react-redux";
import { CompareOrderWrapper } from "./CompareOrder.style";
import Compareorderbox from "./CompareOrderBox";
import { compareMedsElasticThunk } from "../../../../../redux/CompareMedsElastic/Action";
import ModificationLogLoader from "../../../../WebComponents/Shimmer/ModificationLogLoader";

export class CompareOrder extends Component {
  state = {
    originalData: null,
    updatedData: null,
    medData: null,
    comparedData: null,
  };
  componentDidMount = () => {
    //TODO - Add shimmer here
    if (this.props.orderData) {
      this.addImagesForMeds(
        this.props.orderData?.DoctorOrderConfirmed?.currentMedList
      );
    }
  };

  componentDidUpdate(prevProps) {
    if (prevProps.orderData !== this.props.orderData) {
      this.addImagesForMeds(
        this.props.orderData?.DoctorOrderConfirmed?.currentMedList
      );
    }
  }

  getAllProductCodes = (arr) => {
    let productCodes = [];
    for (let i in arr) {
      let med = arr[i];
      if (med.currProductCd === med.orgProductCd) {
        productCodes.push(med.currProductCd);
      } else {
        productCodes.push(med.currProductCd, med.orgProductCd);
      }
    }
    return productCodes;
  };

  addImagesForMeds = (medArr) => {
    let productCodes = this.getAllProductCodes(medArr);
    let data = {
      access_token: this.props.accessToken?.Response?.access_token,
      medicine: {
        query: {
          bool: {
            should: [
              {
                terms: {
                  "original_product_code.keyword": productCodes,
                },
              },
            ],
          },
        },
      },
      warehouseId: 3,
    };

    this.props.compareMedsElasticThunk(data);

    setTimeout(() => {
      if (this.props.medicineList?.length > 0) {
        this.setState(
          {
            originalData:
              this.props.orderData?.DoctorOrderConfirmed?.currentMedList,
            updatedData: this.props.orderData?.BoxVerified?.currentMedList,
            medData: this.props.medicineList.map((i) => i._source),
          },
          () => {
            //TODO - stop shimmer here
            this.compareLogic();
          }
        );
      }
    }, 1000);
  };

  compareLogic = () => {
    let tempArr = [];

    this.state.originalData &&
      this.state.originalData.map((i) => {
        let doctorConfirmedMedInBox = false;

        this.state.updatedData &&
          this.state.updatedData.map((j) => {
            if (i.orgProductCd === j.orgProductCd) {
              doctorConfirmedMedInBox = true;
            }
            this.state.medData &&
              this.state.medData.map((p) => {
                if (i.orgProductCd === j.orgProductCd) {
                  if (
                    i.currProductCd === j.currProductCd &&
                    i.currProductQty !== j.currProductQty &&
                    i.currProductActive === true &&
                    j.currProductActive === true
                  ) {
                    ///////////////////////////////////////////
                    //? Qty Check & medImg, company name check
                    ///////////////////////////////////////////

                    let qtyArr = j;
                    qtyArr.type = 2;
                    qtyArr.ogQty = i.currProductQty;

                    if (j.currProductCd === p.original_product_code) {
                      qtyArr.prodImg = p.product_image_urls;
                      qtyArr.drugType = p.original_drug_type;
                      qtyArr.medCompanyName = p.original_company_nm;

                      tempArr.push(qtyArr);
                    }
                    //? comparing only original product code
                    // else if (j.currProductCd === p.subs_product_code) {
                    //   qtyArr.prodImg = p.product_image_urls;
                    //   qtyArr.drugType = p.original_drug_type;
                    //   qtyArr.medCompanyName = p.subs_company_nm;

                    //   tempArr.push(qtyArr);
                    // }
                  } else if (
                    // i.currProductCd === j.currProductCd &&
                    i.currProductActive === true &&
                    j.currProductActive === false
                  ) {
                    ///////////////////////////////////////////
                    //? Out of stock check & medImg, company name check
                    ///////////////////////////////////////////

                    // console.log("Inside type 3");
                    let remArr = j;
                    remArr.type = 3;

                    if (
                      j.currProductCd === p.original_product_code ||
                      j.currProductCd === p.subs_product_code
                    ) {
                      remArr.prodImg = p.product_image_urls;
                      remArr.drugType = p.original_drug_type;
                      remArr.medCompanyName = p.original_company_nm;
                      tempArr.push(remArr);
                    }

                    if (j.currProductCd === p.subs_product_code) {
                      remArr.medCompanyName = p.subs_company_nm;

                      tempArr.push(remArr);
                    }
                  } else if (
                    i.currProductCd !== j.currProductCd &&
                    i.orgProductCd === j.currProductCd &&
                    i.currProductActive === true &&
                    j.currProductActive === true
                  ) {
                    ///////////////////////////////////////////
                    //? Subs changed check & medImg, company name check
                    ///////////////////////////////////////////

                    // console.log("Inside type 1");
                    let subArr = j;

                    subArr.type = 1;

                    if (i.currProductCd === p.original_product_code) {
                      subArr.originalCompanyName = p.original_company_nm;
                      subArr.originalMedName = p.original_sku_name;
                      subArr.prodImg = p.product_image_urls;
                      subArr.drugType = p.original_drug_type;

                      tempArr.push(subArr);
                    } else if (i.currProductCd === p.subs_product_code) {
                      subArr.originalCompanyName = p.subs_company_nm;
                      subArr.originalMedName = p.subs_sku_name;

                      tempArr.push(subArr);
                    }

                    if (j.currProductCd === p.original_product_code) {
                      subArr.medCompanyName = p.original_company_nm;

                      tempArr.push(subArr);
                    }
                  }
                  if (
                    i.currProductCd !== j.currProductCd &&
                    i.orgProductCd === j.currProductCd &&
                    i.currProductActive === true &&
                    j.currProductActive === true &&
                    i.orgProductQty > j.currProductQty
                  ) {
                    ///////////////////////////////////////////
                    //? Subs changed qty changed check & medImg, company name check
                    ///////////////////////////////////////////
                    let subQtyChangeArr = {
                      ...j,
                      type: 2,
                      ogQty: i.orgProductQty,
                    };

                    // subQtyChangeArr.type = 2;
                    // subQtyChangeArr.ogQty = i.currProductQty;

                    if (j.currProductCd === p.original_product_code) {
                      subQtyChangeArr.prodImg = p.product_image_urls;
                      subQtyChangeArr.drugType = p.original_drug_type;
                      subQtyChangeArr.medCompanyName = p.original_company_nm;

                      tempArr.push(subQtyChangeArr);
                      // console.log(subQtyChangeArr, "TempArray inside type 2");
                    }
                  }
                }

                // console.log(tempArr, "TempArray");
                let newTemp = [...new Set(tempArr)];
                this.setState({ comparedData: newTemp });
              });
          });

        if (!doctorConfirmedMedInBox) {
          // console.log("doctorConfirmedMedInBox falseee");

          this.state.medData &&
            this.state.medData.map((p) => {
              ///////////////////////////////////////////
              //? Out of stock if currProdId doesn't match
              ///////////////////////////////////////////

              let remArr = i;
              remArr.type = 3;

              if (
                i.currProductCd === p.original_product_code ||
                i.currProductCd === p.subs_product_code
              ) {
                remArr.prodImg = p.product_image_urls;
                remArr.drugType = p.original_drug_type;
                remArr.medCompanyName = p.original_company_nm;
                tempArr.push(remArr);
              }

              if (i.currProductCd === p.subs_product_code) {
                remArr.medCompanyName = p.subs_company_nm;

                tempArr.push(remArr);
              }
            });
        }
      });
  };

  render() {
    if (this.props.isLoading || !this.state.comparedData) {
      return <ModificationLogLoader />;
    } else {
      return (
        <>
          <CompareOrderWrapper>
            {this.state.comparedData?.length > 0 &&
              this.state.comparedData.map((i, p) => (
                <Compareorderbox data={i} key={p} />
              ))}
          </CompareOrderWrapper>
        </>
      );
    }
  }
}

const mapStateToProps = (state) => ({
  accessToken: state.loginReducer?.verifyOtpSuccess?.Response?.access_token,
  userDto: state.loginReducer?.verifyOtpSuccess?.CustomerDto,
  medicineList: state.compareMedsElastic?.compareMedsElasticSuccess?.hits,
  elasticService: state.compareMedsElastic,
  isLoading: state.loader.isLoading,
});

const mapDispatchToProps = { compareMedsElasticThunk };

export default connect(mapStateToProps, mapDispatchToProps)(CompareOrder);
