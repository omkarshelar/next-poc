import styled from "styled-components";

export const CompareOrderWrapper = styled.div`
  width: 100%;
  height: 100%;
  /* background-color: #e9eef6; */

  > div:not(:last-child) {
    border-bottom: 8px solid #e9eef6;
  }
`;

export const CompareOrderBoxStyle = styled.div`
  width: 100%;

  .tmText {
    font-size: 12px;
    color: #41a45c;
    font-weight: 500;
    padding: 1em 1em 0 1em;
  }

  .mainSection {
    padding: 1em;
    display: flex;
    flex-flow: row nowrap;
    gap: 1em;

    .medImg {
      width: 25%;
      height: auto;
      overflow: hidden;

      img {
        width: 100%;
        height: auto;
        object-fit: contain;
      }
    }

    .medInfo {
      width: 100%;
      display: flex;
      flex-flow: column;
      /* gap: 0.5em; */

      p {
        margin: 0;
      }

      p.medTitle {
        font-weight: 600;
        font-size: 14px;
        line-height: 24px;
      }

      p.medCompany {
        font-size: 12px;
        text-transform: capitalize;
        color: #8897a2;
      }

      p.warn {
        font-weight: 600;
        font-size: 14px;
        color: #ff7373;
        margin: 0.5em 0 1em 0;
      }

      p.medCost {
        font-weight: bold;
        font-size: 15px;
        margin-top: 0.5em;
      }

      .medCostWrapper {
        display: flex;
        flex-direction: column;
        margin-bottom: 16px;

        div {
          display: flex;
          flex-flow: row nowrap;
          gap: 0.5em;
          align-items: center;
          margin-bottom: 8px;
        }

        span {
          font-size: 12px;
          color: #8897a2;
        }

        span.strikeMrp {
          text-decoration: line-through;
          margin-right: 0.5em;
        }

        span.active {
          font-size: 14px;
          font-weight: 600;
          color: #25b374;
          margin-right: 0.5em;
        }
      }

      .successAlert {
        border-radius: 5px;

        background-color: #edf6f3;
        border: none;

        .ant-alert-message {
          font-size: 14px;
          font-weight: 600;
        }

        .ant-alert-description {
          font-size: 14px;
          color: #8897a2;
        }
      }

      .errorAlert {
        border-radius: 5px;

        background-color: #ffeaea;
        border: none;

        .ant-alert-message {
          font-size: 14px;
          font-weight: 600;
        }
      }
    }
  }

  .replacedMedWrapper {
    background: #edf6f3;
    padding: 14px 14px 19px 14px;
    display: flex;
    flex-flow: column;
    gap: 3px;

    p.medTitle {
      font-weight: 600;
      font-size: 14px;
      line-height: 24px;
      margin-bottom: 8px;
    }

    .replacedMedBox {
      background-color: #fff;
      padding: 1em;
      border-radius: 8px;

      p {
        margin: 0;
      }

      p.medTitle {
        font-weight: 600;
        font-size: 14px;
        line-height: 24px;
      }

      p.medCompany {
        font-size: 12px;
        text-transform: capitalize;
        color: #8897a2;
      }

      p.medCost {
        font-weight: bold;
        font-size: 15px;
        margin-top: 0.5em;
      }

      .medCostWrapper {
        display: flex;
        flex-direction: row wrap;
        align-items: center;
        margin-bottom: 16px;

        div {
          display: flex;
          flex-flow: row nowrap;
          gap: 0.5em;
          align-items: center;
          margin-bottom: 8px;
        }

        span {
          font-size: 12px;
          color: #8897a2;
        }

        span.strikeMrp {
          text-decoration: line-through;

          margin-right: 0.5em;
        }

        span.active {
          font-size: 14px;
          font-weight: 600;
          color: #25b374;
          margin-right: 0.5em;
        }
      }
    }
  }
`;
