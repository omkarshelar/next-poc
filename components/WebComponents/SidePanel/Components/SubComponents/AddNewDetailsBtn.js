import React from "react";
import "./AddNewDetailsBtn.css";

const AddNewDetailsBtn = (props) => {
  return (
    <div
      className={"addNewDetailsBtn_btnWrapper"}
      onClick={() => {
        props.clickHandler();
      }}
    >
      {props.addText}
    </div>
  );
};

export default AddNewDetailsBtn;
