import React from "react";
import "./HeaderWithCTA.css";

const HeaderWithCTA = (props) => {
  return (
    <div
      id={props.id ? props.id : ""}
      className={`${"headerWithCta_HeaderCTAWrap"} ${
        props.shouldSticky ? "headerWithCta_sticky" : ""
      }`}
    >
      <div className={"headerWithCta_title"}>{props.title}</div>
      {props.showCta && (
        <div className={"headerWithCta_ctaText"} onClick={() => props.onCta()}>
          {props.ctaText}
        </div>
      )}
    </div>
  );
};

export default HeaderWithCTA;
