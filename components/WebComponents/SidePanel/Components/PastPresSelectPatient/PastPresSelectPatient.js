import React, { useEffect, useState } from "react";
import "./PastPresSelectPatient.css";
import { connect } from "react-redux";
import { withRouter } from "next/router";
import window from "global";

export function PastPresSelectPatient(props) {
  const [patientArr, setPatientArr] = useState([]);

  const getPrescriptionsByPatientId = (id) => {
    let finalArr = patientArr;
    let finalObj = {};
    if (finalArr?.length > 0) {
      finalObj = finalArr.find((x) => x.patientId === id);
      finalObj.ActiveRx = finalObj.ActiveRx.reverse();
    }
    return finalObj;
  };

  const addAllPrescriptions = (arr) => {
    let allPrescriptions = [];
    for (let i in arr) {
      let patient = arr[i];
      if (patient.ActiveRx?.length > 0) {
        allPrescriptions = [...allPrescriptions, ...patient.ActiveRx];
      }
    }
    return allPrescriptions;
  };

  const createPatientList = (arr) => {
    let patientArr = [];
    let cnt = 0;
    patientArr.push({
      patientId: "All",
      patientName: "All Prescriptions",
      ActiveRx: addAllPrescriptions(arr),
    });
    patientArr.push(...arr);
    for (let i in patientArr) {
      let patient = patientArr[i];
      patient.selected = false;
      let activePrescriptionStamp = [];
      let finalPrescriptions = [];
      for (let j in patientArr[i]["ActiveRx"]) {
        if (
          activePrescriptionStamp.includes(
            patientArr[i]["ActiveRx"][j]["imageId"]
          ) == false
        ) {
          activePrescriptionStamp.push(patientArr[i]["ActiveRx"][j]["imageId"]);
          finalPrescriptions.push(patientArr[i]["ActiveRx"][j]);
        }
      }
      patientArr[i]["ActiveRx"] = finalPrescriptions;
      if (patientArr[i].patientId !== "All" && finalPrescriptions.length > 0) {
        cnt++;
      }
    }
    if (cnt === 1) {
      patientArr.shift();
    }
    setPatientArr(patientArr);
  };

  const onClickSelectPatient = (id) => {
    let selectedPatient = getPrescriptionsByPatientId(id);
    window.openSideBar(true, 2, selectedPatient);
  };

  useEffect(() => {
    createPatientList(props.allPrescriptions?.prescriptionById?.CustomerRx);
  }, []);

  return (
    <div className="selectPatientContainer">
      <p>Select Patient</p>
      <div className="selectPatientGridContainer">
        {patientArr?.length > 0 &&
          patientArr.map(
            (data) =>
              data.ActiveRx.length > 0 && (
                <div onClick={() => onClickSelectPatient(data.patientId)}>
                  <span>{data.patientName ? data.patientName : "Patient"}</span>
                  <span>
                    {data.ActiveRx.length > 1
                      ? data.ActiveRx.length + " items"
                      : data.ActiveRx.length === 0
                      ? ""
                      : "1 item"}
                  </span>
                </div>
              )
          )}
      </div>
    </div>
  );
}

const mapStateToProps = (state) => ({
  allPrescriptions: state.uploadImage,
});

export default withRouter(connect(mapStateToProps, {})(PastPresSelectPatient));
