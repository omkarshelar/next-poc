import styled from "styled-components";

export const ImgPreviewContainer = styled.div`
  display: flex;
  flex-flow: column;
  > div {
    margin-top: 5px;
    width: 50vh;
    height: auto;
  }
  > div > img {
    width: 100%;

    border-radius: 5px;
  }
  z-index: 11000;
  background-color: white;
  padding: 10px 10px;
  border-radius: 4px;
  position: absolute;
  top: 50%;
  left: 50%;
  -moz-transform: translateX(-50%) translateY(-50%);
  -webkit-transform: translateX(-50%) translateY(-50%);
  transform: translateX(-50%) translateY(-50%);
  @media screen and (max-width: 468px) {
    width: 95%;
    height: auto;
  }

  > div > img {
    width: 100%;
    height: auto;
  }
`;
