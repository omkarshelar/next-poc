import styled from "styled-components";

export const MedWrapperOutter = styled.div`
  position: relative;

  .medCardSecondContainer {
    display: flex;
    align-items: center;
    grid-gap: 2rem;
    gap: 2rem;
    position: absolute;
    bottom: 10px;
    right: 10px;
  }

  .medCardErrorMsg {
    color: #ff7373;
    font-weight: 600;
    font-size: 16px;
  }
`;
// Card container
export const Medwrapper = styled.div`
  box-sizing: border-box;
  width: 100%;
  padding: 16px;
  display: flex;
  border-radius: 12px;
  cursor: pointer;
  @media screen and (max-width: 468px) {
    padding: 12px;
  }
`;

export const FirstRow = styled.div`
  display: flex;
  flex-direction: column;
  width: 85%;
  margin-left: 10px;
  @media screen and (max-width: 468px) {
    width: 100%;
  }
  .price-add-container {
    display: flex;
    justify-content: space-between;
    align-items: flex-end;
  }
  .max70 {
    max-width: 70%;
  }
`;

export const ImgContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 15%;
  .tm-med-image {
    width: 80px;
    height: 80px;
    object-fit: contain;
    @media screen and (max-width: 468px) {
      width: 40px;
      height: 40px;
    }
  }
  .tm-stock-image {
    width: 60px;
    height: 60px;
    @media screen and (max-width: 468px) {
      width: 100%;
      height: 100%;
    }
  }
`;

// Image, Med Details
export const ImgMedDetails = styled.div`
  > p {
    font-weight: 600;
    font-size: 16px;
    margin: 0;
    color: #4f585e;
    padding: 0px;
    text-transform: capitalize;
    @media screen and (max-width: 468px) {
      font-size: 14px;
    }
  }
  > span {
    font-weight: 400;
    font-size: 12px;
    color: #8897a2;
    text-transform: capitalize;
  }
`;

export const Price = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: 8px;
  .main-prices-container {
    display: flex;
    align-items: center;
    margin-right: 7px;
    @media screen and (max-width: 768px) {
      margin-right: 3px;
    }
  }
  .selling-price {
    color: #4f585e;
    font-weight: 700;
    font-size: 16px;
    margin-right: 8px;
    @media screen and (max-width: 468px) {
      font-size: 15px;
    }
    @media screen and (max-width: 320px) {
      font-size: 12px;
    }
  }
  .mrp-price {
    font-weight: 400;
    font-size: 12px;
    color: #8897a2;
    @media screen and (max-width: 320px) {
      font-size: 10px;
    }
  }

  .mrp-quantity {
    font-weight: 400;
    font-size: 12px;
    color: #4f585e;
  }
  .min-discount-container {
    border-radius: 4px;
    background-color: none;
    width: fit-content;
    padding: 0px;
  }
  .min-discount-container span {
    color: #25b374;
    font-weight: 700;
    font-size: 14px;
    margin-right: 7px;
    @media screen and (max-width: 768px) {
      margin-right: 3px;
    }
    @media screen and (max-width: 320px) {
      font-size: 12px;
    }
  }
  .main-price-wrap {
    display: flex;
    /* gap: 7px; */
    align-items: center;
    @media screen and (max-width: 320px) {
      /* gap: 3px; */
    }
  }
`;

export const Quantity = styled.div`
  display: block;
  /* width: 100%; */
  width: auto;
  position: absolute;
  bottom: 16px;
  right: 16px;
  @media screen and (max-width: 768px) {
    width: 30%;
  }
  box-sizing: border-box;
  align-items: center;

  .css-b62m3t-container {
    width: 100px !important;
    @media screen and (max-width: 468px) {
      width: 80px !important;
    }
  }

  .css-1s2u09g-control,
  .css-1pahdxg-control {
    border: 1.6px solid #0071bc !important;
    border-radius: 6px;
    height: 44px !important;
    @media screen and (max-width: 468px) {
      height: 30px !important;
    }
  }

  .css-1okebmr-indicatorSeparator {
    display: none !important;
  }

  .css-tj5bde-Svg,
  .css-qc6sy-singleValue {
    color: #0071bc !important;
  }

  .css-1pahdxg-control:hover {
    border-color: #0071bc !important;
  }

  .css-2613qy-menu div {
    color: #0071bc !important;
  }

  .css-1n7v3ny-option {
    background-color: #f5f5f5;
    color: #0071bc !important;
    font-size: 14px;
    font-weight: 400;
  }

  .css-tlfecz-indicatorContainer {
    color: #0071bc !important;
  }
  .css-9gakcf-option {
    background-color: #e6f7ff;
    color: #0071bc !important;
  }

  .css-yt9ioa-option {
    color: #0071bc !important;
    font-size: 14px;
    font-weight: 400;
  }

  .css-14el2xx-placeholder {
    color: #0071bc !important;
    font-size: 14px;
    font-weight: 400;
  }

  .css-26l3qy-menu {
    z-index: 2;
  }

  .css-4ljt47-MenuList::-webkit-scrollbar {
    width: 5px;
  }

  .css-4ljt47-MenuList::-webkit-scrollbar-thumb {
    border-radius: 73px;
    background: #a9a9a9;
  }

  .css-4ljt47-MenuList::-webkit-scrollbar-track {
    background: transparent;
  }

  > div[class="qtyFloat"] {
    color: #4f585e;
    font-size: 16px;
    font-weight: 600;
    text-align: right;
  }
  > div[class="remove"] {
    order: 2;
    display: flex;
    align-items: center;
    > img {
      width: 15px;
    }
    span {
      color: #dd2200;
      /* font-family: "Raleway", sans-serif; */
      font-size: 14px;
      font-style: normal;
      font-weight: 600;
      line-height: 16px;
      letter-spacing: 0.02em;
      text-align: center;
      padding-left: 4px;
    }
    @media screen and (max-width: 468px) {
      order: 1;
    }
  }

  > div[class="quantity-select"] {
    display: block;
    align-items: center;
    float: right;
    order: 1;
    .ant-select {
      width: 100px !important;
      @media screen and (max-width: 468px) {
        width: 80px !important;
      }
    }

    .ant-select:not(.ant-select-customize-input) .ant-select-selector {
      border: 1.6px solid #0071bc !important;
      border-radius: 6px;
      height: 44px !important;
      display: flex !important;
      align-items: center !important;
      @media screen and (max-width: 468px) {
        height: 30px !important;
      }
    }

    .ant-select-arrow {
      color: #0071bc !important;
    }

    .ant-select-focused:not(.ant-select-disabled).ant-select:not(.ant-select-customize-input)
      .ant-select-selector {
      border: 1.6px solid #0071bc !important;
      box-shadow: 0 0 0 2px rgb(0 113 188 / 20%) !important;
    }

    .ant-select-single.ant-select-show-arrow .ant-select-selection-item,
    .ant-select-single.ant-select-show-arrow .ant-select-selection-placeholder {
      font-weight: 700 !important;
      font-size: 16px !important;
      color: #0071bc !important;
      font-family: "Inter", sans-serif !important;
    }

    > i:first-child {
      margin-right: 10px;
    }
    > i:last-child {
      margin-left: 10px;
    }
    > span {
      font-weight: 700;
      font-size: 16px;
      /* font-family: "Century Gothic", sans-serif; */
      color: #333;
    }
    font-family: "Inter", sans-serif;
    font-size: 14px;
    font-style: normal;
    font-weight: 700;
    line-height: 18px;
    letter-spacing: 0em;
    text-align: left;
    @media screen and (max-width: 468px) {
      order: 2;
      .ant-select-single.ant-select-show-arrow .ant-select-selection-item,
      .ant-select-single.ant-select-show-arrow
        .ant-select-selection-placeholder {
        font-size: 14px !important;
      }
    }
  }
`;

// Add to cart/ quantity
export const AddCart = styled.div`
  display: flex;
  width: 100%;
  position: absolute;
  bottom: 16px;
  right: 16px;
  align-items: center;
  justify-content: flex-end;
  > span[class="not-in-stock"] {
    align-self: flex-end;
    color: #999999;
    font-weight: 600;
  }
  > span[class="not-available"] {
    align-self: flex-end;
    color: #999999;
    font-weight: 600;
  }
`;

export const HR = styled.hr`
  display: block;
  width: 100%;
  opacity: 0.3;
  border-top: 1px solid #0071bc;
  margin-top: 0.1rem;
  margin-bottom: 0.1rem;
`;

export const SecondRowWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  width: 100%;
  flex-grow: 1;
  @media screen and (max-width: 768px) {
    justify-content: flex-end;
    width: 40%;
  }
`;

export const ViewOriginal = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  border: 1px solid rgba(34, 181, 115, 0.4);
  border-radius: 4px;
  margin-bottom: ${(props) => (props.hideLast ? "0px" : "16px")};
  > span:nth-child(1) {
    color: #333;
    font-size: 14px;
    font-weight: 600;
    padding: 16px;
  }
  > span:nth-child(2) {
    color: #0071bd;
    font-size: 14px;
    font-weight: 600;
    cursor: pointer;
    padding: 16px;
  }
  @media screen and (max-width: 768px) {
    margin: 0px 15px 18px 15px;
  }
  @media screen and (max-width: 468px) {
    flex-direction: column;
    align-items: flex-start;
    padding: 0px;
    > span:nth-child(1) {
      color: #333;
      font-size: 14px;
      font-weight: 600;
      padding: 10px 10px 0px 10px;
    }
    > span:nth-child(2) {
      padding: 0px 10px 10px 10px;
    }
  }
`;

export const ViewOriginalSavings = styled.div`
  background: #2e5874;
  border-radius: 4px;
  padding: 12px 16px;
  margin-top: 16px;
  margin-bottom: 2px;
  position: relative;
  .savingTitle {
    font-style: normal;
    font-weight: 700;
    font-size: 16px;
    line-height: 24px;
    color: #ffffff;
    margin-bottom: 4px;
  }

  .savingSubText {
    font-style: normal;
    font-weight: 500;
    font-size: 14px;
    line-height: 18px;
    color: #ffffff;
  }

  .savingPointer {
    position: absolute;
    bottom: -14px;
    left: 12px;
  }

  @media screen and (max-width: 768px) {
    margin-left: 16px;
    margin-right: 16px;

    .savingTitle {
      font-size: 14px;
      line-height: 20px;
    }

    .savingSubText {
      font-size: 12px;
      line-height: 20px;
    }
  }
`;
