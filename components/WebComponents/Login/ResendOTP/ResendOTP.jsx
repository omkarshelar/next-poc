import React, { useEffect, useState } from "react";
import { ResendText } from "../Login.style";
import tickIcon from "../../../../src/Assets/tick.png";
function ResendOTP(props) {
  const [seconds, setSeconds] = useState(props.seconds);
  const [enableButton, setenableButton] = useState(false);
  const [resend, setResend] = useState(false);
  const [successMsg, setsuccessMsg] = useState(props.successMessage);
  function updateTime() {
    if (seconds === 58) {
      setsuccessMsg("");
    }
    if (seconds === 1) {
      setenableButton(true);
    } else {
      setSeconds((seconds) => seconds - 1);
    }
  }

  function resendOTP() {
    setSeconds(props.seconds);
    setResend(true);
  }

  useEffect(() => {
    if (resend && seconds === props.seconds) {
      setenableButton(false);
      const token = setTimeout(updateTime, 1000);
      return function cleanUp() {
        clearTimeout(token);
      };
    }
  }, [seconds]);

  useEffect(() => {
    const token = setTimeout(updateTime, 1000);
    return function cleanUp() {
      clearTimeout(token);
    };
  });

  return (
    <>
      {props.errorMessage && (
        <div style={{ marginTop: "5px" }}>
          <span style={{ color: "#e74c3c", fontWeight: "500" }}>
            {props.errorMessage}
          </span>
        </div>
      )}
      {enableButton ? (
        <div style={{ margin: "12px 0px" }}>
          <ResendText style={{ marginRight: "2px" }}>
            Did not receive the OTP?
          </ResendText>
          <ResendText
            style={{
              color: "#0071bc",
              cursor: "pointer",
              fontWeight: "600",
              textDecoration: "underline",
            }}
            onClick={() => {
              props.onClickResend();
              resendOTP();
            }}
          >
            Resend OTP
          </ResendText>
        </div>
      ) : (
        <ResendText>{`Resend OTP in ${seconds} secs`}</ResendText>
      )}
      {successMsg && (
        <div className="otp-success-message">
          <div>
            <img src={tickIcon} alt="success" />
          </div>
          <span>{successMsg}</span>
        </div>
      )}
    </>
  );
}

export default ResendOTP;
