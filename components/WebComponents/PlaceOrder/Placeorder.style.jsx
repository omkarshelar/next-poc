import styled from "styled-components";

export const PlaceorderWrapper = styled.div`
  display: flex;
  flex-direction: column;

  margin: 2rem 0 1rem 0;

  > span {
    /* font-family: "Century Gothic", sans-serif; */
    font-size: 30px;
    font-style: normal;
    font-weight: 700;

    letter-spacing: 0px;
    text-align: center;
    color: #003055;
    align-self: center;
  }
  @media screen and (max-width: 968px) {
    display: none;
  }
`;
export const StepsWrapper = styled.div`
  display: flex;
  max-width: 70%;
  margin: 0 auto;
  flex-wrap: wrap;
  justify-content: center;
`;

export const Step = styled.div`
  box-sizing: border-box;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  flex: 1;
  padding: 1%;
`;

export const StepImage = styled.div`
  width: 100px;
  height: 100px;
  @media screen and (max-width: 568px) {
    width: 100px;
    height: 100px;
  }
  > img {
    width: 100%;
  }
`;
export const StepInfo = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  flex-grow: 1;
  > h3 {
    /* font-family: "Century Gothic", sans-serif; */
    font-size: 24px;
    font-style: normal;
    font-weight: 700;
    line-height: 29px;
    letter-spacing: 0px;
    text-align: center;
    color: #003055;
    margin-top: 5px;
  }
  > span {
    font-size: 16px;
    font-style: normal;
    font-weight: 700;
    line-height: 21px;
    letter-spacing: 0em;
    text-align: center;
    color: #003055;
  }
  > p {
    color: #003055;
    font-size: 16px;
    font-style: normal;
    font-weight: 400;
    line-height: 21px;
    letter-spacing: 0em;
    text-align: center;
    flex-grow: 1;
  }
`;
