import styled from "styled-components";

export const ArticleWrapper = styled.div`
  display: flex;
  flex-flow: column;
  width: 100%;
  margin-top: 5rem;
  background: linear-gradient(360deg, #c9e8fe 0%, rgba(180, 193, 253, 0) 50%);
  > div[class="explore-article"] {
    width: 85%;
    align-self: flex-end;
  }
  @media screen and (max-width: 568px) {
    margin-top: 2rem;
    > div[class="explore-article"] {
      width: 100%;
      display: flex;
      justify-content: center;
    }
  }
`;
export const Heading = styled.div`
  width: 85%;
  display: flex;
  justify-content: space-between;
  > h2 {
    /* font-family: "Century Gothic", sans-serif; */
    font-size: 30px;
    font-style: normal;
    font-weight: 700;
    letter-spacing: 0px;
    color: #003055;

    padding: 0 5px;
  }

  .vaWrap {
    display: flex;
    align-content: center;
    align-items: center;
  }

  .vaText {
    display: flex;
    align-items: center;
    border-bottom: 4px solid #0071bc;
    padding: 5px 10px;
    cursor: pointer;
    font-size: 16px;
    color: #0071bc;
    font-weight: 600;
    flex-direction: row;
    justify-content: center;
    height: fit-content;
  }
  @media screen and (max-width: 968px) {
    flex-flow: column;
    align-items: center;
    width: 100%;
    > div {
      display: flex;
    }
    > h2 {
      font-size: 25px;
      margin: auto;
    }

    .vaWrap {
      flex-flow: column;
    }
    .vaText {
      font-size: 12px;
    }
  }
  @media screen and (max-width: 568px) {
    > h2 {
      font-size: 21px;
    }
  }
  @media screen and (max-width: 480px) {
    > h2 {
      font-size: 18px;
      line-height: 35px;
    }
  }
`;

export const ArticleList = styled.div`
  display: flex;
  margin: 1rem 0 1rem 1rem;
  overflow-x: auto;
  scroll-behavior: smooth;
  transition: scroll-behavior 1s ease;
  -ms-overflow-style: none; /* IE and Edge */
  scrollbar-width: none; /* Firefox */

  ::-webkit-scrollbar:horizontal {
    height: 0;
    width: 0;
    display: none;
  }

  ::-webkit-scrollbar-thumb:horizontal {
    display: none;
  }
`;

export const Container = styled.div`
  width: 100%;
  display: flex;
  justify-content: flex-end;
`;

// ARTICLE CARD

export const CardWrapper = styled.div`
  cursor: pointer;
  box-sizing: border-box;
  min-width: 250px;
  width: 250px;
  height: 350px;
  /* height: 180px; */
  margin: 10px;
  background: #ffffff;
  border: 1px solid #d3f0e3;
  border-radius: 10px;
  overflow: hidden;
  text-overflow: ellipsis;
  display: flex;
  flex-flow: column;
  > div[class="img-placeHolder"] {
    height: 100%;
    max-height: 49%;
    > div {
      height: 100%;
    }
    > div > img {
      width: 100%;
      height: 100%;
      border-radius: 10px;
      border-bottom-left-radius: 0;
      border-bottom-right-radius: 0;
      object-fit: cover;
    }
  }
  :hover > div > div[class="hover-text"] {
    visibility: visible;
  }

  @media screen and (max-width: 568px) {
    min-width: 200px;
    width: 200px;
    height: 270px;
    /* height: 150px; */
  }
  @media screen and (max-width: 480px) {
    min-width: 150px;
    margin: 6px;
    width: 150px;
    height: 200px;
    /* height: 100px; */
  }
`;

export const Info = styled.div`
  position: relative;
  padding: 10px;
  height: 100%;
  display: flex;
  flex-flow: column;
  justify-content: space-between;
  background-color: #ffffff;
  > div[class="first-row"] {
    > header {
      /* font-family: "Raleway", sans-serif; */
      font-size: 14px;
      font-style: normal;
      font-weight: 600;
      letter-spacing: 0px;
      text-align: left;
      color: #333333;
      display: -webkit-box;
      -webkit-line-clamp: 3;
      -webkit-box-orient: vertical;
      overflow: hidden;
      text-overflow: ellipsis;
    }
    > p {
      margin-top: 10px;
    }
  }

  > div[class="second-row"] {
    > p {
      /* font-family: "Century Gothic", sans-serif; */
      font-size: 14px;
      text-align: right;
      margin: 0;
    }
  }

  > div[class="hover-text"] {
    position: absolute;
    text-align: center;
    width: 100%;
    left: 50%;
    -webkit-transform: translateX(-50%);
    transform: translateX(-50%);
    /* font-family: "Raleway", sans-serif; */
    font-style: normal;
    font-weight: 500;
    font-size: 18px;
    color: #0071bc;
    visibility: hidden;
    background: linear-gradient(
      360deg,
      #ffffff 0%,
      rgba(255, 255, 255, 0) 100%
    );
    display: flex;
    justify-content: center;
    align-items: flex-end;
    height: 100%;

    > p {
      margin-bottom: 3rem;
    }
  }

  @media screen and (max-width: 568px) {
    > div[class="first-row"] {
      > header {
        display: -webkit-box;
        -webkit-line-clamp: 2;
        -webkit-box-orient: vertical;
        overflow: hidden;
        text-overflow: ellipsis;
      }
      > p {
        display: -webkit-box;
        -webkit-line-clamp: 4;
        -webkit-box-orient: vertical;
        overflow: hidden;
        text-overflow: ellipsis;
      }
    }
  }

  @media screen and (max-width: 480px) {
    padding: 5px;
    > div[class="first-row"] {
      > header {
        font-size: 12px;
      }
      > p {
        margin: 5px 0px;
        font-size: 10px;
      }
    }

    > div[class="second-row"] {
      > p {
        font-size: 10px;
      }
    }
  }
`;
