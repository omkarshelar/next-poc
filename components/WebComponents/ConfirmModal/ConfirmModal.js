import React from "react";
import Dialog from "@material-ui/core/Dialog";
import { DialogContent } from "@material-ui/core";
import "./ConfirmModal.css";
const ConfirmModal = (props) => {
  return (
    <div>
      <Dialog
        open={props.open}
        onClose={props.handleClose}
        // aria-labelledby="responsive-dialog-title"
        fullWidth={true}
        maxWidth={"xs"}
        classes={{ paper: "confirmModal" }}
      >
        <DialogContent>
          <div className={"confirmModal__wrap"}>
            <div className={"confirmModal__Header"}>{props.Header}</div>
            <div
              className={
                props.type === 1
                  ? "confirmModal__ButtonWrap1"
                  : "confirmModal__ButtonWrap"
              }
            >
              {props.type === 1 && props.isPrescription && (
                <div
                  className={
                    props.type === 1
                      ? "confirmModal__SecBtn1"
                      : "confirmModal__SecBtn"
                  }
                  onClick={props.noMethod}
                >
                  {props.noText}
                </div>
              )}
              <div
                className={
                  props.type === 1
                    ? "confirmModal__PrimaryBtn1"
                    : "confirmModal__PrimaryBtn"
                }
                onClick={props.yesMethod}
              >
                {props.yesText}
              </div>
              {(!props.isPrescription ||
                (props.type === 0 && props.isPrescription)) && (
                <div
                  className={
                    props.type === 1
                      ? "confirmModal__SecBtn1"
                      : "confirmModal__SecBtn"
                  }
                  onClick={props.noMethod}
                >
                  {props.noText}
                </div>
              )}
            </div>
          </div>
        </DialogContent>
      </Dialog>
    </div>
  );
};

export default ConfirmModal;
