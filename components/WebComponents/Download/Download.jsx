import React, { Component } from "react";
import play from "../../../src/Assets/PlayUpdate.webp";
import { DownloadWrapper, Link, BadgeImage, Container } from "./Download.style";
import { Imagekit_URL } from "../../../constants/Urls";

export class Download extends Component {
  render() {
    return (
      <Container>
        <DownloadWrapper>
          <BadgeImage>
            <img
              src={`${Imagekit_URL}/Images/Assets/iphoneOther.webp?tr=cm-pad_resize,lo-true,w-300`}
              alt="iphone"
            />
          </BadgeImage>

          <Link>
            <h2>Download Truemeds</h2>
            <p>
              It’s important to give you health the attention it deserves. Wait
              no further. Download the app today and choose to not compromise on
              your health or your budget.
            </p>
            <a
              href="http://bit.ly/TruemedsAND"
              target="_blank"
              rel="noopener noreferrer"
            >
              <img src={play} alt="Download"></img>
            </a>
          </Link>
        </DownloadWrapper>
      </Container>
    );
  }
}

export default Download;
