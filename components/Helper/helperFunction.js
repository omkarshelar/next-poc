import { message } from "antd";

export function customNotification(
  content = "",
  className = "customNotify",
  duration = 1
) {
  return message.open({
    content: content,
    duration: duration,
    className: className,
  });
}

export function customNumber(num) {
  if (num) {
    return Number(num);
  } else {
    return 0;
  }
}
