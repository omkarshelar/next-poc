//used in View original percentage savings
export function viewOriginalCalulations(orgMrp, subsSellingPrice) {
  return ((100 * (orgMrp - subsSellingPrice)) / orgMrp).toFixed(2);
}

//cancelled savings Calculations
export function cancelledOrderDetailsSavings(arr, offerDiscountApplied = 0) {
  let savings = 0;
  for (let i in arr) {
    let med = arr[i];
    if (!med.coldChainDisabled && !med.disabled && med.medActive) {
      if (med.orgProductCd === med.subsProductCd) {
        savings = savings + (med.subsMrp - med.subsSellingPrice);
      } else {
        savings = savings + med.orgMrp - med.subsSellingPrice;
      }
    }
  }
  let couponValue = offerDiscountApplied;
  savings = savings + couponValue;
  return savings.toFixed(2);
}
