import moment from "moment";

const getDeliveryDate = (deliveryDate, type = 1) => {
  let date = moment(deliveryDate);
  let date2 = moment(deliveryDate);
  let step = 1;
  let firstDate = date;
  let secondDate = date2.subtract(step, "days");

  if (firstDate.format("MMMM") === secondDate.format("MMMM")) {
    if (type === 1) {
      return <>{secondDate.format("Do") + "-" + firstDate.format("Do MMMM")}</>;
    } else {
      return <>{secondDate.format("Do") + "-" + firstDate.format("Do MMM")}</>;
    }
  } else {
    return (
      <>{secondDate.format("Do MMM") + " -" + firstDate.format("Do MMM")}</>
    );
  }
};

export default getDeliveryDate;
