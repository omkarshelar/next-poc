import tabletIcon from "../../src/Assets/tablet-icon.svg";
import syrupIcon from "../../src/Assets/syrup-icon.svg";
import injectionIcon from "../../src/Assets/injection-icon.svg";
// import creamIcon from "../../Assets/cream-icon.svg";
// import powderIcon from "../../Assets/powder-icon.svg";
// import kitIcon from "../../Assets/kit-icon.svg";
// import inhalerIcon from "../../Assets/inhaler-icon.svg";

const checkMedType = (type) => {
  let img = tabletIcon;
  if (type === "SYRUP") {
    img = syrupIcon;
  } else if (type === "INJECTION") {
    img = injectionIcon;
  }
  // else if (type === "CREAM") {
  //   img = tabletIcon;
  // } else if (type === "TETRAPACK" || type === "POWDER") {
  //   img = tabletIcon;
  // } else if (type === "STRIPS" || type === "KIT") {
  //   img = tabletIcon;
  // } else if (type === "INHALER") {
  //   img = tabletIcon;
  // }
  return img;
};
export default checkMedType;
